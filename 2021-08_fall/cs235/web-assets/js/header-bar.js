// CS 235
$( document ).ready( function()
{
    // Course information
    var course = "CS 235: Object Oriented Programming with C++";
    var dates = "2021-01-19 - 2021-05-17";
    var semester = "Spring 2021";
    var homepage = "https://rachels-courses.gitlab.io/cs235-webpage/";
    var courseRepository = "https://gitlab.com/rachels-courses/cs235-object-oriented-programming-with-cpp";
    var courseStudentRepository = "https://gitlab.com/rachels-courses/cs235";
    var courseTextbook = "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false";
    var alternativeTextbook = "http://www.greenteapress.com/thinkcpp/";
    var courseCatalogPage = "http://catalog.jccc.edu/coursedescriptions/cs/#CS_235";
    var syllabus = "syllabus.html";
    var videoArchive = "";
    var quickReference = "https://rachels-courses.gitlab.io/course-commons/reference.html";
    var miscellaneous = "";
    var gather = "https://gather.town/app/2QfpyTXUz9l32na8/rachelsinghjccc";
    var review = "https://rachels-courses.gitlab.io/course-commons/review.html";
    var exampleCode = "https://gitlab.com/rachels-courses/cs235-object-oriented-programming-with-cpp/-/tree/main/example-code";
    var howToSubmit = "https://rachels-courses.gitlab.io/course-commons/reference.html#git-submit";
    var howToUpdate = "https://rachels-courses.gitlab.io/course-commons/reference.html#git-latest";
    
    var sections = [
        {
            "crn" : "11903",
            "section" : "350",
            "method" : "Online only",
            "times" : "Online only",
            "background" : "bg-primary"
        }
    ];
    
    var headerHtml = "";
    headerHtml +=  "<div class='row'>";
    headerHtml += "                    <div class='col-md-9'>";
    headerHtml += "                        <section class='course-information'>";
    headerHtml += "                            <div class='row'>";
    headerHtml += "                                <div class='col-md-8 col-sm-8 row'>";
    headerHtml += "                                    <div class='col-md-12'><p>Course: <strong id='course'>" + course + "</strong></p></div>";
    headerHtml += "                                    <div class='col-md-12'><p>Semester: <strong id='semester'>" + semester + "</strong></p></div>";
    headerHtml += "                                    <div class='col-md-12'><p>Dates: <strong id='dates'>" + dates + "</strong></p></div>";
    headerHtml += "                                    <div class='col-md-12'><p>Instructor: <strong>Rachel Singh <sub>(they/them)</sub></strong></p></div>";
    headerHtml += "                                    <div class='col-md-12'><p>Email: <strong>rsingh13@jccc.edu</strong></p></div>";
    headerHtml += "                                    <div class='col-md-12'><p>Discord: <strong>RachelThePotato#0085</strong> <br><sub>(You can ask me programming questions on Discord)</sub></p></div>";
    headerHtml += "                                </div> <!-- half col -->";
    
    headerHtml += "                                <!-- SECTION INFORMATION START -->"
    headerHtml += "                                <div class='col-md-4 col-sm-4 row'>";
    for ( var i = 0; i < sections.length; i++ )
    {
        headerHtml += "                                    <div class='col-md-12'>";
        headerHtml += "                                        <div class='card " + sections[i]["background"] + "'>";
        headerHtml += "                                            <div class='card-header text-white'>";
        headerHtml += "                                                Section          <strong class='section'>" + sections[i]["section"] + "</strong>";
        headerHtml += "                                            </div>";
        headerHtml += "                                            <div class='card-body bg-light'>";
        headerHtml += "                                                <p>CRN:             <strong class='crn'>" + sections[i]["crn"] + "</strong></p>";
        headerHtml += "                                                <p>Method:          <strong class='method'>" + sections[i]["method"] + "</strong></p>";
        headerHtml += "                                                <p>Times:           <strong class='times'>" + sections[i]["times"] + "</strong></p>";
        headerHtml += "                                            </div>";
        headerHtml += "                                        </div>";
        headerHtml += "                                    </div> <!-- section -->";
    }
    headerHtml += "                                </div> <!-- half col -->      ";  
    headerHtml += "                                <!-- SECTION INFORMATION END -->"
                    
    headerHtml += "                            </div> <!-- row -->";
    headerHtml += "                        </section> <!-- course-information -->";
    headerHtml += "                    </div> <!-- course-information col -->";
    headerHtml += "                    <div class='col-md-3'>";
    headerHtml += "                        <section class='quick-nav border border-primary bg-light'>";
    headerHtml += "                            <div class='container-fluid' id='nav-box'>";
    headerHtml += "                            </div>";
    headerHtml += "                        </section> <!-- quick-nav -->";
    headerHtml += "                    </div> <!-- quick-nav col -->";
    headerHtml += "                </div> <!-- row -->";
    headerHtml += "                <hr>";
    
    $( "#header-container" ).append( headerHtml );
    
    var navHtml = "<ul>"+
                    "<li><a href='" + homepage + "' id='homepage'>Homepage 🏠</a></li>"+
                    "<li><a href='" + syllabus + "'>Syllabus 📝</a></li>"+
                    "<li><a href='" + review + "'>Class archive 🗄️</a></li>"+
                    //"<li><a href='" + videoArchive + "'>Video archive 📺</a></li>"+
                    "<li><a href='" + courseRepository + "'>Course Repository 🗂️</a></li>"+
                    "<li><a href='" + courseStudentRepository + "'>Student's Repository 🧑🏽‍🎓</a></li>"+
                    "<li><a href=\"" + courseTextbook + "\">Class textbook 📘</a></li>"+
                    //"<li><a href='" + alternativeTextbook + "'>Alternative free textbook 📘</a></li>"+
                    "<li><a href='" + quickReference + "'>Quick reference 🔍</a></li>"+
                    "<li><a href='" + howToSubmit + "'>How to submit assignments 📁</a></li>"+
                    "<li><a href='" + howToUpdate + "'>How to update starter code 🔄</a></li>"+
                    "<li><a href='" + exampleCode + "'>Example code 💾</a></li>"+
                    //"<li><a href='" + gather + "'>Gather room 🎮</a></li>"+
                    //"<li><a href='" + miscellaneous + "'>Miscellaneous 🗑️</a></li>"+
                    "<li><a href='" + courseCatalogPage + "'>Course catalog 📖</a></li>"+
                    "<li><a href='http://canvas.jccc.edu'>Canvas <img src='web-assets/graphics/canvas-icon.png'></a></li>"+
                "</ul>";
    
    $( "#nav-box" ).append( navHtml );
    
    $( "#class-info" ).html( course );
} 
);
