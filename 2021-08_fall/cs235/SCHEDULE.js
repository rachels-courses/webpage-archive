// CS 235
$( document ).ready( function() {
    
    firstMondayOfClass = new Date( 2021, 07, 23 );
    
    // JS really doesn't store this info?
    freakingMonths = [ "Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
    
    
    
    // Old
    scheduleInfo = [
        { 
            "week"  : "1",
            "unit"  : "Unit 1",
            "link"  : "unit01.html",
            "topic" : "Introduction and Setup",
            "notes" : "Jan 19: First day of spring credit classes",
            "due" : [
                { "item" : "Notes", "array" : [
                    { "name" : "If you have trouble setting anything up, send me an email via Canvas! We can work together via email or schedule a Zoom meeting to step through everything.", "type" : "due" }
                ] }
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Spring 2021 Tour", "type" : "video", "url" : "http://lectures.moosader.com/cs235/2021-01_Spring/CS%20235%20-%20Lecture%2000%20-%20Introduction.mp4" },
                    ]
                },
                //{
                    //"item" : "📘 Reading", "array" : [
                        //{ "name" : "asdfasdf", "type" : "reading", "where" : "Chapter 1: Introduction" },
                    //]
                //},
            ],
            "exercises" : [
                { "item" : "01Ae 🏋️ Getting started", "array" : [
                    { "name" : "Assignment 01Ae",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "01Be 🏋️ Tools Setup ", "array" : [
                    { "name" : "01B Exercise",           "type" : "exercise", "docs" : "01Be_ToolsSetup.html", "video" : "http://lectures.moosader.com/cs235/01Be_Walkthru_CS235CS250.mp4" },
                ]},
            ],
            "quizzes" : [
                { "item" : "01Aq ❓ Availability - Survey", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "01Bq ❓ Tools Setup - Survey", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
                { "item" : "01-02p 💻 Project: CS 200 review", "array" : [{ "name" : "", "type" : "project", 
                    "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/01-02p%20-%20CS%20200%20Review", 
                    "video" : "http://lectures.moosader.com/cs235/2021-01_Spring/01-02p_Walkthru.mp4" },
                ]},
            ],
        },
        
        { 
            "week"  : "2",
            "unit"  : "Unit 2",
            "link"  : "unit02.html",
            "topic" : "Debugging and Testing",
            "notes" : "Jan 26: Last day to drop a full-semester course and receive a 100-percent refund.",
            "due" : [
                { "item" : "Things due", "array" : [
                    { "name" : "01Ae 🏋️ Getting started", "type" : "due" },
                    { "name" : "01Be 🏋️ Tools Setup", "type" : "due" },
                    { "name" : "01Aq ❓ Availability - Survey", "type" : "due" },
                    { "name" : "01Bq ❓ Tools Setup - Survey", "type" : "due" },
                ] }
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lecture - Testing and debugging exercises", "array" : [
                        { "name" : "Jan 27 lecture", "type" : 
                            "video", "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-01-26_cs250_lecture.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "Chapter 19: Testing", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter19-testing.pdf" },
                        //{ "name" : "Chapter 20: Debugging", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter20-debugging.pdf" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "02Ae 🏋️ Debugging", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs235/-/blob/main/Unit%2002%20-%20Debugging%20and%20Testing/02Ae%20-%20Debugging/Debugging.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/02Ae%20-%20Debugging/Debugging_Lab", 
                    "video" : "http://lectures.moosader.com/cs235/2021-02-09_cs235_debugging_lab.mp4" }
                ]},
                { "item" : "02Be 🏋️ Testing", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs235/-/blob/main/Unit%2002%20-%20Debugging%20and%20Testing/02Be%20-%20Testing/Testing.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/02Be%20-%20Testing/TestingLab_Student", 
                    "video" : "http://lectures.moosader.com/cs235/2021-02-09_cs235_testing_lab.mp4" }
                ]},
            ],
            "quizzes" : [
            ],
            "projects" : [
                { "item" : "01-02p 💻 Project: CS 200 review", "array" : [{ "name" : "", "type" : "project", 
                    "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/01-02p%20-%20CS%20200%20Review", 
                    "video" : "http://lectures.moosader.com/cs235/2021-01_Spring/01-02p_Walkthru.mp4" },
                ]},
            ],
            "exams" : [
            ]
        },
        
        { 
            "week"  : "3",
            "unit"  : "(Break)",
            "link"  : "",
            "topic" : "(Break) Units 1 - 2  finsh up. No new content this week.",
            "notes" : "<strong>All Unit 1, 2, assignments due</strong><br><strong>Student check in #1</strong>",
            "due" : [
                { "item" : "Things due", "array" : [
                    { "name" : "01Ae 🏋️ Getting started", "type" : "due" },
                    { "name" : "01Be 🏋️ Tools Setup ", "type" : "due" },
                    { "name" : "02Ae 🏋️ Debugging", "type" : "due" },
                    { "name" : "02Be 🏋️ Testing", "type" : "due" },
                    { "name" : "01Aq ❓ Availability - Survey", "type" : "due" },
                    { "name" : "01Bq ❓ Tools Setup - Survey", "type" : "due" },
                    { "name" : "01-02p 💻 Project: CS 200 review", "type" : "due" },
                ] },
            ],
        },
        
        { 
            "week"  : "4",
            "unit"  : "Unit 3",
            "link"  : "unit03.html",
            "topic" : "Templates, exception handling",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Templates", "type" : 
                            "video", "url" : "http://lectures.moosader.com/cs200/26-Templates.mp4" },
                        { "name" : "Exception handling", "type" : 
                            "video", "url" : "http://lectures.moosader.com/cs200/06-Exceptions.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf", "type" : 
                            "reading", "name" : "(CS200/235 book) Chapter 12: Advanced Object Oriented Programming (12.2 Templates)" },
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter15-exceptions.pdf", "type" : 
                            "reading", "name" : "(CS200/235 book) Chapter 15: Exception handling with try/catch" },
                    ]
                },
                {
                    "item" : "🏫 Class archive", "array" : [
                        { "name" : "🎥 Templates and exception handling (CS 250)", "type" : "video", "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-02_cs250_templates_and_exceptions.mp4" },
                    ]
                },
                {
                    "item" : "💾 Example code directory", "array" : [
                        { "name" : "Example code - CS 235", 
                            "type" : "code", 
                            "url" : "https://gitlab.com/rachels-courses/cs235-object-oriented-programming-with-cpp/-/tree/main/example-code" },
                            
                        { "name" : "Example code - CS 250", 
                            "type" : "code", 
                            "url" : "https://gitlab.com/rachels-courses/cs250-basic-data-structures-with-cpp/-/tree/main/example-code" },
                    ]
                },
                {
                    "item" : "📓 Optional reading", "array" : [
                        { "where" : "https://www.programiz.com/cpp-programming/templates", 
                            "type" :  "reading", 
                            "name" : "Templates - programiz" },
                        { "where" : "https://www.tutorialspoint.com/cplusplus/cpp_templates.htm", 
                            "type" :  "reading", 
                            "name" : "Templates - tutorials point" },
                        { "where" : "http://www.cplusplus.com/doc/oldtutorial/templates/", 
                            "type" :  "reading", 
                            "name" : "Templates - cplusplus.com" },
                        { "where" : "https://www.tutorialspoint.com/cplusplus/cpp_exceptions_handling.htm", 
                            "type" :  "reading", 
                            "name" : "Exceptions - tutorials point" },
                        { "where" : "https://www.cplusplus.com/doc/tutorial/exceptions/", 
                            "type" :  "reading", 
                            "name" : "Exceptions - cplusplus.com" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "03Ae 🏋️ Templates", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs235/-/blob/main/Unit%2003%20-%20Templates%20and%20Exceptions/03Ae%20-%20Templates/TemplatesLab.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2003%20-%20Templates%20and%20Exceptions/03Ae%20-%20Templates", 
                    "video" : "#" }
                ]},
                { "item" : "03Be 🏋️ Exception handling", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs235/-/blob/main/Unit%2003%20-%20Templates%20and%20Exceptions/03Be%20-%20Exception%20handling/ExceptionsLab.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2003%20-%20Templates%20and%20Exceptions/03Be%20-%20Exception%20handling", 
                    "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "03Aq ✅ Templates", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "03Bq ✅ Exception handling ", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "exams" : [
                { "item" : "03t 💯 Templates and Exception handling", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        
        { 
            "week"  : "5",
            "unit"  : "Unit 4",
            "link"  : "unit04.html",
            "topic" : "STL Vector, STL List, STL Map",
            "notes" : "Feb 15: Application deadline to apply for spring graduation. Last day to drop a full-semester course without a withdrawal “W” on the student’s permanent record.",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "C++ Standard Template Library", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs200/27-STL.mp4" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "04Ae 🏋️ STL Vector", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "04Ae_STLVector.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "04Be 🏋️ STL List", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "04Be_STLList.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "04Ce 🏋️ STL Map", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "04Ce_STLMap.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "04De 🏋️ STL Application", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "04De_STLTogether.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
            ],
            "projects" : [
                { "item" : "05p 💻 Project: Templates and Memory", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "05p_SmartDynamicArray.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
        },
        
        { 
            "week"  : "6",
            "unit"  : "Unit 5",
            "link"  : "unit05.html",
            "topic" : "Pointers, Memory management, Dynamic allocation review",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Pointers", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs200/16-Pointer.mp4" },
                        { "name" : "Memory management", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs200/17-Memory-Management.mp4" },
                        { "name" : "Dynamic arrays", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs200/18-Dynamic-Arrays.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter14-pointers_and_memory.pdf", "type" : "reading", 
                            "name" : "Chapter 14: Pointers, memory management, and dynamic variables and arrays" },
                    ]
                },
                {
                    "item" : "🏫 Class archive", "array" : [
                        { "name" : "🎥 Week overview, project information", "type" : "video", 
                            "url" : "// http://lectures.moosader.com/cs235/2021-01_Spring/2021-02-16_cs250_lecture.mp4" },
                    ]
                },
                {
                    "item" : "📓 Optional reading", "array" : [
                        { "where" : "https://www.tutorialspoint.com/cplusplus/cpp_pointers.htm", 
                            "type" :  "reading", 
                            "name" : "C++ Pointers - tutorialspoint.com" },
                        { "where" : "https://www.cplusplus.com/doc/tutorial/pointers/", 
                            "type" :  "reading", 
                            "name" : "C++ Pointers - cplusplus.com" },
                        { "where" : "https://www.geeksforgeeks.org/pointers-c-examples/", 
                            "type" :  "reading", 
                            "name" : "C++ Pointers - geeksforgeeks.com" },
                            
                        { "where" : "https://www.tutorialspoint.com/cplusplus/cpp_dynamic_memory.htm", 
                            "type" :  "reading", 
                            "name" : "Dynamic Memory - tutorialspoint.com" },
                        { "where" : "http://www.cplusplus.com/doc/tutorial/dynamic/", 
                            "type" :  "reading", 
                            "name" : "Dynamic Memory - cplusplus.com" },
                        { "where" : "https://www.geeksforgeeks.org/new-and-delete-operators-in-cpp-for-dynamic-memory/", 
                            "type" :  "reading", 
                            "name" : "Dynamic Memory - geeksforgeeks.com" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "05Ae 🏋️ Pointers review", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "05Ae_PointersReview.html", "starter" : "https://gitlab.com/rachels-courses/cs235/-/tree/main/Unit%2005%20-%20Pointers%2C%20Memory%20management%2C%20Dynamic%20allocation/05Ae%20-%20Pointers%20review/PointersLabStudent", "video" : "#" }
                ]},
                { "item" : "05Be 🏋️ Dynamic memory allocation review", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "05Be_MemAllocReview.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "05Ce 🏋️ Utilizing pointers and memory management", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "05Ce_UtilizingPointersAndMemory.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "05Aq ✅ Pointers review", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "05Bq ✅ Dynamic memory allocation review", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
                { "item" : "05p 💻 Project: Templates and Memory", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "05p_SmartDynamicArray.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
        },
        { 
            "week"  : "7",
            "unit"  : "(Break)",
            "link"  : "",
            "topic" : "(Break) Units 3 - 5 finish up. No new content this week.",
            "notes" : "<strong>All Unit 3, 4, 5 assignments due</strong><br><strong>Student check in #2</strong>",
            "due" : [
            ],
        },
        { 
            "week"  : "8",
            "unit"  : "",
            "link"  : "",
            "topic" : "(Break)",
            "notes" : "March 8-14 Spring Break. Classes not in session. College offices open Monday through Friday.",
            "due" : [
            ],
        },
        { 
            "week"  : "9",
            "unit"  : "Unit 6",
            "link"  : "unit06.html",
            "topic" : "Static members, Copy constructor",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter10-oop1.pdf", "type" : "reading", 
                            "name" : "Chapter 10: Basic Object Oriented Programming <br>(10.3: Classes)" },
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf", "type" : "reading", 
                            "name" : "Chapter 11: Intermediate Object Oriented Programming <br>(11.6: Copy constructors)" },
                    ]
                },
                {
                    "item" : "🎥 Coding examples", "array" : [
                        { "name" : "Copy constructors", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs235/example-code/cs235-copy-constructor.mp4" },
                        { "name" : "Static members", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs235/example-code/cs235-static-members.mp4" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "06Ae 🏋️ Static members", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "06Ae_StaticMembers.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "06Be 🏋️ Copy constructors", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "06Be_CopyConstructor.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "06Bq ✅ Copy constructors", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "06Aq ✅ Static members", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
            ],
            "exams" : [
                { "item" : "06-08t 💯 Polymorphism, Static members, Copy constructor, Friends, Overloaded operators", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        { 
            "week"  : "10",
            "unit"  : "Unit 7",
            "link"  : "unit07.html",
            "topic" : "Class inheritance, Polymorphism",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Class inheritance", "type" : "video", "url" : "http://lectures.moosader.com/cs200/15-Inheritance.mp4" },
                        { "name" : "Polymorphism", "type" : "video", "url" : "http://lectures.moosader.com/cs200/22-Polymorphism.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "whee" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf", "type" : "reading", 
                            "name" : "Chapter 11: Intermediate Object Oriented Programming <br>(11.6: Inheritance)" },
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf", "type" : "reading", 
                            "name" : "Chapter 12: Advanced Object Oriented Programming <br>(12.1: Polymorphism)" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "07Ae 🏋️ Class inheritance review", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "07Ae_ClassInheritance.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "07Be 🏋️ Polymorphism", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "07Be_Polymorphism.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                //{ "item" : "07Aq ✅ Class inheritance", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
                //{ "item" : "07Bq ✅ Polymorphism", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
            ],
            "projects" : [
            ],
            "exams" : [
                { "item" : "06-08t 💯 Polymorphism, Static members, Copy constructor, Friends, Overloaded operators", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        { 
            "week"  : "11",
            "unit"  : "Unit 8",
            "link"  : "unit08.html",
            "topic" : "Friends, Overloaded operators",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Friends", "type" : "video", "url" : "http://lectures.moosader.com/cs200/19-Friends.mp4" },
                        { "name" : "Operator overloading", "type" : "video", "url" : "http://lectures.moosader.com/cs200/20-Operator-Overloading.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "Chapter 11: Intermediate Object Oriented Programming <br>(11.3: Friends, 11.4: Operator overloading)", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "08Ae 🏋️ Friends", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "08Ae_Friends.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "08Be 🏋️ Overloaded operators", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "08Be_OperatorOverloading.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                //{ "item" : "08Aq ✅ Friends", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
                //{ "item" : "08Bq ✅ Overloaded operators", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
            ],
            "projects" : [
                { "item" : "09p 💻 Project: Object oriented programming ", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "09p_Project.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
            "exams" : [
                { "item" : "07-08t 💯 Static members, Copy constructor, Friends, Overloaded operators", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        { 
            "week"  : "12",
            "unit"  : "Unit 9",
            "link"  : "unit09.html",
            "topic" : "Recursion",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "(CS200/235 book) Chapter 17: Recursion", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter17-recursion.pdf" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "09Ae 🏋️ Recursion", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "09Ae_Recursion.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                //{ "item" : "09Aq ✅ Recursion", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
            ],
            "projects" : [
                { "item" : "09p 💻 Project: Object oriented programming ", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "09p_Project.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
        },
        { 
            "week"  : "13",
            "unit"  : "(Break)",
            "link"  : "",
            "topic" : "(Break) Units 6 - 9 finish up. No new content this week.",
            "notes" : "<strong>All Unit 6, 7, 8, 9 assignments due</strong><br><strong>Student check in #3</strong><br>April 15 Last day to request a pass/fail grade option or to withdraw with a “W” from a full semester course.",
            "due" : [
            ],
        },
        { 
            "week"  : "14",
            "unit"  : "Unit 10",
            "link"  : "unit10.html",
            "topic" : "Linking libraries and Student Project",
            "notes" : "",
            "due" : [
            ],
            "lectures" : [
                //{
                    //"item" : "🎥 Video lectures", "array" : [
                        //{ "name" : "wip", "type" : 
                            //"video", "url" : "#" },
                    //]
                //},
                //{
                    //"item" : "📘 Reading", "array" : [
                        //{ "name" : "asdfasdf", "type" : "reading", "where" : "wip" },
                    //]
                //},
            ],
            "exercises" : [
                { "item" : "10Ae 🏋️ Linking libraries", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "10Ae_LinkingLibraries.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            //"quizzes" : [
                //{ "item" : "10Aq ✅ Linking libraries", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
            //],
            "projects" : [
                { "item" : "10p 💻 Project: Student project ", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "10p_StudentProject.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
        },
        { 
            "week"  : "15",
            "unit"  : "Unit 10",
            "link"  : "unit10.html",
            "topic" : "Linking libraries and Student Project",
            "notes" : "",
        },
        { 
            "week"  : "16",
            "unit"  : "Unit 10",
            "link"  : "unit10.html",
            "topic" : "Linking libraries and Student Project",
            "notes" : "",
        },
        { 
            "week"  : "17",
            "unit"  : "",
            "link"  : "",
            "topic" : "No final exam",
            "notes" : "<strong>Student project due.</strong><br>May 11-17 Scheduled final exams for students.",
            "due" : [
            ],
            "lectures" : [
            ],
            "labs" : [
            ],
            "quizzes" : [
            ],
            "projects" : [
            ],
            "exams" : [
            ]
        },
        { 
            "week"  : "18",
            "unit"  : "",
            "link"  : "",
            "topic" : "",
            "notes" : "May 18 Grades entered online by professors by 5 p.m.<br><br>May 20 Grades available to students by noon on the web."
        }
    ];
    
    // Insert into specific fields
    if ( $( "#index-data" ) != null )
    {
        //var unitIndex = parseInt( $( "#index-data" ).val() );
        //
        //$( ".insert-unit" ).html( scheduleInfo[ unitIndex ]["unit"] );
        //$( ".insert-topics" ).html( scheduleInfo[ unitIndex ]["topic"] );
    }
} );
