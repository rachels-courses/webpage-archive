// CS 210
$( document ).ready( function() {
    
    firstMondayOfClass = new Date( 2022, 01, 18 );
    
    // JS really doesn't store this info?
    freakingMonths = [ "Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
    
    
    weekInfo = [
        {   "week" : "1", 
            "units" : [ "WIP" ],//[ "Unit 1" ],
            "notes" : "WIP", // "Aug 23: First day of fall semester<br>23-24 Late registration: after these dates, students must be enrolled in a course no later than the first day the course meets."
        },
        {   "week" : "2", 
            "units" : [ "WIP" ],//[ "Unit 2" ],
            "notes" : "WIP", // "Aug 30: Last day to drop a full-semester course and receive a 100 percent refund."
        },
        {   "week" : "3", 
            "units" : [ "WIP" ],//[ "Unit 3" ],
            "notes" : "WIP", // "Sep 6: Labor Day holiday. College closed."
        },
        {   "week" : "4", 
            "units" : [ "WIP" ],//[ "Unit 4" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "5", 
            "units" : [ "WIP" ],//[ "Unit 5" ],
            "notes" : "WIP", // "Sep 20: Last day to drop a full-semester course without a withdrawal “W” on the student’s permanent record. Deadline is 11 p.m. for drops completed on the web."
        },
        {   "week" : "6", 
            "units" : [ "WIP" ],//[ "Break 1" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "7", 
            "units" : [ "WIP" ],//[ "Unit 6" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "8", 
            "units" : [ "WIP" ],//[ "Unit 7" ],
            "notes" : "WIP", // "Oct 15: Application deadline for fall graduation."
        },
        {   "week" : "9", 
            "units" : [ "WIP" ],//[ "Break 2" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "10", 
            "units" : [ "WIP" ],//[ "Unit 8" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "11", 
            "units" : [ "WIP" ],//[ "Unit 9" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "12", 
            "units" : [ "WIP" ],//[ "Break 3" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "13", 
            "units" : [ "WIP" ],//[ "Unit 10" ],//"Unit 11" ],
            "notes" : "WIP", // "Nov 15: Last day to request a pass/fail grade option or to withdraw with a 'W' from a full-semester course."
        },
        {   "week" : "14", 
            "units" : [ "WIP" ],//[ "Unit 12" ],
            "notes" : "WIP", // "Nov 24-26: Thanksgiving Day holiday. Classes not in session. College offices closed."
        },
        {   "week" : "15", 
            "units" : [ "WIP" ],//[ "Unit 12" ],
            "notes" : "WIP", // ""
        },
        {   "week" : "16", 
            "units" : [ "WIP" ],//[ "Unit 12" ],
            "notes" : "WIP", // "Dec 7-13: Scheduled final exams for students. See <a href='https://www.jccc.edu/calendars/_files/fall-final-exam-schedule.pdf'>Fall 2021 Final Exam Schedule</a>."
        },
        {   "week" : "17", 
            "units" : [ "WIP" ],//[ "SEMESTER OVER" ],
            "notes" : "WIP", // "Dec 14: Grades entered online by professors by 5 p.m.<br>Dec 16: Grades available to students by noon on the web."
        },
    ];
} );
