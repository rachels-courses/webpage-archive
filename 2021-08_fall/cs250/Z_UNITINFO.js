// CS 250
$( document ).ready( function() {

unitInfo = {
  "Unit 1" : {
    "link" : "unit01.html",
    "topic" : "Introduction and setup",
    "reminders" : [
    "If you have trouble setting anything up, send me an email via Canvas! We can work together via email or schedule a Zoom meeting to step through everything."
    ],
    //"lectures" :        [ 
      //{ "name" : "", "url" : "" },
      //{ "name" : "", "url" : "" },
    //],
    "reading" :         [   
      { "name" : "Testing",       "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter19-testing.pdf" },
      { "name" : "Debugging",     "url" : "" },
    ],
    "archives" :        [   
      { "name" : "Aug 23rd class", "url" : "http://lectures.moosader.com/cs250/2021-08_Fall/2021-08-23_cs250_lecture_intro.mp4" },
    ],
    "exercises" :       [   
      { "key" : "u01eA", 
          "name" : "Git/GitLab Setup (part 1)",            
          "docs" : "u01eA_GitSetup.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966795" ,
          "due" : "Sept 1",
          "end" : "Sept 4",
      },
      { "key" : "u01eA", 
          "name" : "Git/GitLab Setup (part 2)",            
          "docs" : "u01eA_GitSetup.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966796" ,
          "due" : "Sept 1",
          "end" : "Sept 4",
      },
      { "key" : "u01eB", 
          "name" : "IDE Setup",                            
          "docs" : "u01eB_IDESetup.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966797" ,
          "due" : "Sept 1",
          "end" : "Sept 4",
      },
      { "key" : "u01eC", 
          "name" : "Debugging Tools",                      
          "docs" : "u01eC_DebuggingTools.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966798" ,
          "due" : "Sept 1",
          "end" : "Sept 4",
      },
      { "key" : "u01eD", 
          "name" : "Testing",                              
          "docs" : "u01eD_Testing.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966799" ,
          "due" : "Sept 1",
          "end" : "Sept 4",
      },
    ],
    "quizzes" :         [   
      { "key" : "u01sA", 
          "name" : "👋 Introductions! ",                   
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966773" ,
          "due" : "Aug 28",
          "end" : "",
      },
      { "key" : "u01sB", 
          "name" : "📜 Syllabus highlights ",              
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966779" ,
          "due" : "Aug 28",
          "end" : "",
      },
      { "key" : "u01sC", 
          "name" : "‼️ Attendance Quiz ",                  
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966766" ,
          "due" : "Aug 28",
          "end" : "Aug 29",
      },
      { "key" : "u01sD", 
          "name" : "❓ Availability - Survey  ",           
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966770" ,
          "due" : "Aug 28",
          "end" : "",
      },
    ],
  },

  "Unit 2" : {
    "link" : "unit02.html",
    "topic" : "C++ Review",
    "reading" :         [   
      { "name" : "Resource list", "url" : "https://rachels-courses.gitlab.io/webpage/ref/review.html" },
    ],
    "archives" :        [   
      { "name" : "Aug 30th class", "url" : "http://lectures.moosader.com/cs250/2021-08_Fall/2021-08-30_cs250_lecture_review.mp4" },
    ],
    "exercises" :       [   
      { "key" : "u02eA", 
          "name" : "C++ Review",            
          "docs" : "u02eA_CPPReview.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/989799", 
            "due" : "Sept 8",
            "end" : "Sept 15",
      },
    ],
    "quizzes" :         [   
      { "key" : "u02rA", 
          "name" : "⭐ Review: C++ Basics",                   
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966775",
          "due" : "Sept 19",
          "end" : "Sept 26", 
      },
      { "key" : "u02rB", 
          "name" : "⭐ Review: Control flow",                 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966785", 
          "due" : "Sept 19",
          "end" : "Sept 26",
      },
      { "key" : "u02rC", 
          "name" : "⭐ Review: Functions",                    
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966784", 
          "due" : "Sept 19",
          "end" : "Sept 26",
      },
      { "key" : "u02rD", 
          "name" : "⭐ Review: Arrays",                       
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966783", 
          "due" : "Sept 19",
          "end" : "Sept 26",
      },
      { "key" : "u02rE", 
          "name" : "⭐ Review: Classes",                      
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966780", 
          "due" : "Sept 19",
          "end" : "Sept 26",
      },
      { "key" : "u02rF", 
          "name" : "⭐ Review: Pointers and memory",    
          "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229582",
          "due" : "Sept 19",
          "end" : "Sept 26", 
      },
      { "key" : "c1",  
          "name" : "🧑‍🏫 Check-in (September)",                   
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966790", 
          "due" : "Sept 1",
          "end" : "Sept 30",
      },
    ],
    "tech-literacy" :   [   
      { "key" : "u02tl", "name" : "Case study: GitHub AI Copilot",      
          "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229585",
          "due" : "Sept 8",
          "end" : "Sept 15",
      },
    ],
  },

  "Unit 3" : {
    "link" : "unit03.html",
    "topic" : "Templates, exception handling",
    "lectures" :        [   
      { "name" : "Templates",                                             "url" : "http://lectures.moosader.com/cs200/26-Templates.mp4" },
      { "name" : "Exception handling",                                    "url" : "http://lectures.moosader.com/cs200/06-Exceptions.mp4" },
    ],
    "reading" :         [   
      { "name" : "CC - Chapter 12: Advanced Object Oriented Programming (12.2 Templates)",  "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf" },
      { "name" : "CC - Chapter 15: Exception handling with try/catch",                      "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter15-exceptions.pdf" },
    ],
    "exercises" :       [   
      { "key" : "u03eA", "name" : "Templates",                            
          "docs" : "u03eA_Templates.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966800",
          "due" : "Sept 15",
          "end" : "Sept 22", 
      },
      { "key" : "u03eB", "name" : "Exception handling",             
        "docs" : "u03eB_Exceptions.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966801",
          "due" : "Sept 15",
          "end" : "Sept 22", 
      },
    ],
    "quizzes" :         [   
      { "key" : "u03qA", "name" : "Templates",                            
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966763",
          "due" : "Sept 15",
          "end" : "Sept 22", 
      },
      { "key" : "u03qB", "name" : "Exception handling",                  
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966777",
          "due" : "Sept 15",
          "end" : "Sept 22", 
      },
    ],
    "exams" :           [   
      { "key" : "t1",  
        "name" : "Test 1: Basic Data Structures", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229594" 
      },
    ]
  },

  "Unit 4" : {
    "link" : "unit04.html",
    "topic" : "Intro to Data Structures",
    "lectures" :        [   
      { "name" : "Intro to Data Structures",                              "url" : "http://lectures.moosader.com/cs250/cs250-00-introduction.mp4" },
    ],
    "reading" :         [   
      { "name" : "DS - Chapter 1: Intro to Data Structures",              "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-chapter1-intro_to_data_structures.pdf" },
      { "name" : "DS - Chapter 2: Our first data structures",             "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-first-data-structure.pdf" },
    ],
    "exercises" :       [   
      { "key" : "u04eA", "name" : "Intro to Data Structures (Paper)",      "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit04/u04eA_IntroToDataStructures/IntroDataStructures.pdf", "376" : "https://canvas.jccc.edu/courses/49331/assignments/966802" },
      { "key" : "u04eB", "name" : "Coding a Fixed-length array structure", "docs" : "u04eB_FixedArrayStructure.html", "376" : "https://canvas.jccc.edu/courses/49331/assignments/966803" },
      { "key" : "u04eC", "name" : "Coding a Dynamic array structure",      "docs" : "u04eC_DynamicArrayStructure.html", "376" : "https://canvas.jccc.edu/courses/49331/assignments/966804" },
    ],
    //"quizzes" :         [   
      //{ "key" : "u04qA", "name" : "(WIP) Intro to Data Structures",             "376" : "https://canvas.jccc.edu/courses/49331/assignments/966776" },
    //],
    "tech-literacy" :   [   
      { "key" : "u04tl", "name" : "Case study: Rohrer and NFTs",    "376" : "https://canvas.jccc.edu/courses/49331/assignments/966787" },
    ],
    "exams" :           [   
      { "key" : "t1",  
        "name" : "Test 1: Basic Data Structures", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229594" 
      },
    ]
    },

    "Unit 5" : {
    "link" : "unit05.html",
    "topic" : "Linked Lists",
    "lectures" :        [   
      { "name" : "Linked Lists", "url" : "https://www.youtube.com/embed/JIrgGBOZGeY" },
    ],
    "archives" :        [   
      { "name" : "Class archive", "url" : "https://youtu.be/6FwhiameYPI" },
    ],
    "reading" :         [   
      { "name" : "DS - Chapter 3: Linked Lists",                          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-linked_lists.pdf" },
    ],
    "exercises" :       [   
      { "key" : "u05eA", 
        "name" : "Intro to Linked Lists (Paper)",        
        "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit05/u05eA_IntroLinkedList/IntroLinkedLists.pdf", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966806",      
        "due" : "Oct 6",
        "end" : "Oct 13",
        },
        
      { "key" : "u05eB", 
        "name" : "Coding a Linked List",                 
        "docs" : "u05eB_LinkedListStructure.html", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966805" ,
          "due" : "Oct 6",
          "end" : "Oct 13",
        },
    ],
    "quizzes" :         [   
      { "key" : "u05qA", "name" : "Linked Lists",                         
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966786",
          "due" : "Oct 6",
          "end" : "Oct 13",
      },
      
      { "key" : "d1", "name" : "Debugging - Basics",
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/1004541",
          "due" : "Oct 6",
          "end" : "Oct 13",
      },
    ],
    "exams" :           [   
      { "key" : "t1",  
        "name" : "Test 1: Basic Data Structures", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229594" 
      },
    ]
  },

  "Unit 6" : {
    "link" : "unit06.html",
    "topic" : "Stacks and Queues",
    "lectures" :        [   
      { "name" : "Stacks and Queues", "url" : "https://www.youtube.com/watch?v=UdPRKHdIc8k" },
    ],
    "archives" :        [   
      { "name" : "Class archive", "url" : "https://www.youtube.com/watch?v=pF3EIL6G2AM" },
    ],
    "reading" :         [   
      { "name" : "DS - Chapter 4: Queues",                                "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-queue.pdf" },
      { "name" : "DS - Chapter 5: Stacks",                                "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-stack.pdf" },
    ],
    "exercises" :       [   
      { "key" : "u06eA", "name" : "Intro to Stacks and Queues (Paper)",   
        "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit06/u06eA_IntroStackQueue/IntroStacksQueues.pdf", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966807",
        "due" : "Oct 13",
        "end" : "Oct 20", 
        },
      { "key" : "u06eB", "name" : "Coding Stacks and Queues",     
        "docs" : "u06eB_StacksAndQueues.html", "376" : "https://canvas.jccc.edu/courses/49331/assignments/966810" ,
        "due" : "Oct 13",
        "end" : "Oct 20", 
        },
    ],
    "quizzes" :         [   
      { "key" : "u06qA", "name" : "Stacks and Queues",                   
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966782" 
        },
      { "key" : "c2",  "name" : "🧑‍🏫 Check-in (October)",                 "376" : "https://canvas.jccc.edu/courses/49331/assignments/966791" },
    ],
    "projects" :        [   
      { "key" : "p1", "name" : "Linked List, Queue, Stack project",
        "docs" : "p1_project1.html", 
        "due" : "Oct 22",
        "end" : "Oct 29",
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966794" 
      },
    ],
    "exams" :           [   
      { "key" : "t1",  
        "name" : "Test 1: Basic Data Structures", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229594" 
      },
    ]
    },

    "Unit 7" : {
    "link" : "unit07.html",
    "topic" : "Algorithm efficiency, Big-O notation, Searching and Sorting",
    //"lectures" :        [   
      //{ "name" : "Algorithm efficiency (WIP, Coming soon)", "url" : "#" },
    //],
    "archives" :        [   
      { "name" : "Class archive, Oct 18th", "url" : "https://youtu.be/6yopfVQO5fE" },
    ],
    "reading" :         [   
      { "name" : "CC - Chapter 16: Algorithm Efficiency",                         
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter16-algorithm_efficiency.pdf" },
        
      { "name" : "CC - Chapter 21: Searching and Sorting",                         
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-unorganized2.pdf" },
    ],
    "exercises" :       [   
      { "key" : "u07eA", "name" : "Intro to efficiency and searching/sorting algorithms",   
          "docs" : "u07eA_IntroEfficiencyAndAlgorithms.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966809",
          "due" : "Oct 27",
          "end" : "Nov 3"
        },
      { "key" : "u07eB", "name" : "Observing algorithm efficiency",           
          "docs" : "u07eB_AlgorithmEfficiency.html", "376" : "https://canvas.jccc.edu/courses/49331/assignments/966811" ,
          "due" : "Oct 27",
          "end" : "Nov 3"
        },
    ],
    "quizzes" :         [   
      { "key" : "u07qA", "name" : "(WIP)Algorithm efficiency",                         "376" : "https://canvas.jccc.edu/courses/49331/assignments/966765" },
      
      { "key" : "d2", "name" : "Debugging - Branching",
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/1004558",
          "due" : "Oct 7",
          "end" : "Oct 14",
      },
    ],
    "projects" :        [   
      { "key" : "p1", "name" : "Linked List, Queue, Stack project",
        "docs" : "p1_project1.html", 
        "due" : "Oct 22",
        "end" : "Oct 29",
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966794" 
      },
    ],
    "exams" :           [   
      { "key" : "t2",  
        "name" : "Test 2: Algorithm efficiency, Recursion, Trees", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229589" 
      },
    ]
    },

    "Unit 8" : {
    "link" : "unit08.html",
    "topic" : "Recursion",
    "lectures" :        [   
      { "name" : "Recursion basics",                                             
        "url" : "https://www.youtube.com/watch?v=vmPMZ0PJkFw" },
    ],
    "reading" :         [   
      { "name" : "CC - Chapter 17: Recursion",                            
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter17-recursion.pdf" },
    ],
    "archives" :        [   
      { "name" : "Class archive, Oct 25th", "url" : "https://www.youtube.com/watch?v=NsP4VQHa19g" },
    ],
    "exercises" :       [   
      //{ "key" : "u08eA", "name" : "(WIP) Intro to Recursion",      "docs" : "", "376" : "https://canvas.jccc.edu/courses/49331/assignments/966812" },
      { "key" : "u08eA", "name" : "Recursion in code",                    
          "docs" : "u08eA_Recursion.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966873",
          "due" : "Nov 3",
          "end" : "Nov 11"
          },
    ],
    "quizzes" :         [   
      //{ "key" : "u08qA", "name" : "(WIP) Recursion",                            "376" : "https://canvas.jccc.edu/courses/49331/assignments/966778" },
      
      { "key" : "d3", "name" : "Debugging - Functions",
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/1004566",
          "due" : "Oct 7",
          "end" : "Oct 14",
      },
    ],
    "tech-literacy" :   [   
      { "key" : "u08tl", "name" : "Case study: Activion/Blizzard",  "376" : "https://canvas.jccc.edu/courses/49331/assignments/966788" },
    ],
    "exams" :           [  
      { "key" : "t2",  
        "name" : "Test 2: Algorithm efficiency, Recursion, Trees", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229589" 
      },
    ]
  },

  "Unit 9" : {
    "link" : "unit09.html",
    "topic" : "Trees, binary search trees",
    //"lectures" :        [   
      //{ "name" : "Trees (WIP, Coming soon)", "url" : "#" },
      //{ "name" : "Binary Search Trees (WIP, Coming soon)", "url" : "#" },
    //],
    "reading" :         [   
      { "name" : "DS - Chapter 6: Trees",                                 
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-tree.pdf" },
      { "name" : "DS - Chapter 7: Binary Search Trees",                   
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-binary_search_tree.pdf" },
    ],
    "archives" :        [   
      { "name" : "Nov 1st class", "url" : "https://youtu.be/gch9JwT7Pw4" },
    ],
    "exercises" :       [   
      { "key" : "u09eA", "name" : "Intro to Trees (Paper)",               
        "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit09/u09eA_IntroTrees/IntroTrees.pdf", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966813",
        "due" : "Nov 17",
        "end" : "Nov 24" 
        },
        
      { "key" : "u09eB", "name" : "Intro to Binary Search Trees (Paper)", 
        "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit09/u09eB_IntroBinarySearchTrees/IntroBinarySearchTrees.pdf", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966815",
        "due" : "Nov 17",
        "end" : "Nov 24"
        },
        
      { "key" : "u09eC", "name" : "Coding a Binary Search Tree",          
        "docs" : "u09eC_BinarySearchTree.html", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966814",
        "due" : "Nov 17",
        "end" : "Nov 24"
        },
    ],
    "quizzes" :         [   
      { "key" : "u09qA", "name" : "Trees",                                
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966771",
        "due" : "Nov 17",
        "end" : "Dec 10"
        },
      { "key" : "u09qB", "name" : "Binary Search Trees",                  
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966774",
        "due" : "Nov 17",
        "end" : "Dec 10" 
        },
      { "key" : "c3",  "name" : "🧑‍🏫 Check-in (November)",                 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966792" 
        },
    ],
    "projects" :        [   
      { "key" : "p2", "name" : "Binary Search Trees project",
        "docs" : "p2_project2.html", 
        "due" : "Nov 17",
        "end" : "Nov 24",
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/1019333" 
      },
    ],
    "exams" :           [   
      { "key" : "t2",  
        "name" : "Test 2: Algorithm efficiency, Recursion, Trees", 
        "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229589" 
      },
    ]
  },

  "Unit 10" : {
    "link" : "unit10.html",
    "topic" : "Hash tables",
    "lectures" :        [   
      /*{ "name" : "Hash Tables (WIP, Coming soon)", "url" : "#" },*/
    ],
    "reading" :         [   
      { "name" : "DS - Chapter 8: Hash Tables", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-dictionary.pdf" },
    ],
    "archives" :        [   
      { "name" : "Nov 15th class", "url" : "https://www.youtube.com/watch?v=FJGHphovMQw" },
    ],
    "exercises" :       [   
      { "key" : "u10eA", "name" : "Intro to Hash Tables (Paper)",         
          "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit10/u10eA_IntroHashTable/IntroHashTables.pdf", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966816" 
      },
      { "key" : "u10eB", "name" : "Coding a Hash Table",                  
          "docs" : "u10eB_HashTable.html", 
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966817",
          "due" : "",
          "end" : ""
      },
    ],
    "quizzes" :         [   
      { "key" : "u10qA", "name" : "Hash tables",                          
          "376" : "https://canvas.jccc.edu/courses/49331/assignments/966769" },
    ],
    "tech-literacy" :   [   
      { "key" : "u10tl", "name" : "Case study: Facebook",          
          "376" : "https://canvas.jccc.edu/courses/49331/quizzes/229588" },
    ],
    "projects" :        [   
      { "key" : "p2", "name" : "Binary Search Trees project",
        "docs" : "p2_project2.html", 
        "due" : "Nov 17",
        "end" : "Nov 24",
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/1019333" 
      },
    ],
  }, 

  "Unit 11" : {
    "link" : "unit11.html",
    "topic" : "Heaps and Balanced Search Trees",
    "lectures" :        [   
      //{ "name" : "Heaps (WIP)", "url" : "" },
      //{ "name" : "Balanced Search Trees (WIP)", "url" : "" },
    ],
    "reading" :         [   
      //{ "name" : "(WIP)", "url" : "" },
    ],
    "archives" :        [   
      { "name" : "Nov 22nd class", "url" : "https://www.youtube.com/watch?v=a1AyWJWP2jU" },
    ],
    "exercises" :       [   
      { "key" : "u11eA", "name" : "Intro to Heaps (Paper)",               
        "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit11/u11eA_IntroHeaps/IntroHeaps.pdf", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966818" 
      },
      { "key" : "u11eB", "name" : "Balanced Search Trees (Paper)",        
        "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit11/u11eB_IntroBalancedSearchTrees/IntroBalancedSearchTrees.pdf", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966819" 
      },
    ],
    "projects" :        [   
      { "key" : "pS", "name" : "Student project",                       
        "docs" : "p_StudentProject.html", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/1019739" 
      },
    ],
  },

  "Unit 12" : {
    "link" : "unit12.html",
    "topic" : "Student project",
    "quizzes" :         [
      { "key" : "c4",  "name" : "🧑‍🏫 Check-in (December)",               
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/966793" 
      },
    ],
    "projects" :        [   
      { "key" : "pS", "name" : "Student project",                       
        "docs" : "p_StudentProject.html", 
        "376" : "https://canvas.jccc.edu/courses/49331/assignments/1019739" ,
        "due" : "Dec 9",
        "end" : "Dec 10"
      },
    ],
  },

  "TEMPLATE" : {
    "lectures" :        [   
      { "name" : "", "url" : "" },
      { "name" : "", "url" : "" },
    ],
    "reading" :         [   
      { "name" : "", "url" : "" },
      { "name" : "", "url" : "" },
    ],
    "archives" :        [   
      { "name" : "", "url" : "" },
    ],
    "exercises" :       [   
      { "key" : "", "name" : "", "docs" : "", "376" : "" },
      { "key" : "", "name" : "", "docs" : "", "376" : "" },
    ],
    "quizzes" :         [   
      { "key" : "", "name" : "", "376" : "" },
      { "key" : "", "name" : "", "376" : "" },
    ],
    "tech-literacy" :   [   
      { "key" : "", "name" : "", "docs" : "", "376" : "" },
    ],
    "projects" :        [   
      { "key" : "", "name" : "", "docs" : "", "376" : "" },
    ],
    "exams" :           [   
      { "key" : "", "name" : "", "url" : "" },
    ]
    },
  };

} );
