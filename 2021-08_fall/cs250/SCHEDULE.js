// CS 250
$( document ).ready( function() {
    
    firstMondayOfClass = new Date( 2021, 07, 23 );
    
    // JS really doesn't store this info?
    freakingMonths = [ "Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
    
    scheduleInfo = [
        { 
            "week"  : "1",
            "unit"  : "Unit 1",
            "link"  : "unit01.html",
            "topic" : "Introduction, using source control",
            "notes" : "Aug 23 First day of fall semester.",
            "due" : [
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Spring 2021 Tour", "type" : "video", "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/CS%20250%20-%20Lecture%2000%20-%20Introduction.mp4" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "01Ae 🏋️ Getting started", "array" : [
                    { "name" : "Assignment 01Ae",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Canvas" },
                ]},
                { "item" : "01Be 🏋️ Tools Setup ", "array" : [
                    { "name" : "01B Exercise",           "type" : "exercise", "docs" : "01Be_ToolsSetup.html", "video" : "http://lectures.moosader.com/cs235/01Be_Walkthru_CS235CS250.mp4" },
                ]},
            ],
            "quizzes" : [
                { "item" : "01Aq ❓ Availability - Survey", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "01Bq ❓ Tools Setup - Survey", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
                { "item" : "01-02p 💻 Project: CS 200 review", "array" : [{ "name" : "", "type" : "project", 
                    "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/01-02p%20-%20CS%20200%20Review", 
                    "video" : "http://lectures.moosader.com/cs235/2021-01_Spring/01-02p_Walkthru.mp4" },
                ]},
            ],
            "exams" : [
            ]
        },
        
        { 
            "week"  : "2",
            "unit"  : "Unit 2",
            "link"  : "unit02.html",
            "topic" : "Debugging and testing",
            "notes" : "Aug 30 Last day to drop a full-semester course and receive a 100 percent refund.",
            "due" : [
                { "item" : "Things due", "array" : [
                    { "name" : "01Ae 🏋️ Getting started", "type" : "due" },
                    { "name" : "01Be 🏋️ Tools Setup", "type" : "due" },
                    { "name" : "01Aq ❓ Availability - Survey", "type" : "due" },
                    { "name" : "01Bq ❓ Tools Setup - Survey", "type" : "due" },
                ] }
            ],
            "lectures" : [
                {
                    "item" : "🎥 Video lecture - Testing and debugging exercises", "array" : [
                        { "name" : "Jan 27 lecture", "type" : 
                            "video", "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-01-26_cs250_lecture.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "Chapter 19: Testing", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter19-testing.pdf" },
                        //{ "name" : "Chapter 20: Debugging", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter20-debugging.pdf" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "02Ae 🏋️ Debugging", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2002%20-%20Debugging%20and%20Testing/02Ae%20-%20Debugging/Debugging.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/02Ae%20-%20Debugging/Debugging_Lab", 
                    "video" : "http://lectures.moosader.com/cs235/2021-02-09_cs235_debugging_lab.mp4" }
                ]},
                { "item" : "02Be 🏋️ Testing", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2002%20-%20Debugging%20and%20Testing/02Be%20-%20Testing/Testing.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/02Be%20-%20Testing/TestingLab_Student", 
                    "video" : "http://lectures.moosader.com/cs235/2021-02-09_cs235_testing_lab.mp4" }
                ]},
            ],
            "quizzes" : [
            ],
            "projects" : [
                { "item" : "01-02p 💻 Project: CS 200 review", "array" : [{ "name" : "", "type" : "project", 
                    "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2002%20-%20Debugging%20and%20Testing/01-02p%20-%20CS%20200%20Review", 
                    "video" : "http://lectures.moosader.com/cs235/2021-01_Spring/01-02p_Walkthru.mp4" },
                ]},
            ],
            "exams" : [
            ]
        },
        
        { 
            "week"  : "3",
            "unit"  : "Unit 3",
            "link"  : "unit03.html",
            "topic" : "Templates, exception handling",
            "notes" : "",
            "lectures" : [
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Templates", "type" : 
                            "video", "url" : "http://lectures.moosader.com/cs200/26-Templates.mp4" },
                        { "name" : "Exception handling", "type" : 
                            "video", "url" : "http://lectures.moosader.com/cs200/06-Exceptions.mp4" },
                    ]
                },
                {
                    "item" : "📘 Reading", "array" : [
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf", "type" : 
                            "reading", "name" : "(CS200/235 book) Chapter 12: Advanced Object Oriented Programming (12.2 Templates)" },
                        { "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter15-exceptions.pdf", "type" : 
                            "reading", "name" : "(CS200/235 book) Chapter 15: Exception handling with try/catch" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Templates and exception handling", "type" : "video", "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-02_cs250_templates_and_exceptions.mp4" },
                    ]
                },
                {
                    "item" : "💾 Example code directory", "array" : [
                        { "name" : "Example code", "type" : "code", 
                        "url" : "https://gitlab.com/rachels-courses/cs250-basic-data-structures-with-cpp/-/tree/main/example-code" },
                    ]
                },
                {
                    "item" : "📓 Optional reading", "array" : [
                        { "where" : "https://www.programiz.com/cpp-programming/templates", 
                            "type" :  "reading", 
                            "name" : "Templates - programiz" },
                        { "where" : "https://www.tutorialspoint.com/cplusplus/cpp_templates.htm", 
                            "type" :  "reading", 
                            "name" : "Templates - tutorials point" },
                        { "where" : "http://www.cplusplus.com/doc/oldtutorial/templates/", 
                            "type" :  "reading", 
                            "name" : "Templates - cplusplus.com" },
                        { "where" : "https://www.tutorialspoint.com/cplusplus/cpp_exceptions_handling.htm", 
                            "type" :  "reading", 
                            "name" : "Exceptions - tutorials point" },
                        { "where" : "https://www.cplusplus.com/doc/tutorial/exceptions/", 
                            "type" :  "reading", 
                            "name" : "Exceptions - cplusplus.com" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "03Ae 🏋️ Templates", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2003%20-%20Templates,%20Exception%20handling/03Ae%20-%20Templates/TemplatesLab.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2003%20-%20Templates%2C%20Exception%20handling/03Ae%20-%20Templates/Templates_Lab", 
                    "video" : "#" }
                ]},
                { "item" : "03Be 🏋️ Exception handling", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2003%20-%20Templates,%20Exception%20handling/03Be%20-%20Exception%20handling/ExceptionsLab.pdf", 
                    "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2003%20-%20Templates%2C%20Exception%20handling/03Be%20-%20Exception%20handling/ExceptionsLab_Starter", 
                    "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "03Aq ✅ Templates", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "03Bq ✅ Exception handling ", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "exams" : [
                { "item" : "03t 💯 Templates and Exception handling", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        
        { 
            "week"  : "4",
            "unit"  : "(Break)",
            "link"  : "",
            "topic" : "(Break) Units 1 - 3  finish up. No new content this week.",
            "notes" : "",
            "due" : [
                { "item" : "Things due", "array" : [
                    { "name" : "01Ae 🏋️ Getting started", "type" : "due" },
                    { "name" : "01Be 🏋️ Tools Setup ", "type" : "due" },
                    { "name" : "02Ae 🏋️ Debugging", "type" : "due" },
                    { "name" : "02Be 🏋️ Testing", "type" : "due" },
                    { "name" : "03Ae 🏋️ Templates", "type" : "due" },
                    { "name" : "03Be 🏋️ Exception handling", "type" : "due" },
                    { "name" : "01Aq ❓ Availability - Survey", "type" : "due" },
                    { "name" : "01Bq ❓ Tools Setup - Survey", "type" : "due" },
                    { "name" : "03Aq ✅ Templates", "type" : "due" },
                    { "name" : "03Bq ✅ Exception handling", "type" : "due" },
                    { "name" : "01-02p 💻 Project: CS 200 review", "type" : "due" },
                ] },
            ],
        },
        
        { 
            "week"  : "5",
            "unit"  : "Unit 4",
            "link"  : "unit04.html",
            "topic" : "Intro to data structures, Structure 1: smart dynamic array",
            "notes" : "20 Last day to drop a full-semester course without a withdrawal “W” on the student’s permanent record. Deadline is 11 p.m. for drops completed on the web.",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "Chapter 1: Introduction to Data Structures and Algorithm Analysis", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-chapter1-intro_to_data_structures.pdf?inline=false" },
                    ]
                },
                {
                    "item" : "🎥 Video lectures", "array" : [
                        { "name" : "Intro to Data Structures", "type" : "video", "url" : "http://lectures.moosader.com/cs250/cs250-00-introduction.mp4" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Getting started on the Smart Dynamic Array", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-16_cs250_lecture.mp4" },
                        { "name" : "🎥 Smart Dynamic Array addendum (Feb 23rd)", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-23_cs250_lectureA_smart_dynamic_array_addendum.mp4" },
                    ]
                },
            ],
            "exercises" : [
                { "item" : "04Ae 🏋️ Coding a Smart Dynamic Array - Programming Exercise", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "04Ae_SmartDynamicArray.html", "starter" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2004%20-%20Structure%201%20-%20Intro%20to%20Data%20Structures/04Ae%20-%20Coding%20a%20Smart%20Dynamic%20Array", "video" : "#" }
                ]},
            ],
            "quizzes" : [
            ],
            "projects" : [
            ],
            "exams" : [
            ]
        },
        { 
            "week"  : "6",
            "date"  : "asdf",
            "unit"  : "Unit 5",
            "link"  : "unit05.html",
            "topic" : "Structure 2: Linked lists",
            "notes" : "",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "Chapter 2: Linked Lists", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-linked_lists.pdf" },
                    ]
                },
                {
                    "item" : "🎥 Pre-recorded video lectures", "array" : [
                        { "name" : "Let's talk about LInked Lists (2020 livestream)", 
                            "type" : "video", "url" : "http://lectures.moosader.com/cs250/topics/LinkedLists_2020.mp4" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Smart Dynamic Array addendum", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-23_cs250_lectureA_smart_dynamic_array_addendum.mp4" },
                            
                        { "name" : "🎥 Linked Lists", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-02-23_cs250_lectureB_linked_lists.mp4" },
                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "05Ae 🏋️ Intro to Linked Lists", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2005%20-%20Structure%202%20-%20Linked%20Lists/05Ae%20-%20Intro%20to%20Linked%20Lists/05Ae%20Intro%20to%20Linked%20LIsts.pdf", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "05Be 🏋️ Coding a Linked List", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "05Be_CodingLinkedList.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "05Aq ✅ Linked Lists", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
                { "item" : "06p 💻 Project: Data Structures Application 1 (Linked structures, Stacks, Queues) ", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "06p_ApplyingDataStructures.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
            "exams" : [
                { "item" : "05-06t 💯 Intro to data structures, Linked lists, Stacks, Queues", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        { 
            "week"  : "7",
            "unit"  : "Unit 6",
            "link"  : "unit06.html",
            "topic" : "Structures 3, 4: Stacks and Queues",
            "notes" : "",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "(CS250) Chapter 3: Queues", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-queue.pdf" },
                        { "name" : "(CS250) Chapter 4: Stacks", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-stack.pdf" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Stacks and Queues, project info", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-02_cs250_lecture_stacksqueues.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "06Ae 🏋️ Intro to Stacks and Queues", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2006%20-%20Structure%203,%204%20-%20Stacks%20and%20Queues/06Ae%20-%20Intro%20to%20Stacks%20and%20Queues/06Ae%20Intro%20to%20Stacks%20and%20Queues.pdf", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "06Be 🏋️ Coding a Stack", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "06Be_CodingStack.html", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "06Ce 🏋️ Coding a Queue", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "06Ce_CodingQueue.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "06Aq ✅ Stacks and Queues", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
                { "item" : "06p 💻 Project: Data Structures Application 1 (Linked structures, Stacks, Queues) ", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "06p_ApplyingDataStructures.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
            "exams" : [
                { "item" : "05-06t 💯 Intro to data structures, Linked lists, Stacks, Queues", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        
        { 
            "week"  : "8",
            "unit"  : "(Break)",
            "link"  : "",
            "topic" : "(Break) Units 4 - 6  finsh up. No new content this week.",
            "notes" : "Oct 15 Application deadline for fall graduation.",
            "due" : [
            ],
        },
        
        { 
            "week"  : "9",
            "unit"  : "Unit 7",
            "link"  : "unit07.html",
            "topic" : "Algorithm efficiency and Big-O notation",
            "notes" : "",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "(CS200/235 book) Chapter 16: Algorithm Efficiency", "type" : "reading", 
                            "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter16-algorithm_efficiency.pdf" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Algorithm Efficiency", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-16_cs250_lecture_algorithm_efficiency.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "07Ae 🏋️ Algorithm efficiency introduction", "array" : [ 
                    { "name" : "", "type" : "exercise", 
                    "docs" : "#", "starter" : "#", "video" : "#",
                    "canvas" : "yes" }
                ]},
                { "item" : "07Be 🏋️ Observing algorithm speeds", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "07Be_AlgorithmObservation.html", "starter" : "", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                //{ "item" : "07Aq ✅ Algorithm Efficiency", "array" : [
                    //{ "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
            ],
        },
        { 
            "week"  : "10",
            "unit"  : "Unit 8",
            "link"  : "unit08.html",
            "topic" : "Recursion",
            "notes" : "",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "(CS200/235 book) Chapter 17: Recursion", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter17-recursion.pdf" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Recursion", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-24_cs250_lecture_recursion.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "08Ae 🏋️ Recursion", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "08Ae_Recursion.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                //{ "item" : "08Aq ✅ Recursion", "array" : [
                //    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                //]},
            ],
        },
        { 
            "week"  : "11",
            "unit"  : "Unit 9",
            "link"  : "unit09.html",
            "topic" : "Trees, Structure 5: Binary search trees",
            "notes" : "",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "(CS250) Chapter 5: Trees", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-tree.pdf", "type" : "reading" },
                        { "name" : "(CS250) Chapter 6: Binary Search Trees", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-binary_search_tree.pdf", "type" : "reading" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Binary Search Trees", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-30_cs250_lecture_binary_search_trees.mp4" },                            
                        { "name" : "🎥 Binary Search Tree project", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-04-06_cs250_lecture_bstproject.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "09Ae 🏋️ Intro to Trees", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/Unit%2009%20-%20Structure%205%20-%20Binary%20Search%20Trees/09Ae%20-%20Intro%20to%20Trees/09Ae%20Intro%20to%20Trees.pdf", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "09Be 🏋️ Intro to Binary Search Trees", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/tree/main/Unit%2009%20-%20Structure%205%20-%20Binary%20Search%20Trees/09Be%20-%20Intro%20to%20Binary%20Search%20Trees", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "09Ce 🏋️ Coding a Binary Search Tree", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "09Ce_BinarySearchTree.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "09Aq ✅ Trees", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
                { "item" : "09Bq ✅ Binary Search Trees", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
                { "item" : "09p 💻 Project: Applying Data Structures 2 (Binary Search Trees)", "array" : [{ "name" : "", "type" : "project", 
                    "docs" : "09p_ApplyingDataStructures2.html", "starter" : "#", "video" : "#", "starter" : "#" },
                ]},
            ],
            "exams" : [
                { "item" : "09t 💯 Trees and Binary Search Trees", "array" : [ { "name" : "", "type" : "Canvas exam", "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" }, ] }
            ]
        },
        
        { 
            "week"  : "12",
            "unit"  : "(Break)",
            "link"  : "",
            "topic" : "(Break) Units 7 - 9  finsh up. No new content this week.",
            "notes" : "",
            "due" : [
            ],
        },
        { 
            "week"  : "13",
            "unit"  : "Unit 10",
            "link"  : "unit10.html",
            "topic" : "Structure 6: Hash tables",
            "notes" : "April 15 Last day to request a pass/fail grade option or to withdraw with a “W” from a full semester course.",
            "lectures" : [
                {
                    "item" : "📘 Reading", "array" : [
                        { "name" : "(CS250) Chapter 7: Hash Tables", "type" : "reading", "where" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-dictionary.pdf" },
                    ]
                },
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Hash Tables", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-04-13_cs250_lecture_hashtables.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "10Ae 🏋️ Intro to Hash Tables", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/10Ae_IntroHashTables/10Ae_IntroHashTables.pdf", "starter" : "#", "video" : "#" }
                ]},
                { "item" : "10Be 🏋️ Coding a Hash Table", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "10Be_HashTable.html", "starter" : "#", "video" : "#" }
                ]},
            ],
            "quizzes" : [
                { "item" : "10Aq ✅ Hash Tables", "array" : [
                    { "name" : "",  "type" : "Canvas quiz",     "where" : "<img src='web-assets/graphics/canvas-icon.png'> Available on Canvas" },
                ]},
            ],
            "projects" : [
            ],
        },
        { 
            "week"  : "14",
            "unit"  : "Unit 11",
            "link"  : "unit11.html",
            "topic" : "Structure 7: Heaps",
            "notes" : "25-26 Thanksgiving Day holiday. Classes not in session. College offices closed.",
            "lectures" : [
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 Heaps", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-04-20_cs250_lecture_heaps.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "11Ae 🏋️ Intro to Heaps", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/11Ae_IntroHeaps/11Ae_IntroHeaps.pdf", "starter" : "#", "video" : "#" }
                ]},
            ],
        },
        { 
            "week"  : "15",
            "unit"  : "Unit 12",
            "link"  : "unit12.html",
            "topic" : "Structure 8: Balanced search trees",
            "notes" : "Dec 1 Deadline for faculty to submit grade changes for “I” grades assigned in spring or summer.",
            "lectures" : [
                {
                    "item" : "🏫 Class archive (Spring 2021)", "array" : [
                        { "name" : "🎥 AVL Trees", "type" : "video", 
                            "url" : "http://lectures.moosader.com/cs250/2021-01_Spring/2021-04-27_cs250_lecture_avl_trees.mp4" },                            
                    ]
                },
            ],
            "exercises" : [
                { "item" : "12Ae 🏋️ Balanced Search Trees", "array" : [ { "name" : "", "type" : "exercise", 
                    "docs" : "https://gitlab.com/rachels-courses/cs250/-/blob/main/12Ae_IntroBalancedSearchTrees/12Ae_IntroBalancedSearchTrees.pdf", "starter" : "#", "video" : "#" }
                ]},
            ],
        },
        { 
            "week"  : "16",
            "unit"  : "",
            "link"  : "",
            "topic" : "Catch-up time",
            "notes" : "<strong>Dec 7-13 Scheduled final exams for students. <a href='https://www.jccc.edu/calendars/_files/fall-final-exam-schedule.pdf'>(Schedule link)</a></strong>"
        },
        { 
            "week"  : "17",
            "unit"  : "",
            "link"  : "",
            "topic" : "",
            "notes" : "<strong>13 Last day of fall semester.</strong><br><strong>Dec 14 Grades entered online by professors by 5 p.m."
        },
        //{ 
            //"week"  : "18",
            //"unit"  : "",
            //"link"  : "",
            //"topic" : "",
            //"notes" : "May 18 Grades entered online by professors by 5 p.m.<br><br>May 20 Grades available to students by noon on the web."
        //}
    ];
    
    // Insert into specific fields
    if ( $( "#index-data" ) != null )
    {
        var unitIndex = parseInt( $( "#index-data" ).val() );
        
        $( ".insert-unit" ).html( scheduleInfo[ unitIndex ]["unit"] );
        $( ".insert-topics" ).html( scheduleInfo[ unitIndex ]["topic"] );
    }
} );
