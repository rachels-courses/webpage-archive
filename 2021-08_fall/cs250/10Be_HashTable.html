<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> 10Be 🏋️ Coding a Hash Table - Programming Exercise (CS 250: Basic Data Structures using C++) </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="icon" type="image/png" href="web-assets/favicon.png">

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <!-- bootstrap -->
  <link rel="stylesheet" href="../web-assets/bootstrap-dark/css/bootstrap-dark.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- highlight.js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

  <!-- rachel's stuff -->
  <link rel="stylesheet" href="../web-assets/style/2020_10.css">
  <script src="SCHEDULE.js"></script>
  <script src="web-assets/js/header-bar.js"></script>
  <script src="web-assets/js/unit-page.js"></script>
</head>
<body>
    <div id="data" style="display: none;">
        <input type="text" id="index-data" value="4">
    </div> <!-- data -->

    <div class="container-fluid">
        <h1> CS 250: Basic Data Structures using C++ </h1>
        <hr>
        <div id="header-container"></div> <!-- JS inserts header -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a href="index.html" class="nav-link">View-by-week</a></li>
            <li class="nav-item"><a href="unit10.html" class="nav-link">Unit 10</a></li>
            <!-- <li class="nav-item">       <a href="view-by-topic.html" class="nav-link">View-by-topic</a></li> -->
            <li class="nav-item active"><a href="index.html" class="nav-link active"> 10Be 🏋️ Coding a Hash Table </a></li>
        </ul>

        <br>

        <div class="container-fluid exercise">
            <div class="row"><div class="col-md-12"><h2> 10Be 🏋️ Coding a Hash Table </h2></div> <!-- column --> </div>
            
            <hr>

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        
                        <!-- Exercise Intro -->
                        <div class="col-md-8">

                            <h3>Introduction                         </h3>
                            
                            <p>
                                A Hash Table (aka Associative Array, aka Map, aka Dictionary) is a type of data structure
                                that is built on top of an array, taking advantage of the fast access times,
                                while further improving on it by using a <strong>Hash Function</strong> to map
                                some data's key to a particular index in the array.
                                Make sure to read over the chapter on Hash Tables/Dictionaries if you haven't already.
                            </p>
                            
                            <p class="center"><img src="course-assets/hash-function.png" title="A drawn representation of a hash table built on top of an array, with a key mapping to a specific index."></p>
                                         
                            <p><strong>Goals:</strong></p>
                            <ul>
                                <li>Practice implementing a Hash Table data structure.</li>
                                <li>Practice with the Linear Probing collision strategy.</li>
                                <li>Practice with the Quadratic Probing collision strategy.</li>
                                <li>Practice with the Double Hashing collision strategy.</li>
                            </ul>

                        </div>
                        <!-- Exercise Intro -->
                        
                        <!-- Quick Jump -->
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">Quick jump</div>
                                <div class="card-body">
                                    <ul>
                                        <li><a href="#part0">Project setup                            </a></li>
                                        <li><a href="#part1">The structures around Dictionary                            </a></li>
                                        <li><a href="#part2">Dictionary functions                            </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Quick Jump -->


                        <!-- Exercise Content -->
                        <div class="col-md-12">
                            
                            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                            <hr><a name="part0" href="#part0"><h3>Project setup</h3></a>
                                            
                                <h4 style="margin-top:0;">Get the starter code</h4>
                                
                                    <p>
                                        You will need to get the starter code for this assignment to get started.
                                    </p>
                                    
                                    <ul>
                                        <li>View the starter code: 
                                        <strong><a href="https://gitlab.com/rachels-courses/cs250/-/tree/main/10Be_CodingHashTable">On GitLab</a></strong></li>
                                        
                                        <li>Download the starter code: 
                                        <strong><a href="https://gitlab.com/rachels-courses/cs250/-/archive/main/cs250-main.zip?path=10Be_CodingHashTable">Download zip</a></strong></li>
                                        
                                        <li>Downloading the code to your computer: 
                                        <strong><a href="reference.html#git-latest">Go to the quick-reference page for information on how to get the starter code files</a></strong></li>
                                    </ul>                         

                                <h4>Unit tests</h4>
                                
                                <p>
                                    There is no program for this data structure - just the unit tests.
                                    Implementing a program with a Hash Table will be part of the
                                    10p project. For this assignment, you will run the program and
                                    check your work via the automated unit tests.
                                </p>

                            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                            <hr><a name="part1" href="#part1"><h3>The structures around Dictionary</h3></a>
                            
                                <p class="center"><img src="course-assets/10p/HashTable.png" title="A UML diagram of the class declarations for Dictionary, DictionaryNode, SwissCheeseArray, and SwissCheeseNode."></p>

                                <h5>SwissCheeseArray</h5>
                                <p>
                                    The Dictionary class is built on top of an array, but rather than use a non-resizable vanilla array,
                                    the <code>SwissCheeseArray</code> has been pre-written. It is <em>similar</em> to the SmartDynamicArray,
                                    except that it allows holes between elements. You don't need to worry about its implementation too much,
                                    but the Dictionary will call functions from the SwissCheeseArray it contains, called <code>m_vector</code>.
                                </p>
                                
                                <h5>DictionaryNode</h5>
                                <p>
                                    The vector within the Dictionary contains a SwissCheeseArray, which itself is a template.
                                    In this implementation, Dictionary stores an array of <code>DictionaryNode</code> objects.
                                </p>
                                <p>
                                    The DictionaryNode contains information on its own <code>key</code>, <code>value</code>,
                                    and a boolean <code>used</code> to note whether the node is in use or not.
                                    (The element at a given index is ``deleted'' if <code>used == false</code>.)
                                </p>
                                
                                <h5>Dictionary</h5>
                                <p>
                                    The Dictionary itself has functions to Insert, Get, and Remove, as well as the inner workings
                                    of the Hash Function, Linear Probe, Quadratic Probe, and a Second Hashing Function.
                                    You'll be implementing functions within the Dictionary.
                                </p>
                                
                                <h5>CollisionMethod enum</h5>
                                
                                <p>At the top of the Dictionary.hpp file, an enum is declared:</p>
                                
                                <pre><code>enum CollisionMethod { LINEAR, QUADRATIC, DOUBLE };</code></pre>

                                <p>
                                    This enum stores the type of collision method being used by
                                    the Dictionary, and can be updated via the SetCollisionMethod function.
                                </p>
                            
                            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                            <hr><a name="part2" href="#part2"><h3>Dictionary functions</h3></a>
                            
                                <h4 style="margin-top:0;">Functions that are already implemented...</h4>
                            
                                <div class="row">
                                    <div class="col-md-6">
                                <h5>Dictionary()</h5>
                                
<pre><code>template &lt;typename TK, typename TV&gt;
Dictionary&lt;TK,TV&gt;::Dictionary()         // done
    : m_arraySize( 2251 ), m_vector( 2251 )
{
    Logger::OutHighlight( "Function begin", "Dictionary::Dictionary", 2 );
    m_itemCount = 0;
    m_collisionMethod = LINEAR;
}
</code></pre>
                                <p>
                                    The constructor function automatically initializes the underlying
                                    vector (SwissCheeseArray) to the size of 2251.
                                    By default, it sets the collision method to <code>LINEAR</code>.
                                </p>
                                    </div>
                                    <div class="col-md-6">
                                <h5>~Dictionary()</h5>
                                
<pre><code>template &lt;typename TK, typename TV&gt;
Dictionary&lt;TK,TV&gt;::~Dictionary()        // done
{
    Logger::OutHighlight( "Function begin", "Dictionary::~Dictionary", 2 );
}
</code></pre>
                                <p>
                                    The destructor doesn't really need to do anything here.
                                </p>
                                
                                    </div>
                                    <div class="col-md-6">
                                <h5>void SetCollisionMethod( CollisionMethod cm )</h5>
                                
<pre><code>template &lt;typename TK, typename TV&gt;
void Dictionary&lt;TK,TV&gt;::SetCollisionMethod( CollisionMethod cm )
{
    Logger::OutHighlight( "Function begin", "Dictionary::SetCollisionMethod", 2 );
    m_collisionMethod = cm;
}
</code></pre>
                                <p>
                                    This is a simple setter function.
                                </p>
                                
                                    </div>
                                    <div class="col-md-6">
                                <h5>int Size()</h5>
                                
<pre><code>template &lt;typename TK, typename TV&gt;
int Dictionary&lt;TK,TV&gt;::Size()
{
    return m_itemCount;
}
</code></pre>
                                <p>
                                    This returns the amount of items currently stored in the Dictionary.
                                </p>
                                
                                    </div>
                                    <div class="col-md-6">
                                <h5>bool IsFull()</h5>
                                
<pre><code>template &lt;typename TK, typename TV&gt;
bool Dictionary&lt;TK,TV&gt;::IsFull()
{
    return ( m_itemCount == m_arraySize );
}
</code></pre>
                                <p>
                                    This returns whether the Dictionary is full.
                                </p>
                                
                                    </div>
                                    <div class="col-md-6">
                                <h5>WriteToFile( const string&amp; filename )</h5>
                                
<pre><code>template &lt;typename TK, typename TV&gt;
void Dictionary&lt;TK,TV&gt;::WriteToFile( const string&amp; filename )
{
    Logger::OutHighlight( "Function begin", "Dictionary::WriteToFile", 2 );

    cout &lt;&lt; "\t Output table to: " &lt;&lt; filename &lt;&lt; endl &lt;&lt; endl;

    ofstream output( filename );
    output &lt;&lt; "Index,Key,Value\n";
    for ( int i = 0; i &lt; m_arraySize; i++ )
    {
        output &lt;&lt; i &lt;&lt; ",";

        if ( m_vector.GetAt(i).used == true )
        {
            output &lt;&lt; m_vector.GetAt(i).key &lt;&lt; "," &lt;&lt; m_vector.GetAt(i).value;
        }

        output &lt;&lt; "\n";
    }
    output.close();
}
</code></pre>
                                <p>
                                    This function outputs the contents of the Dictionary to a 
                                    CSV (comma separated value) file, iterating over every index
                                    in the Dictionary's array (2251 items) and outputting
                                    each one's key and value out.
                                </p>
                                    </div>
                                </div>
    
                                <br><br><br>
                                <p>These next functions need to be implemented:</p>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                                <h4>int HashFunction( TK key )</h4>
                                
                                <p>
                                    The HashFunction is responsible for taking in the <code>key</code>
                                    and mapping it to some <strong>array index</strong> via a math operation.
                                    For this project, we're going to keep the hashing function simple -
                                    using modulus on the array size to keep the key bound to the size of the array.
                                </p>
                                
                                <p class="math">
                                    H(k) = k % s
                                </p>
                                
                                <p>
                                    Where <span class="math">k</span> is the key, 
                                    <span class="math">s</span> is the array size,
                                    and <span class="math">H(k)</span> is the resulting hashed index.
                                </p>
                          
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                                <h4>int LinearProbe( int originalIndex, int collisionCount )</h4>
                               
                                <p>
                                For the linear probe, we are looking at the original index
                                that was generated by the HashFunction and the amount of
                                collisions encountered so far, and returning a new index
                                to try out. The formula for this is:
                                </p>

                                <p class="math">
                                    L( i, c ) = i + c
                                </p>

                                <p>
                                Where <span class="math">i</span> is the original index,
                                <span class="math">c</span> is the amount of collisions,
                                and <span class="math">L( i, c )</span> is the new index generated.
                                </p>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                               <h4>int QuadraticProbe( int originalIndex, int collisionCount )</h4>
                          
                                <p>
                                For the quadratic probe, we are looking at the original index
                                that was generated by the HashFunction and the amount of
                                collisions encountered so far, and returning a new index
                                to try out. The formula for this is:
                                </p>

                                <p class="math">
                                    Q( i, c ) = i + c^2
                                </p>

                                <p>
                                Where <span class="math">i</span> is the original index,
                                <span class="math">c</span> is the amount of collisions,
                                and <span class="math">Q( i, c )</span> is the new index generated.
                                </p>
                                <p>
                                    (Note that in C++, you'll need to <code>#include &lt;cmath&gt;</code>
                                    if you haven't already, and use the <a href="https://www.cplusplus.com/reference/cmath/pow/">pow</a> function.)
                                </p>
                                
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                               <h4>int HashFunction2( TK key )</h4>
                               
                               <p>
                                   The second hash function is another way we can figure out a new index
                                   if there is a collision. This function takes the key, and has a 
                                   <em>different</em> function than the original HashFunction.
                                   This function generates an <strong>offset</strong> - how much to
                                   add onto the original index.
                               </p>
                               <p>
                                   For this application, we're using the following formula:
                               </p>
                               
                                <p class="math">
                                    H<sub>2</sub>(k) = 7 - ( k % 7 )
                                </p>
                                
                                <p>
                                    Where <span class="math">k</span> is the key
                                    and <span class="math">H<sub>2</sub>(k)</span> is the resulting hashed offset.
                                </p>
                          
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                               <h4>int FindUnusedNode( TK key )</h4>
                               
                               <p>
                                   This function will be used during <strong>inserts</strong> to find
                                   a location in the array not currently occupied by data.
                                   The entire array is storing <code>DictionaryNode</code> objects,
                                   but since the DictionaryNode contains a <code>used</code> boolean,
                                   any elements with <code>used == false</code> is considered a ``blank space''.
                               </p>
                               
                               <div class="row">
                                   <div class="col-md-9">
                                       <p>You'll need to do the following:</p>
                                       
                                       <ol>
                                           <li>Create an <code>index</code> integer variable. Call the HashFunction, passing in the <code>key</code>, and assigning the result to the <code>index</code> variable.</li>
                                           <li>Create an <code>originalIndex</code> integer variable and assign it the value of <code>index</code>.</li>
                                           <li>Create a <code>collisionCount</code> integer variable and assign it the value of 0.</li>
                                           <li>Create a <code>stepSize</code> integer variable and assign it the value of 0.</li>
                                           <li>
                                               Create a while loop. The loop's condition is that it will
                                               continue looping while the element at the <code>index</code>
                                               in the <code>m_vector</code> has a value of <code>true</code>
                                               for its <code>used</code> boolean.
                                               
                                                <div class="hint-box">
                                                    <section class="click-to-expand">What is that condition going to look like?</section>
                                                    <section class="hint-contents">
                                                        <pre><code>while ( m_vector.GetAt(index).used )</code></pre>
                                                    </section>
                                                </div>
                                                <br>
                                                Within the while loop:
                                                <ol>
                                                    <li>
                                                        Error check: Check to see if the element at this index has a matching key.
                                                        If it does, throw a <code>runtime_error</code> because the key already exists
                                                        in the table (it <em>should</em> be unique).
                                                    </li>
                                                    
                                                    <li>Increase <code>collisionCount</code> by 1.</li>
                                                    
                                                    <li>
                                                        If the collisionMethod <code>m_collisionMethod</code> is <code>LINEAR</code>, then
                                                        call the LinearProbe function, passing in the <code>originalIndex</code>
                                                        and <code>collisionCount</code>, and storing the result in <code>index</code>.
                                                    </li>
                                                    
                                                    <li>
                                                        If the collisionMethod is <code>QUADRATIC</code>, then
                                                        call the QuadraticProbe function, passing in the <code>originalIndex</code>
                                                        and <code>collisionCount</code>, and storing the result in <code>index</code>.
                                                    </li>
                                                    
                                                    <li>
                                                        If the collisionMethod is <code>DOUBLE</code>, then
                                                        call the HashFunction2 function, passing in the <code>key</code>
                                                        and storing the result in <code>stepSize</code>. Then,
                                                        assign the <code>index</code> the result of <br>
                                                        <code>originalIndex + ( collisionCount * stepSize )</code>.
                                                    </li>
                                                </ol>
                                                <br>
                                           </li>
                                           <li>
                                               After the while loop, we can assume that a valid index has been found
                                               if no exception was thrown. Return the <code>index</code>.
                                           </li>
                                       </ol>
                                   </div>
                                   <div class="col-md-3 fit-image">
                                        <p>
                                            How to access an element of the <code>m_vector</code>:
                                            <pre><code>m_vector.GetAt( index )</code></pre>
                                        </p>
                                       
                                        <p>DictionaryNode members:</p>
                                        <p><img src="course-assets/10p/DictionaryNode.png" title="The UML diagram of the DictionaryNode class."></p>
                                   </div>
                               </div>
                               
                               
                          
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                               <h4>int FindNodeIndexWithKey( TK key )</h4>
                               
                               <p>
                                   This function will be similar to the FindUnusedNode function in
                                   that an index will be hashed and then a collision strategy will be
                                   followed if the element at the index does not match the key we're 
                                   looking for.
                                   The difference, however, is that we're looping until we find
                                   the node we're looking for, or if we hit an unused node - which means
                                   there is no node with our key.
                               </p>
                               
                               <p>
                                   Utilize the FindUnusedNode but make these changes:
                               </p>
                               
                               <ul>
                                   <li>
                                       The while loop's condition will now be
                                       to continue looping while
                                       (a) the element at this index has a <code>key</code> that does not match the parameter <code>key</code>,
                                       AND
                                       (b) the element at this index has its <code>used</code> boolean set to true.
                                       
                                       
                                        <div class="hint-box">
                                            <section class="click-to-expand">What does the condition look like?</section>
                                            <section class="hint-contents">
                                                <pre><code>while ( m_vector.GetAt(index).key != key &amp;&amp; m_vector.GetAt(index).used )</code></pre>
                                            </section>
                                        </div>
                                   </li>
                                   <li>
                                       After the while loop has ended, don't return the index right away.
                                       Instead, do the following:
                                       <ul>
                                           <li>If the element at the <code>index</code> is marked as <strong>not used</strong>, then throw a <code>runtime_error</code> -- no node exists with the key we were searching for.</li>
                                           <li>Otherwise, return the <code>index</code>.</li>
                                       </ul>
                                   </li>
                               </ul>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                               <h4>bool Exists( TK key )</h4>
                               
                               <p>
                                   In this function, you can simplify the functionality
                                   by calling the FindNodeIndexWithKey function and then
                                   returning <code>true</code> if it returns an index,
                                   or <code>false</code> if it throws an exception.
                                   <br><br>
                                   This will require calling the FindNodeIndexWithKey
                                   function within a try block and writing a catch
                                   block that listens for a <code>runtime_error</code>.
                               </p>
                          
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                               <h4>void Insert( TK key, TV value )</h4>
                               
                               <p>
                                   This function will get an index for the new item and
                                   add it to the array.
                               </p>
                               
                               <ol>
                                    <li><strong>Error check:</strong>
                                        If the array is full, then throw a <code>runtime_error</code> -- there is no more room.
                                        (Use the <code>IsFull()</code> function that belongs to the Dictionary class.)
                                    </li>
                                    <li>
                                        Use the FindUnusedNode function, passing in the <code>key</code> parameter,
                                        and assigning the result to an integer named <code>index</code>.
                                    </li>
                                    <li>
                                        Set the element of the <code>m_vector</code> at this <code>index</code>'s
                                        <code>key</code>, <code>value</code>, and <code>used</code> variables.
                                        (Copy over the parameters for key and value, set used to true.)
                                    </li>
                                    <li>
                                        Increment <code>m_itemCount</code> by 1.
                                    </li>
                               </ol>
                               
     
                          
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->     
                               <h4>TV* Get( TK key )</h4>
                               
                               <p>
                                   This function will return the address of some data
                                   in the array - not the DictionaryNode, but the
                                   actual value.
                               </p>
                               
                               <ol>
                                   <li>
                                       Use the FindNodeIndexWithKey function to find the 
                                        index of the item we're searching for.
                                    </li>
                                    <li>
                                        Return the address of the element at that index.
                                        
                                        <div class="hint-box">
                                            <section class="click-to-expand">How do I get the element at that index?</section>
                                            <section class="hint-contents">
                                                <pre><code>m_vector.GetAt(index).value</code></pre>
                                            </section>
                                        </div>
                                        <br>

                                        <div class="hint-box">
                                            <section class="click-to-expand">How do I return the address?</section>
                                            <section class="hint-contents">
                                                <pre><code>return &(m_vector.GetAt(index).value);</code></pre>
                                            </section>
                                        </div>
                                    </li>
                               </ol>
                               





            <!-- Shortcuts: -->
            <!--
            
            
            
TWO COLUMNS:
<div class="row">
    <div class="col-md-6">
    </div>
    <div class="col-md-6">
    </div>
</div>


VIDEO EMBED:

<div style="text-align:center;">
    <video width="640" height="360" controls>
        <source src="http://lectures.moosader.com/cs200/03-Branching.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video>
</div> 
        
                
CODE:

<pre><code>#include &lt;iostream&gt;
using namespace std;

int main()
{
    cout << "Hello, world!" << endl;

    return 0;
}
</code></pre>


PROGRAM OUTPUT:

<pre class="program-output">
PROGRAM 1
What is your bank balance? 30
How much to withdraw? 20
Balance: 10
</pre>


HINT BOX:

<div class="hint-box">
    <section class="click-to-expand">(Click to open hint)</section>
    <section class="hint-contents">
        <pre><code>asdf</code></pre>
    </section>
</div>
            -->

                                
                                
                        </div>
                        <!-- Exercise Content -->

                    </div>

                </div> <!-- main page info -->
            </div> <!-- row -->

        </div> <!-- container -->

        <hr>

        <section class="">
        </section> <!-- footer -->
    </div> <!-- container-fluid -->
</body>
