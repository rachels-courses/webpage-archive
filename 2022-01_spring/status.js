$( document ).ready( function() {
  
  statusInfo = "";
  
  toWrite = [
    
   
    {
      "key"       : "Trees test",
      "class"     : "CS250",
      "due"       : "Apr 4",
      "plan"      : "?",
      "size"      : "Medium"
    },

    // CS 210 PREP...
    
    
    // ********** WEEK 12 **********
    // CS 200 PREP...

    {
      "key"       : "Classes/inheritance test",
      "class"     : "CS200",
      "due"       : "Apr 4",
      "plan"      : "?",
      "size"      : "Medium"
    },

    
    // CS 235 PREP...

    // CS 250 PREP...
    {
      "key"       : "Hash tables exercise",
      "class"     : "CS250",
      "due"       : "Apr 4",
      "plan"      : "?",
      "size"      : "Small"
    },

    {
      "key"       : "Hash tables quiz",
      "class"     : "CS250",
      "due"       : "Apr 4",
      "plan"      : "?",
      "size"      : "Small"
    },

    // ********** WEEK 14 **********
    // CS 200 PREP...

    {
      "key"       : "Pointers quiz",
      "class"     : "CS200",
      "due"       : "Apr 18",
      "plan"      : "?",
      "size"      : "Medium"
    },
    {
      "key"       : "Pointers test",
      "class"     : "CS200",
      "due"       : "Apr 18",
      "plan"      : "?",
      "size"      : "Large"
    },
    // CS 235 PREP...

    {
      "key"       : "Functions and Relations test",
      "class"     : "CS210",
      "due"       : "Apr 18",
      "plan"      : "?",
      "size"      : "Large"
    },

    
  ],
  
  toGrade = [
    {
      "key"       : "[P2] 💻 Fooddood Project (v2)",
      "class"     : "CS-235-350",
      "quantity"  : "4",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },

    {
      "key"       : "[T2] 💯 Trees, Binary Search Trees, Algorithm Efficiency",
      "class"     : "CS-250-377",
      "quantity"  : "3",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    
    
    {
      "key"       : "[U03E] 🏋️ Templates, Exceptions, and STL Structures",
      "class"     : "CS-250-377",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U05E2] 🏋️ Coding a Linked List",
      "class"     : "CS-250-377",
      "quantity"  : "4",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U06E2] 🏋️ Coding Stacks and Queues",
      "class"     : "CS-250-377",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U06E2] 🏋️ File input and output",
      "class"     : "CS-200-378",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U08E] 🏋️ Operator Overloading and Copy Constructors",
      "class"     : "CS-235-350",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[TL3] 🧠 Code licenses",
      "class"     : "CS-235-350",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U09E1] 🏋️ Structs",
      "class"     : "CS-200-378",
      "quantity"  : "8",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U09E2] 🏋️ Classes",
      "class"     : "CS-200-378",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[UBE2] 📝 Logic Circuits",
      "class"     : "CS-210-376",
      "quantity"  : "2",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[UBE3] 📝 Karnaugh Maps",
      "class"     : "CS-210-376",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U09E] 🏋️ Friends, Static Members, and Lambda Expressions",
      "class"     : "CS-235-350",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[C4] 🧑‍🏫 Check-in (Week 13)",
      "class"     : "CS-235-350",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[C4] 🧑‍🏫 Check-in (Week 13)",
      "class"     : "CS-200-378",
      "quantity"  : "3",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U10E] 🏋️ Object Oriented Design and Programming",
      "class"     : "CS-200-350",
      "quantity"  : "8",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[UPE5] 📝 Proofs - Inductive proofs - Exercise",
      "class"     : "CS-210-376",
      "quantity"  : "3",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U10E] 🏋️ Object Oriented Design and Programming",
      "class"     : "CS-200-378",
      "quantity"  : "2",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[C4] 🧑‍🏫 Check-in (Week 13)",
      "class"     : "CS-250-377",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U11E1] 🏋️ Linking third party libraries",
      "class"     : "CS-235-350 ",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[U11E3] 🏋️ Coding a Hash Table",
      "class"     : "CS-250-377",
      "quantity"  : "3",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[UFE1] 📝 Definitions, Diagrams, and Inverses - Exercise",
      "class"     : "CS-210-376",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[TL4] 🧠 Bias and ethics and tech",
      "class"     : "CS-200-350",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
    {
      "key"       : "[TL2] 🧠 Jobs in Software (classic)",
      "class"     : "CS-200-350",
      "quantity"  : "1",
      "link"      : "",
      "size"      : "",
      "plan"      : "",
    },
  ];
  
  worklog = [
  ];
  
} );
