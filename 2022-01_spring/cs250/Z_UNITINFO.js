// CS 250
$( document ).ready( function() {

unitInfo = {
  "Unit 1" : {
    "link" : "unit01.html",
    "topic" : "Introduction and setup",
    "reminders" : [
    "If you have trouble setting anything up, send me an email via Canvas! We can work together via email or schedule a Zoom meeting to step through everything."
    ],
      "lectures" :        
      [
        { 
          "name" : "SPRING 2022 INTRODUCTION / SYLLABUS", 
          "url" : "https://youtu.be/F-_Im-lzRj4" 
        },
      ],
    "exercises" :       
    [   
        { 
          "key" : "INTRO", "name" : "👋 The first assignment!",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2462116",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },
        { 
          "key" : "SETUP1", "name" : "Installing an IDE",
          "docs" : "ASSIGNMENT_SETUP1_IDE.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2462121",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },  
        { 
          "key" : "SETUP2", "name" : "Creating a GitLab account",
          "docs" : "ASSIGNMENT_SETUP2_GitLab.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2462122",
          "due" : "Jan 21",
          "end" : "Jan 21",
        },
        { 
          "key" : "SETUP3", "name" : "Installing Git",
          "docs" : "ASSIGNMENT_SETUP3_Git.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2462123",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },    
    ],
    "tech-literacy" :
    [
      { "key" : "TL1", "name" : "How compiling works in C++",
          "docs" : "ASSIGNMENT_TL1_CompilerLinker.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2462124",
          "due" : "Jan 26",
          "end" : "Jan 26",
      },
    ],
  },

  "Unit 2" : 
  {
      "link" : "unit02.html",
      "topic" : "Review: C++ Basics",
    
      "quizzes" :         
      [   
        { "key" : "R1", "name" : "C++ Review - Program basics",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2494675",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R2", "name" : "C++ Review - Control flow",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2494676",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R3", "name" : "C++ Review - Functions",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2494677",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R4", "name" : "C++ Review - Arrays",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2494678",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R5", "name" : "C++ Review - Classes",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2494680",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R6", "name" : "C++ Review - Pointers and memory",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2494679",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
      ],
      
      "exercises" :
      [
        {
          "key"   : "U02E", "name" : "C++ Review",
          "docs"  : "ASSIGNMENT_U02E_CPPReview.html",
          "350"   : "https://canvas.jccc.edu/courses/52473/modules/items/2493905",
          "due"   : "Feb 2",
          "end"   : "Feb 7",
        },
        {
          "key"   : "C1", "name" : "Check-in (Week 2)",
          "350"   : "https://canvas.jccc.edu/courses/52473/modules/items/2462839",
          "due"   : "Jan 28",
          "end"   : "Jan 29",
        },
      ]
  },

  "Unit 3" : 
  {
      "link" : "unit03.html",
      "topic" : "Templates, Exception Handling, and STL Structures",

      "lectures" :        [
        { "name" : "CORE Exception handling",
            "url" : "http://lectures.moosader.com/cs200/06-Exceptions.mp4"
        },
        { "name" : "CORE Templates",
            "url" : "http://lectures.moosader.com/cs200/26-Templates.mp4"
        },
      ],
      "reading" :         [
        { "name" : "Chapter 12: Advanced Object Oriented Programming (12.2 Templates)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf"
        },
        { "name" : "Chapter 15: Exception handling with try/catch",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter15-exceptions.pdf"
        },
      ],
      
      "exercises" :
      [
        {
          "key"   : "U03E", "name" : "Templates, Exceptions, and STL Structures",
          "docs"  : "ASSIGNMENT_U03E_Templates_Exceptions_STL.html",
          "350"   : "https://canvas.jccc.edu/courses/52473/modules/items/2595323",
          "due"   : "Feb 9",
          "end"   : "Feb 11",
        },
      ],
      
      "quizzes" :         
      [
        { "key" : "U03Q1", "name" : "Templates",
            "350" : "https://canvas.jccc.edu/courses/52473/modules/items/2595346",
            "due" : "Feb 9",
            "end" : "Feb 16",
        },
        { "key" : "U03Q2", "name" : "Exception handling",
            "350" : "https://canvas.jccc.edu/courses/52473/modules/items/2595347",
            "due" : "Feb 9",
            "end" : "Feb 16",
        },
      ],
      
  },

  "Unit 4" : 
  {
      "link" : "unit04.html",
      "topic" : "Tests, Debugging, Navigating your IDE",
    
      "reading" :         [
        { "name" : "CORE Chapter 19: Testing", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter19-testing.pdf"
        },
      ],
      "lectures" :        [
        { "name" : "What is TDD? What is Test Driven Development? (Quick overview) - Development That Pays",
            "url" : "https://www.youtube.com/watch?v=H4Hf3pji7Fw"
        },
        { "name" : "How to DEBUG C++ in VISUAL STUDIO - The Cherno",
            "url" : "https://www.youtube.com/watch?v=0ebzPwixrJA"
        },
      ],
      "exercises" :            
      [
        { "key" : "U04E1", "name" : "Unit tests",
            "docs" : "ASSIGNMENT_U04E1_Unit_tests.html",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2601317",
            "due" : "Feb 16",
            "end" : "Feb 18",
        },
        { "key" : "U04E2", "name" : "Debugging tools and navigating your IDE",
            "docs" : "ASSIGNMENT_U04E2_Debugging_and_navigating.html",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2598318",
            "due" : "Feb 16",
            "end" : "Feb 18",
        },
      ],
  },

  "Unit 5" : 
  {
      "link" : "unit05.html",
      "topic" : "Intro to Data Structures, Linked Lists",
    
      "lectures" :        
      [
        { "name" : "Introducing Data Structures", "url" : "http://lectures.moosader.com/cs250/cs250-00-introduction.mp4" },
        { "name" : "Introducing Linked Lists", "url" : "https://www.youtube.com/watch?v=JIrgGBOZGeY" },
        { "name" : "Navigating the DataStructures project", "url" : "https://www.youtube.com/watch?v=Jiuk3CH_59c" },
        { "name" : "Coding a Linked List", "url" : "https://www.youtube.com/watch?v=6FwhiameYPI" },
      ],
      "reading" :         
      [
        { "name" : "DS Chapter 2: Our First Data Structures", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-first-data-structure.pdf" },
        { "name" : "DS Chapter 3: Linked Lists", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-linked_lists.pdf" },
      ],
      "exercises" :       
      [
        { 
          "key" : "U05E1", 
          "name" : "Intro to Linked Lists (Paper)",
          "docs" : "docs/U05E1_IntroLinkedList.pdf",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610296",
          "due" : "Mar 2",
          "end" : "Nar 8",
        },
        { 
          "key" : "U05E2", 
          "name" : "Coding Linked Lists",
          "docs" : "ASSIGNMENT_U05E2_LinkedListStructure.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610297",
          "due" : "Mar 4",
          "end" : "Mar 11",
        },
      ],
      "quizzes" :         
      [
        { 
          "key" : "U05Q", 
          "name" : "Linked Lists Quiz",
          "docs" : "ASSIGNMENT_U05Q_LinkedListQuiz.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610446",
          "due" : "Mar 2",
          "end" : "Mar 8",
        },
      ],
      "tech-literacy" :   
      [
        { 
          "key" : "TL2", 
          "name" : "Case study: GitHub AI Copilot and software licensing",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610330",
          "due" : "Mar 9",
          "end" : "Mar 16",
        },
      ],
  },

  "Unit 6" : 
  {
    "link" : "unit06.html",
    "topic" : "Stacks and Queues",
      
    "lectures" :        
    [   
      { "name" : "Introducing Stacks and Queues", "url" : "https://www.youtube.com/watch?v=UdPRKHdIc8k"  }, 
      { "name" : "Stacks and Queues", "url" : "https://www.youtube.com/watch?v=pF3EIL6G2AM"  },
    ],
    "reading" :         
    [   
      { "name" : "DS Chapter 4: Queues", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-queue.pdf" },
      { "name" : "DS Chapter 5: Stacks", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-stack.pdf" },
    ],
    "exercises" :
    [
      { "key" : "U06E1", "due" : "Mar 9", "end" : "Mar 15", "name" : "Intro to Stacks and Queues (Paper)", "docs" : "docs/U06E1_IntroStacksQueues.pdf", "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610492" },
      { "key" : "U06E2", "due" : "Mar 9", "end" : "Mar 15", "name" : "Coding Stacks and Queues", "docs" : "ASSIGNMENT_U06E2_StacksAndQueues.html", "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610493" },
    ],
    "quizzes" :         
    [
      { 
        "key" : "U06Q", 
        "name" : "Stacks and Queues Quiz",
        "docs" : "ASSIGNMENT_U06Q_StacksAndQueues.html",
        "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2610566",
        "due" : "Mar 9",
        "end" : "Mar 16",
      },
    ],
  },

  "Unit 7" : 
  {
    "link" : "unit07.html",
    "topic" : "Algorithm Efficiency, Recursion",
    
    "lectures" :        
    [   
      { "name" : "Algorithm Efficiency", "url" : "https://www.youtube.com/watch?v=6yopfVQO5fE" },
      { "name" : "Recursion", "url" : "https://www.youtube.com/watch?v=NsP4VQHa19g" },
    ],
    "reading" :         
    [   
      { "name" : "CORE Chapter 16: Algorithm Efficiency", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter16-algorithm_efficiency.pdf" },
      { "name" : "CORE Chapter 17: Recursion", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter17-recursion.pdf" },
    ],
    "exercises" :
    [
      { "key" : "U07E1", "due" : "Mar 16", "end" : "Mar 23", "name" : "Intro to Algorithm Efficiencies",  "docs" : "ASSIGNMENT_U07E1_IntroEfficiencyAndAlgorithms.html", "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2622170" },
      { "key" : "U07E2", "due" : "Mar 16", "end" : "Mar 23", "name" : "Observing Algorithm Efficiency",   "docs" : "ASSIGNMENT_U07E2_AlgorithmEfficiency.html",          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2622171" },
      { "key" : "U07E3", "due" : "Mar 16", "end" : "Mar 23", "name" : "Recursion",                        "docs" : "ASSIGNMENT_U07E3_Recursion.html",                    "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2622172" },
    ],
  },


  "Unit 8" : {
      "link" : "unit08.html",
      "topic" : "Trees and Binary Search Trees, and Project 1",
      
    "lectures" :        
    [   
      { "name" : "Trees and Binary Search Trees", "url" : "https://www.youtube.com/watch?v=gch9JwT7Pw4" },
    ],
    "reading" :         
    [   
      { "name" : "DS Chapter 6: Trees", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-tree.pdf" },
      { "name" : "DS Chapter 7: Binary Search Trees", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-binary_search_tree.pdf" },
    ],
    "exercises" :
    [
      { "key" : "U08E1", "due" : "Mar 30", "end" : "Apr 6", "name" : "Intro to Trees",               
                "docs" : "docs/U08E1_IntroTrees.pdf", 
                "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630499" },
      { "key" : "U08E2", "due" : "Mar 30", "end" : "Apr 6", "name" : "Intro to Binary Search Trees", 
                "docs" : "docs/U08E2_IntroBinarySearchTrees.pdf",
                "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630500" },
      { "key" : "U08E3", "due" : "Apr 1", "end" : "Apr 8", "name" : "Coding a Binary Search Tree",                    
                "docs" : "ASSIGNMENT_U08E3_BinarySearchTree.html",                    
                "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630501" },
    ],
    "quizzes" :         
    [
      { 
        "key" : "U08Q", 
        "name" : "Trees and Binary Search Trees Quiz",
        "docs" : "ASSIGNMENT_U08Q_BinarySearchTree.html",
        "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630508",
        "due" : "Apr 1",
        "end" : "Apr 8",
      },
    ],
      "projects" :        
      [   
        { "key" : "P1",     "name" : "Project 1: Lists, Stacks, and Queues",
            "docs" : "ASSIGNMENT_P1_PROJECT1.html",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630608",
            "due" : "Apr 2",
            "end" : "Apr 4",
        },
      ],
  },

  "Unit 9" : 
  {
      "link" : "unit09.html",
      "topic" : "AVL Trees (Balanced Search Tree)",
    "exercises" :
    [
      { "key" : "U08E1", "due" : "Apr 6", "end" : "Apr 13", "name" : "Intro to Trees",
                "docs" : "docs/U09E1_IntroBalancedSearchTrees.pdf", 
                "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630492" },
    ],
  },

  "Unit 10" : 
  {
    "link" : "unit10.html",
    "topic" : "Heaps, Project 2",
    
    "exercises" : [
      { "key" : "U10E1", "due" : "Apr 13", "end" : "Apr 20", "name" : "Intro to Heaps",
                "docs" : "docs/U10E1_IntroHeaps.pdf", 
                "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630494" },
      ],
      "projects" :        
      [   
        { "key" : "P2",     "name" : "Project 2: Binary Search Tree",
            "docs" : "ASSIGNMENT_P2_PROJECT2.html",
            "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2644926",
            "due" : "Apr 25",
            "end" : "Apr 25",
        },
      ],
    "exams" :           
    [   
      { "key" : "T2", "name" : "Trees, Binary Search Trees, and Algorithm Efficiency", "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2645079" },
    ]
  },

  "Unit 11" : 
  {
      "link" : "unit11.html",
      "topic" : "Hash Tables (Dictionaries)",
      "lectures" :        
      [   
        { "name" : "Hash Tables", "url" : "https://www.youtube.com/watch?v=FJGHphovMQw" },
      ],
      "reading" :         
      [   
        { "name" : "DS Chapter 8: Hash Tables", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-dictionary.pdf" },
      ],
      
      "exercises" : [
        { "key" : "U11E1", "due" : "Apr 13", "end" : "Apr 20", "name" : "Intro to Heaps",
                  "docs" : "docs/U10E1_IntroHeaps.pdf", 
                  "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630494" },
        { "key" : "U11E2", "due" : "Apr 13", "end" : "Apr 20", "name" : "Intro to Hash Tables",
                  "docs" : "docs/U10E2_IntroHashTables.pdf", 
                  "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630495" },
        { "key" : "U11E3", "due" : "Apr 15", "end" : "Apr 22", "name" : "Coding a Hash Table",
                  "docs" : "ASSIGNMENT_U10E3_HashTable.html", 
                  "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630503" },
      ],
      "quizzes" :         
      [
        { 
          "key" : "U10Q", 
          "name" : "Hash Table Quiz",
          "docs" : "ASSIGNMENT_U10Q_HashTable.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2630507",
          "due" : "Apr 13",
          "end" : "Apr 20",
        },
      ],
  },
  
  "Unit 12" : {
    "link" : "unit12.html",
    "topic" : "Student projects",
    
    "projects" :        
    [   
      { "key" : "PS",     "name" : "Student Project",
          "docs" : "StudentProject.html",
          "377" : "https://canvas.jccc.edu/courses/52473/modules/items/2649876",
          "due" : "May 2",
          "end" : "May 6",
      },
    ],
    "tech-literacy" :   
    [   
      { "key" : "TL3", "name" : "Case study: Rohrer, digital art ownership, and NFTs", "376" : "https://canvas.jccc.edu/courses/52473/modules/items/2659322" },
    ],
    
  },

  //"TEMPLATE" : 
  //{
    //"lectures" :        
    //[   
      //{ "name" : "", "url" : "" },
      //{ "name" : "", "url" : "" },
    //],
    //"reading" :         
    //[   
      //{ "name" : "", "url" : "" },
      //{ "name" : "", "url" : "" },
    //],
    //"archives" :        
    //[   
      //{ "name" : "", "url" : "" },
    //],
    //"exercises" :       
    //[   
      //{ "key" : "", "name" : "", "docs" : "", "376" : "" },
      //{ "key" : "", "name" : "", "docs" : "", "376" : "" },
    //],
    //"quizzes" :         
    //[   
      //{ "key" : "", "name" : "", "376" : "" },
      //{ "key" : "", "name" : "", "376" : "" },
    //],
    //"tech-literacy" :   
    //[   
      //{ "key" : "", "name" : "", "docs" : "", "376" : "" },
    //],
    //"projects" :        
    //[   
      //{ "key" : "", "name" : "", "docs" : "", "376" : "" },
    //],
    //"exams" :           
    //[   
      //{ "key" : "", "name" : "", "url" : "" },
    //]
  //},
  };

} );
