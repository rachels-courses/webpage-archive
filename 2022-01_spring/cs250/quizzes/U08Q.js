$( document ).ready( function() {

/*
 * Hi there. Yes, I know you can view the answers here. The quizzes
 * are meant to help you learn and practice ideas, vocab, etc.,
 * and you can take it as much as you want, so like... I guess you
 * can look at the answers, that's fine, but it's not really necessary?
 * */

quizInfo = [

  {
    "type" : "multiple-choice",
    "question" : "Is this a valid Binary Search Tree?<br> <div class='center'><img src='quizzes/images/U08Q_Tree1.png'></div>",
    "more-info" : "This is invalid because the ordering doesn't adhere to 'left' for lower values and 'right' for higher values.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "Valid Binary Search Tree", "key" : "d1" },
      { "text" : "Not a valid Binary Search Tree", "key" : "d2" },
    ],
    "solution" : "d2",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Is this a valid Binary Search Tree?<br> <div class='center'><img src='quizzes/images/U08Q_Tree2.png'></div>",
    "more-info" : "This is a valid Binary Search Tree",
    "resources" : [ ],
    "options" :
    [
      { "text" : "Valid Binary Search Tree", "key" : "d1" },
      { "text" : "Not a valid Binary Search Tree", "key" : "d2" },
    ],
    "solution" : "d1",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given this Binary Search Tree: <p class='center'><img src='quizzes/images/U08Q_Tree3.png'></p> If we call <code>Push(\"E\");</code> where would the new node go?",
    "more-info" : "The Push process will begin at the root - C. E &gt; C, so then we move right. E &lt; F, so we move left. E &gt; D, so we move right.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "E would be B's left child",  "key" : "d1" },
      { "text" : "E would be B's right child", "key" : "d2" },
      { "text" : "E would be D's left child",  "key" : "d3" },
      { "text" : "E would be D's right child", "key" : "d4" },
      { "text" : "E would be G's left child",  "key" : "d5" },
      { "text" : "E would be G's right child", "key" : "d6" },
    ],
    "solution" : "d4",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "What causes a Binary Search Tree's Search, Insert, and Delete growth rate to be <em><strong>O(log n)</strong></em>?",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "Every time we recurse left or right, we're removing half of the tree to search, causing the inverse of exponential growth.", "key" : "d1" },
      { "text" : "Because a tree has more nodes than a linked list, its search time is double that of a linked list.", "key" : "d2" },
      { "text" : "Because accessing the min or max value requires traversing in one direction only, search time ends up linear for any node.", "key" : "d3" },
      { "text" : "Because of modern technology, CPUs are able to handle tree structures better than linear structures.", "key" : "d4" },
    ],
    "solution" : "d1",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "All Binary Search Tree functionality begins its recursion at",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "The last inserted item", "key" : "d1" },
      { "text" : "The right-most leaf", "key" : "d2" },
      { "text" : "The left-most leaf", "key" : "d3" },
      { "text" : "The root", "key" : "d4" },
    ],
    "solution" : "d4",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "Given this series of commands, which Binary Search Tree would result? <br> <code>Push(\"A\");</code><br><code>Push(\"B\");</code><br><code>Push(\"C\");</code><br>",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U08Q_Tree5.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U08Q_Tree6.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U08Q_Tree4.png'>", "key" : "d3" },
      { "text" : "<img src='quizzes/images/U08Q_Tree7.png'>", "key" : "d4" },
    ],
    "solution" : "d3",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "Given this series of commands, which Binary Search Tree would result? <br> <code>Push(5);</code><br><code>Push(3);</code><br><code>Push(6);</code><br><code>Push(1);</code><br><code>Push(4);</code><br>",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U08Q_Tree8.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U08Q_Tree9.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U08Q_Tree10.png'>", "key" : "d3" },
      { "text" : "<img src='quizzes/images/U08Q_Tree11.png'>", "key" : "d4" },
    ],
    "solution" : "d2",
    "width" : 6
  },

  //{ // TEMPLATE MULTIPLE CHOICE
    //"type" : "multiple-choice",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"options" :
    //[
    // { "text" : "", "key" : "" },
    //],
    //"solution" : "",
    //"width" : "12"
  //},

  //{ // TEMPLATE DROPDOWN
    //"type" : "dropdown",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"dropdowns" :
    //[
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
    //]
  //},

];

} );
