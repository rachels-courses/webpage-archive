$( document ).ready( function() {

/*
 * Hi there. Yes, I know you can view the answers here. The quizzes
 * are meant to help you learn and practice ideas, vocab, etc.,
 * and you can take it as much as you want, so like... I guess you
 * can look at the answers, that's fine, but it's not really necessary?
 * */

quizInfo = [

  {
    "type"      : "dropdown",
    "question"  : "When comparing Link-based structures like a LinkedList and Array-based structures like a Vector there are tradeoffs. For the following two statements, which describes what structure?",
    "more-info" : "With arrays we allocate memory for a sequence of items at a time. This means we often have empty, unused spots in the array. With a linked list, we only allocate space for 1 new item each time something new is added.",
    "resources" : [ { "name" : "DS Chapter 3: Linked Lists", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures%20-%20Multilingual/standalone-linked_lists.pdf" }, ],
    "dropdowns" :
    [
      {
        "width" : 12,
        "prompt" : "This structure only allocates as much memory as is needed.",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Linked",
          "Array"
        ],
        "solution" : "Linked"
      },
      {
        "width" : 12,
        "prompt" : "This structure generally allocates more memory than is needed, and needs to resize once it is full.",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Linked",
          "Array"
        ],
        "solution" : "Array"
      }
    ]
  },

  {
    "type" : "dropdown",
    "question" : "When comparing Link-based structures like a LinkedList and Array-based structures like a Vector there are tradeoffs. For the following two statements, which describes what structure?",
    "more-info" : "Since an array's elements are contiguous in memory, we can quickly find the memory address of the item at position 'i' by taking the address of [0] and adding i * sizeof( datatype ). This gives us random access.<br><br>With a linked list, elements are NOT contiguous in memory, so we have to traverse them from beginning-to-end (or vice versa).",
    "resources" : [ ],
    "dropdowns" :
    [
      {
        "width" : 12,
        "prompt" : "This structure allows for instant random-access to an element at some position.",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Linked",
          "Array"
        ],
        "solution" : "Array"
      },
      {
        "width" : 12,
        "prompt" : "This structure requires walking through the structure to access an element at some position.",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Linked",
          "Array"
        ],
        "solution" : "Linked"
      },
    ]
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram1.png'></p><p>This diagram represents a...</p>",
    "more-info" : "A linked list contains pointers to its FIRST and LAST node elements. A Node contains pointers to the PREVIOUS and NEXT node neighbors.",
    "resources" : [  ],
    "options" :
    [
      { "text" : "Linked List", "key" : "a1" },
      { "text" : "Node", "key" : "a2" },
    ],
    "solution" : "a1",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram2.png'></p><p>This diagram represents a...</p>",
    "more-info" : "A linked list contains pointers to its FIRST and LAST node elements. A Node contains pointers to the PREVIOUS and NEXT node neighbors.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "Linked List", "key" : "a1" },
      { "text" : "Node",        "key" : "a2" },
    ],
    "solution" : "a2",
    "width" : 6
  },

  {
    "type" : "dropdown",
    "question" : "In the <strong>Node's</strong> constructor, which pointers should be set to <code>nullptr</code>?",
    "more-info" : "A linked list contains pointers to its FIRST and LAST node elements. A Node contains pointers to the PREVIOUS and NEXT node neighbors.",
    "resources" : [ ],
    "dropdowns" :
    [
      {
        "width" : 12,
        "prompt" : "These pointers should be set to nullptr.",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "ptrFirst and ptrLast",
          "ptrPrev and ptrNext"
        ],
        "solution" : "ptrPrev and ptrNext"
      },
    ]
  },

  {
    "type" : "dropdown",
    "question" : "In the <strong>Linked List's</strong> constructor, which pointers should be set to <code>nullptr</code>?",
    "more-info" : "A linked list contains pointers to its FIRST and LAST node elements. A Node contains pointers to the PREVIOUS and NEXT node neighbors.",
    "resources" : [ ],
    "dropdowns" :
    [
      {
        "width" : 12,
        "prompt" : "These pointers should be set to nullptr.",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "ptrFirst and ptrLast",
          "ptrPrev and ptrNext"
        ],
        "solution" : "ptrFirst and ptrLast"
      },
    ]
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram3.png'></p><p>Here is a LinkedList with one element. After <code>PopFront()</code> is called, what is the new state of the LinkedList?</p>",
    "more-info" : "Once the last element of a list is removed, the FIRST and LAST pointers of the list should be pointing to nullptr.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U05Q_diagram3a.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U05Q_diagram3b.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U05Q_diagram3c.png'>", "key" : "d3" },
      { "text" : "<img src='quizzes/images/U05Q_diagram3d.png'>", "key" : "d4" },
    ],
    "solution" : "d2",
    "width" : 3
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram4.png'></p><p>Given this LinkedList as a starting point, what will the LinkedList look like after <code>PopFront()</code> is called?</p>",
    "more-info" : "PopFront removes the first element of the list.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U05Q_diagram4a.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U05Q_diagram4b.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U05Q_diagram4c.png'>", "key" : "d3" },
    ],
    "solution" : "d1",
    "width" : 4
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram4.png'></p><p>Given this LinkedList as a starting point, what will the LinkedList look like after <code>PopBack()</code> is called?</p>",
    "more-info" : "PopBack removes the last element of the list.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U05Q_diagram4a.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U05Q_diagram4b.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U05Q_diagram4c.png'>", "key" : "d3" },
    ],
    "solution" : "d2",
    "width" : 4
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram5.png'></p><p>Given this LinkedList as a starting point, what will the LinkedList look like after <code>PushFront( \"GAW\" )</code> is called?</p>",
    "more-info" : "PushFront adds a new item to the front of the list.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U05Q_diagram5a.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U05Q_diagram5b.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U05Q_diagram5c.png'>", "key" : "d3" },
      { "text" : "<img src='quizzes/images/U05Q_diagram5d.png'>", "key" : "d4" },
    ],
    "solution" : "d4",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram6.png'></p><p>Given this LinkedList as a starting point, what will the LinkedList look like after <code>PushBack( \"GC\" )</code> is called?</p>",
    "more-info" : "PushBack adds a new item to the back of the list.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<img src='quizzes/images/U05Q_diagram6a.png'>", "key" : "d1" },
      { "text" : "<img src='quizzes/images/U05Q_diagram6b.png'>", "key" : "d2" },
      { "text" : "<img src='quizzes/images/U05Q_diagram6c.png'>", "key" : "d3" },
      { "text" : "<img src='quizzes/images/U05Q_diagram6d.png'>", "key" : "d4" },
    ],
    "solution" : "d2",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "<p>A linked list started like this:</p><p class='center'><img src='quizzes/images/U05Q_diagram7a.png'></p><p>And after running a command it looks like this:</p><p class='center'><img src='quizzes/images/U05Q_diagram7b.png'></p><p>What command was executed?</p>",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "PushBack( \"EMI\" );",  "key" : "a1" },
      { "text" : "PushFront( \"EMI\" );", "key" : "a2" },
      { "text" : "PopBack( \"EMI\" );",   "key" : "a3" },
      { "text" : "PopFront( \"EMI\" );",  "key" : "a4" },
    ],
    "solution" : "a1",
    "width" : 3
  },

  {
    "type" : "dropdown",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram8.png'></p><p>Given this linked list with ptrCurrent pointing to \"B\"...</p>",
    "more-info" : "",
    "resources" : [ ],
    "dropdowns" :
    [
      {
        "width" : 12,
        "prompt" : "To access the element with \"A\", you would use...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "ptrCurrent->ptrNext->ptrNext",
          "ptrCurrent->ptrNext",
          "ptrCurrent->ptrNext->ptrPrev",
          "ptrCurrent->ptrPrev",
        ],
        "solution" : "ptrCurrent->ptrPrev"
      },
      {
        "width" : 12,
        "prompt" : "To access the element with \"C\", you would use...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "ptrCurrent->ptrNext->ptrNext",
          "ptrCurrent->ptrNext",
          "ptrCurrent->ptrNext->ptrPrev",
          "ptrCurrent->ptrPrev",
        ],
        "solution" : "ptrCurrent->ptrNext"
      },
      {
        "width" : 12,
        "prompt" : "To access the element with \"D\", you would use...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "ptrCurrent->ptrNext->ptrNext",
          "ptrCurrent->ptrNext",
          "ptrCurrent->ptrNext->ptrPrev",
          "ptrCurrent->ptrPrev",
        ],
        "solution" : "ptrCurrent->ptrNext->ptrNext"
      },
    ]
  },

  {
    "type" : "dropdown",
    "question" : "<p class='center'><img src='quizzes/images/U05Q_diagram9.png'></p><p>Given this linked list, PushAt has been called to insert the value \"Z\" to index 2. ptrCurrent is pointing to the Node at position 2, where \"Z\" will need to be inserted. How will the pointers be updated?</p>",
    "more-info" : "<p>The diagram after the PushAt will look like this:</p><p class='center'><img src='quizzes/images/U05Q_diagram9b.png'></p>",
    "resources" : [ ],
    "dropdowns" :
    [
      {
        "width" : 12,
        "prompt" : "The new node \"Z\"'s ptrPrev will point to...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Node \"A\"",
          "Node \"B\"",
          "Node \"C\"",
          "Node \"D\"",
        ],
        "solution" : "Node \"B\""
      },
      {
        "width" : 12,
        "prompt" : "The new node \"Z\"'s ptrNext will point to...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Node \"A\"",
          "Node \"B\"",
          "Node \"C\"",
          "Node \"D\"",
        ],
        "solution" : "Node \"C\""
      },
      {
        "width" : 12,
        "prompt" : "The node pointing to \"Z\" via its <code>ptrNext</code> is...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Node \"A\"",
          "Node \"B\"",
          "Node \"C\"",
          "Node \"D\"",
        ],
        "solution" : "Node \"B\""
      },
      {
        "width" : 12,
        "prompt" : "The node pointing to \"Z\" via its <code>ptrPrev</code> is...",
        "options" :
        [
          "( SELECT AN ANSWER )",
          "Node \"A\"",
          "Node \"B\"",
          "Node \"C\"",
          "Node \"D\"",
        ],
        "solution" : "Node \"C\""
      },
    ]
  },



  //{ // TEMPLATE MULTIPLE CHOICE
    //"type" : "multiple-choice",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"options" :
    //[
    // { "text" : "", "key" : "" },
    //],
    //"solution" : "",
    //"width" : "12"
  //},

  //{ // TEMPLATE DROPDOWN
    //"type" : "dropdown",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"dropdowns" :
    //[
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
    //]
  //},

];

} );
