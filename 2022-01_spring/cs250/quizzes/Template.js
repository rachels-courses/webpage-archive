$( document ).ready( function() {

/*
 * Hi there. Yes, I know you can view the answers here. The quizzes
 * are meant to help you learn and practice ideas, vocab, etc.,
 * and you can take it as much as you want, so like... I guess you
 * can look at the answers, that's fine, but it's not really necessary?
 * */

quizInfo = [

  {
    "type" : "multiple-choice",
    "question" : "Determine the state of a <strong>queue</strong> after the following commands: <ol><li><code>Push(\"A\")</code></li><li><code>Push(\"B\")</code></li><li><code>Push(\"C\")</code></li></ol>",
    "more-info" : "Queues are First In First Out",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>C</td><td>B</td><td>A</td><th>Back</th></table>", "key" : "d1" },
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>A</td><td>B</td><td>C</td><th>Back</th></table>", "key" : "d2" },
    ],
    "solution" : "d2",
    "width" : 6
  },
  

  //{ // TEMPLATE MULTIPLE CHOICE
    //"type" : "multiple-choice",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"options" :
    //[
    // { "text" : "", "key" : "" },
    //],
    //"solution" : "",
    //"width" : "12"
  //},

  //{ // TEMPLATE DROPDOWN
    //"type" : "dropdown",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"dropdowns" :
    //[
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
    //]
  //},

];

} );
