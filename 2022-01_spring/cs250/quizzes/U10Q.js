$( document ).ready( function() {

/*
 * Hi there. Yes, I know you can view the answers here. The quizzes
 * are meant to help you learn and practice ideas, vocab, etc.,
 * and you can take it as much as you want, so like... I guess you
 * can look at the answers, that's fine, but it's not really necessary?
 * */

quizInfo = [

  {
    "type" : "multiple-choice",
    "question" : "Another common name for a Hash Table is...",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "Dynamic Array", "key" : "d1" },
      { "text" : "Heaps", "key" : "d2" },
      { "text" : "Dictionaries", "key" : "d3" },
    ],
    "solution" : "d3",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "In the C++ Standard Template Library, the hash table it provides is called the...",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "map", "key" : "d1" },
      { "text" : "vector", "key" : "d2" },
      { "text" : "list", "key" : "d2" },
    ],
    "solution" : "d1",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Generally to have better efficiency, the size of the hash table's array should be...",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "An even number", "key" : "d1" },
      { "text" : "A prime number", "key" : "d2" },
      { "text" : "A number less than 10", "key" : "d3" },
      { "text" : "An exponent of 2", "key" : "d4" },
    ],
    "solution" : "d2",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given an array of size 7 and the following hash function - <p><code>Hash( x ) = x % arraySize</code></p> - find the generated index (assume no collisions) for an item with a <strong>key of 16</strong>.",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "0", "key" : "d1" },
      { "text" : "1", "key" : "d2" },
      { "text" : "2", "key" : "d3" },
      { "text" : "3", "key" : "d4" },
    ],
    "solution" : "d3",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given an array of size 7 and the following hash function - <p><code>Hash( x ) = x % arraySize</code></p> - find the generated index (assume no collisions) for an item with a <strong>key of 14</strong>.",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "0", "key" : "d1" },
      { "text" : "1", "key" : "d2" },
      { "text" : "2", "key" : "d3" },
      { "text" : "3", "key" : "d4" },
    ],
    "solution" : "d1",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given the following array - <br><table class='array'><tr><th class='invis'>Index</th><th>0</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th></tr><tr><th class='invis'>Element</th><td></td><td>x</td><td></td><td>x</td><td></td><td></td></tr></table><br> - An item's key has been hashed to the resulting value of index 1 - however, there's a collision. Using <strong>Linear Probing</strong>, what is the next valid index?",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "0", "key" : "d1" },
      { "text" : "2", "key" : "d2" },
      { "text" : "4", "key" : "d3" },
    ],
    "solution" : "d2",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given the following array - <br><table class='array'><tr><th class='invis'>Index</th><th>0</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th></tr><tr><th class='invis'>Element</th><td></td><td>x</td><td>x</td><td>x</td><td></td><td></td></tr></table><br> - An item's key has been hashed to the resulting value of index 1 - however, there's a collision. Using <strong>Linear Probing</strong>, what is the next valid index?",
    "more-info" : "",
    "resources" : [ ],
    "options" :
    [
      { "text" : "4", "key" : "d1" },
      { "text" : "5", "key" : "d2" },
      { "text" : "6", "key" : "d3" },
    ],
    "solution" : "d1",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given the following array - <br><table class='array'><tr><th class='invis'>Index</th><th>0</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th></tr><tr><th class='invis'>Element</th><td></td><td></td><td>x</td><td>x</td><td></td><td></td></tr></table><br> - An item's key has been hashed to the resulting value of index 2 - however, there's a collision. Using <strong>Quadratic Probing</strong>, what is the next valid index?",
    "more-info" : "Index 2 is the first collision. 1<sup>2</sup>=1, so next check is at 2+1 = 3. Index 3 is the second collision, so next move is 2<sup>2</sup> = 4 (from original location), so 2+4 = 6.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "4", "key" : "d1" },
      { "text" : "5", "key" : "d2" },
      { "text" : "6", "key" : "d3" },
    ],
    "solution" : "d3",
    "width" : 6
  },
  

  //{ // TEMPLATE MULTIPLE CHOICE
    //"type" : "multiple-choice",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"options" :
    //[
    // { "text" : "", "key" : "" },
    //],
    //"solution" : "",
    //"width" : "12"
  //},

  //{ // TEMPLATE DROPDOWN
    //"type" : "dropdown",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"dropdowns" :
    //[
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
    //]
  //},

];

} );
