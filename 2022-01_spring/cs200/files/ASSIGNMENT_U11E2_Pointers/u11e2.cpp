#include <iostream>
using namespace std;

#include "Functions.h"

int main()
{
  bool done = false;
  while ( !done )
  {
    cout << endl << endl << string( 80, '#' ) << endl << "MAIN MENU" << endl << string( 80, '#' ) << endl;
    cout << "1. Program1_ExploringAddresses" << endl;
    cout << "2. Program2_DereferencingPointers" << endl;
    cout << "3. Program3_PointersAndClasses" << endl;
    cout << "4. Program4_DynamicArrays" << endl;
    cout << endl;
    cout << "0. Exit" << endl;
    cout << endl;

    cout << ">> ";
    int choice;
    cin >> choice;

    if      ( choice == 0 ) { done = true; }
    else if ( choice == 1 ) { Program1_ExploringAddresses(); }
    else if ( choice == 2 ) { Program2_DereferencingPointers(); }
    else if ( choice == 3 ) { Program3_PointersAndClasses(); }
    else if ( choice == 4 ) { Program4_DynamicArrays(); }
  }
}
