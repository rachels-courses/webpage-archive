// CS 210
$( document ).ready( function() {

    headerInfo = {
        "courseCode"    : "CS 210",
        "course"        : "CS 210: Discrete Structures I",
        "dates"         : "2022-01-18 to 2022-05-16",
        "semester"      : "Spring 2022",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs210/",
        //"textbook1"     : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_210",
        "syllabus"      : "syllabus.html",
        //"exampleCode"   : "https://gitlab.com/rachels-courses/cs200-concepts-of-programming-with-cpp/-/tree/main/example-code"
        "prerequisites" : "MATH 171 with a grade of 'B' or higher or a higher level MATH course with a grade of 'C' or higher.",
        "description"   : "This course focuses on the study of topics in Discrete Structures aimed at applications in Computer Science. Students will study logic, methods of proof including induction, set theory, relations, functions and Boolean algebra. They will develop general problem-solving skills.",
        "credit_hours"  : "3",
    };
    
    sectionInfo = [
        {
            "semester"      : "Spring 2022",
            "crn"           : "11482",
            "section"       : "376",
            "method"        : "Hybrid",
            "times"         : "Tues/Thurs 12:30 - 1:45 pm",
            "background"    : "bg1",
            "room"          : "RC 344"
        },
        //{
            //"semester"      : "Fall 2021",
            //"crn"           : "81830",
            //"section"       : "378",
            //"method"        : "Online hybrid",
            //"times"         : "Tues, 6:00 - 8:50 pm",
            //"background"    : "bg2"
        //}
    ];
    
} );
