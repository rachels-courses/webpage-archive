function ThisExists( obj )
{
  return ( obj !== undefined && obj != "" );
}

function ThisAintEmpty( obj )
{
  return ( obj !== undefined && obj.length > 0 );
}



$( document ).ready( function()
{
  // Section information
  sectionNumbers = ["350", "351", "376", "377", "378"];
  
  // Hint expander
  $( ".click-to-expand" ).click( function() {
      
      if ( $( this ).next( ".hint-contents" ).css( "display" ) == "none" ) {
          $( this ).next( ".hint-contents" ).slideDown( "fast", function() {} );
      }
      else {
          $( this ).next( ".hint-contents" ).slideUp( "fast", function() {} );
      }
  } );
  
  // Collapse box expander
  $( ".click-to-expand" ).click( function() {
      
      if ( $( this ).next( ".collapse-contents" ).css( "display" ) == "none" ) {
          $( this ).next( ".collapse-contents" ).slideDown( "fast", function() {} );
      }
      else {
          $( this ).next( ".collapse-contents" ).slideUp( "fast", function() {} );
      }
  } );
  
  $( ".insert-prev_page" ).attr( "href", $( "#prev_page" ).val() );
  
  $( ".insert-next_page" ).attr( "href", $( "#next_page" ).val() );
  
  $( ".insert-prev_page_name" ).html( $( "#prev_page_name" ).val() );
  
  $( ".insert-next_page_name" ).html( $( "#next_page_name" ).val() );
  
  
}
);
