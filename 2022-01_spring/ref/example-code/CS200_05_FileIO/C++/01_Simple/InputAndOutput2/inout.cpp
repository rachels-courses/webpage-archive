#include <iostream>     // Console streams
#include <fstream>      // File streams
using namespace std;

int main()
{
    const int MAX_COURSES = 4;
    string courses[MAX_COURSES];

    for ( int i = 0; i < MAX_COURSES; i++ )
    {
        cout << "Enter in course " << (i+1) << ": ";
        getline( cin, courses[i] );
    }

    // File output
    ofstream output;            // ofstream = output-file-stream
    output.open( "course.txt" );

    for ( int i = 0; i < MAX_COURSES; i++ )
    {
        output << courses[i] << endl;
    }

    output.close();


    // File input
    ifstream input;
    input.open( "course.txt" );

    for ( int i = 0; i < MAX_COURSES; i++ )
    {
        getline( input, courses[i] );
    }

    for ( int i = 0; i < MAX_COURSES; i++ )
    {
        cout << courses[i] << endl;
    }

    return 0;
}
