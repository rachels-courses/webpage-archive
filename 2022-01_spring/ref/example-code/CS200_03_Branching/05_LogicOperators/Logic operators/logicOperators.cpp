#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    // AND: &&
    // OR:  ||
    // NOT: !

    int grade;
    cout << "Enter your grade (0 - 100): ";
    cin >> grade;

    if ( grade >= 0 && grade <= 100 )
    {
        cout << "Valid grade range" << endl;
    }
    else
    {
        // grade < 0 or grade > 100
        cout << "Invalid grade range" << endl;
    }

    if ( grade < 0 || grade > 100 )
    {
        cout << "Invalid grade range" << endl;
    }
    else
    {
        // grade >= 10 and grade <= 100
        cout << "Valid grade range" << endl;
    }

    //   a && b
    // !(a && b)    == !a || !b

    //    c || d
    // !( c || d )  == !c && !d

    return 0;
}

