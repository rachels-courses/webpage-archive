#ifndef _ROOM_MANAGER_HPP
#define _ROOM_MANAGER_HPP

#include "Room.hpp"

class RoomManager
{
    public:
    static void Setup();
    static void Cleanup();

    static void LoadMapData();
    static int GetIndexOfRoomWithName( string name );
    static void TryToMove( string direction );

    static void DisplayCurrentRoom();

    private:
    static void AllocateSpace( int size );
    static void DeallocateSpace();

    static Room * s_rooms;             // dynamic array
    static int s_totalRooms;
    static Room * s_ptrCurrentRoom;    // pointer to room
};

#endif
