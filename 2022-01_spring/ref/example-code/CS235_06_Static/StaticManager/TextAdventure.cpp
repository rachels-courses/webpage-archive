#include "TextAdventure.hpp"

#include "RoomManager.hpp"

#include <iostream>
#include <fstream>
using namespace std;

TextAdventure::TextAdventure()
{
    m_isDone = false;
    RoomManager::Setup();
}

TextAdventure::~TextAdventure()
{
    RoomManager::Cleanup();
}

void TextAdventure::Run()
{
    string command;
    while ( !m_isDone )
    {
        RoomManager::DisplayCurrentRoom();
        command = GetUserCommand();

        if ( command == "quit" || command == "q" )
        {
            m_isDone = true;
        }
        else
        {
            RoomManager::TryToMove( command );
        }
    }
}


string TextAdventure::GetUserCommand()
{
    cout << endl << endl;
    cout << "Move commands: north / south / east / west" << endl;
    cout << "Game commands: quit" << endl;
    cout << endl;

    string command;
    cout << "Command: ";
    cin >> command;

    // make it lower-case
    for ( unsigned int i = 0; i < command.size(); i++ )
    {
        command[i] = tolower( command[i] );
    }

    return command;
}

