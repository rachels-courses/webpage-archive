#include <iostream>
using namespace std;

int main()
{
    float room1width, room1length, room1area;
    float room2width, room2length, room2area;
    float room3width, room3length, room3area;

    bool done = false;
    while ( !done )
    {
        cout << endl << endl;
        cout << "MAIN MENU" << endl;
        cout << "1. Enter room 1" << endl;
        cout << "2. Enter room 2" << endl;
        cout << "3. Enter room 3" << endl;
        cout << "4. View house stats" << endl;
        cout << "5. Quit" << endl;

        int choice;
        cout << endl << "Your choice is: ";
        cin >> choice;

        cout << endl << endl;

        if ( choice == 1 )
        {
            cout << "ROOM 1 INFORMATION" << endl;
            cout << "Enter room 1 width: ";
            cin >> room1width;
            cout << "Enter room 1 length: ";
            cin >> room1length;
        }
        else if ( choice == 2 )
        {
            cout << "ROOM 2 INFORMATION" << endl;
            cout << "Enter room 2 width: ";
            cin >> room2width;
            cout << "Enter room 2 length: ";
            cin >> room2length;
        }
        else if ( choice == 3 )
        {
            cout << "ROOM 3 INFORMATION" << endl;
            cout << "Enter room 3 width: ";
            cin >> room3width;
            cout << "Enter room 3 length: ";
            cin >> room3length;
        }
        else if ( choice == 4 )
        {
            room1area = room1length * room1width;
            room2area = room2length * room2width;
            room3area = room3length * room3width;
            float houseArea = room1area + room2area + room3area;

            cout << "Room 1:"
                << endl << "\t " << room1width << "x" << room1length
                << endl << "\t Area is " << room1area << " sqft" << endl << endl;

            cout << "Room 2:"
                << endl << "\t " << room2width << "x" << room2length
                << endl << "\t Area is " << room2area << " sqft" << endl << endl;

            cout << "Room 3:"
                << endl << "\t " << room3width << "x" << room3length
                << endl << "\t Area is " << room3area << " sqft" << endl << endl;

            cout << "House area: " << houseArea << " sqft" << endl;
        }
        else if ( choice == 5 )
        {
            done = true;
        }
    }

    return 0;
}
