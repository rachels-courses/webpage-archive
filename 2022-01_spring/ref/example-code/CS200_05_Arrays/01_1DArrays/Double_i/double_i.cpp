#include <iostream>
#include <string>
using namespace std;

int main()
{
	const int ARRAY_SIZE = 5;

	int doubles[ARRAY_SIZE];

	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		doubles[i] = i * 2;
		cout << "Item " << i << " = " << doubles[i] << endl;
	}

	return 0;
}
