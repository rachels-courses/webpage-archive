#include <iostream>
#include <string>
using namespace std;

int main()
{
    string text = "helloworld";
    
    cout << "Original text: " << text << endl;
    
    int start;
    int length;
    
    cout << "Enter position to begin erasing: ";
    cin >> start;
    
    cout << "Enter length of text to erase: ";
    cin >> length;
    
    text = text.erase( start, length );    
    cout << endl << "String is now: " << text << endl;
    
    return 0;
}
