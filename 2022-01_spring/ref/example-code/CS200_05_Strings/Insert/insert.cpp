#include <iostream>
#include <string>
using namespace std;

int main()
{
    string text = "helloworld";
    
    cout << "Original text: " << text << endl;
    
    int start;
    string insertText;
    
    cout << "Enter text to insert: ";
    getline( cin, insertText );
    
    cout << "Enter position to insert: ";
    cin >> start;
    
    text = text.insert( start, insertText );
    
    cout << endl << "String is now: " << text << endl;
    
    return 0;
}
