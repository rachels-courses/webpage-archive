#include <iostream>
#include <string>
using namespace std;

int main()
{
    string first;
    string second;
    
    cout << "Enter first string: ";
    cin >> first;
    
    cout << "Enter second string: ";
    cin >> second;
    
    int order = first.compare( second );
    
    cout << endl << "Result: " << order << endl;
    
    return 0;
}
