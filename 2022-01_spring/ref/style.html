<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Style Guide (Course Commons) </title>
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="icon" type="image/png" href="../web-assets/favicon.png">

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <!-- bootstrap -->
  <link rel="stylesheet" href="../web-assets/bootstrap-dark/css/bootstrap-dark.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- highlight.js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

  <!-- rachel's stuff -->
  <link rel="stylesheet" href="../web-assets/style/2020_10.css">
  <script src="../web-assets/js/header-bar.js"></script>
</head>
<body>
    <div id="data" style="display: none;">
        <input type="text" id="index-data" value="4">
    </div> <!-- data -->

    <div class="container-fluid exercise">
        <div id="course-links"></div> <!-- JS inserts links -->
        <h1 id="class-info"> Quick Reference - Style guide </h1><hr>
        
        
        <div class="container-fluid">
            
        <!-- ============================= PAGE CONTENT BEGIN ============================= -->
            
            <div class="row">
                <div class="col-md-12">
                    
                    <h2>Style guide</h2>
                    
                    <p>
                        When programming, it is important to maintain a consistent coding style
                        to maintain readability. Each language has its own specified standard and
                        each project will also generally have its own standard. When working alone,
                        you can develop your own sense of style, but you should remain consistent.
                    </p>
                    
                    <p>
                        More information:
                    </p>
                    <ul>
                        <li><a href="https://en.wikipedia.org/wiki/Indentation_style">Indentation styles</a></li>
                        <li><a href="https://en.wikipedia.org/wiki/Naming_convention_(programming)">Naming conventions</a></li>
                    </ul>
                    
                    <br><br>
                    
                    <h3>Naming conventions</h3>
                    
                        <p>
                            What should our variable names, function names, class names look like?
                            Again, it depends on the source. As you come up with your
                            personal style, it is mostly important to be <strong>consistent</strong>
                            - don't change up your naming style haphazardly.
                        </p>
                        
                        <h4 style="margin-top:0;">Variables</h4>
                        
                        <p>
                            The C++ standard writes variables like this:
                            <pre><code>int amount_of_cats;</code></pre>
                            where the variable names are all lower-case and
                            underscores _ are used to separate words.
                        </p>
                        
                        <p>
                            Another common style of variable naming is
                            camelCase, like this:
                            <pre><code>int amountOfCats;</code></pre>
                            In this case, the first letter is lower-case
                            but the first letter of each subsequent word is
                            capitalized.
                        </p>
                        
                        <h4>Named constants</h4>
                        
                        <p>
                            It is standard to write named constants' names as:
                            <pre><code>const int AMOUNT_OF_CATS = 100;</code></pre>
                            all upper-case and words separated by underscores.
                        </p>
                        
                        <h4>Functions</h4>
                        
                        <p>
                            Function names in C++ are often styled like this:
                            <pre><code>int AddAllCats(int a, int b, int c);</code></pre>
                            Using CamelCasing with the first letter of each word capitalized.
                        </p>
                        
                        <p>
                            However, functions in the C++ Standard Libraries follow
                            this style:
                            <pre><code>myList.push_back( new_data );</code></pre>
                        </p>
                        
                        <p>(In Java, <code>addAllCats</code> is more common)</p>
                        
                        <h4>Classes</h4>
                        
                        <p>
                            Class naming in C++ often looks like this:
                            <pre><code>class PetCat</code></pre>
                            using CamelCasing with the first letter of each word capitalized.
                        </p>
                        
                        <br><br>
                    
                    <h3>Indentation style</h3>
                    
                        <h4 style="margin-top:0;">Indenting</h4>
                    
                        <p>
                            In C++, we denote the start and end of <strong>code blocks</strong>
                            with curly braces <code>{ }</code>. For example:
<pre><code>if ( catCount == 0 )
{
    AdoptACat();
}</code></pre>
                            Each time a new code block opens, <strong>the code within should be indented forward by 1 tab (or equivalent spaces)</strong>.
                        </p>
                        
                        <div class="row">
                            <div class="col-md-6 green-highlight">
                                <p><strong>Do this</strong></p>
<pre><code>for ( int y = 0; y &lt; 5; y++ )
{
    for ( int x = 0; x &lt; 10; x++ )
    {
        if ( image[x][y] == "fence" )
        {
            cout &lt;&lt; "#";
        }
        else if ( image[x][y] == "player" )
        {
            cout &lt;&lt; "@";
        }
        else
        {
            cout &lt;&lt; ".";
        }
    }
    cout &lt;&lt; endl;
}</code></pre>
                            </div>
                            <div class="col-md-6 red-highlight">
                                <p><strong>NOT this</strong></p>
<pre><code>for ( int y = 0; y &lt; 5; y++ )
{
for ( int x = 0; x &lt; 10; x++ )
{
if ( image[x][y] == "fence" )
{
cout &lt;&lt; "#";
}
else if ( image[x][y] == "player" )
{
cout &lt;&lt; "@";
}
else
{
cout &lt;&lt; ".";
}
}
cout &lt;&lt; endl;
}</code></pre>
                            </div>
                        </div>
                        
                        <h4>Curly braces</h4>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    According to the C++ standard, usage of curly braces should look like this:
<pre><code>struct Cable {
    int x;
    // ...
};

double Example( int x, int y )
{
    if ( x == y ) {
        // Multiple lines of code
    }

    if      ( x &lt; 0 )   
        negative_case();
    else if ( x &gt; 0 )   
        positive_case();
    else                
        zero_case();

    return some_value;
}
</code></pre>
                                </p>
                                <p>
                                    Curly braces start on the same line as a class/struct declaration
                                    and with if statements and other control flow, but on a new line
                                    for functions.
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                    However, I <em>prefer</em> having opening curly braces start on the next line,
                                    and always having curly braces even for one line statements:
<pre><code>struct Cable 
{
    int x;
    // ...
};

double Example( int x, int y )
{
    if ( x == y ) 
    {
        // Multiple lines of code
    }

    if      ( x &lt; 0 )   
    {
        negative_case();
    }
    else if ( x &gt; 0 )   
    {
        positive_case();
    }
    else                
    {
        zero_case();
    }

    return some_value;
}
</code></pre>
                                </p>
                                <p>
                                    I think it makes it easier to detect missing curly braces if braces
                                    for the same block are on the same level.
                                    <br><br>
                                    Additionally, I put in curly braces even for one line statements because
                                    if you need to go back and update it to add more lines later, it is
                                    faster to add stuff in if the curly braces are already there.
                                </p>
                            </div>
                        </div>
                        
                        
                        
                
                    <h3>Tabs vs. Spaces</h3>
                    
                        <p>
                            I don't care what you use, just be consistent. I prefer spaces.
                        </p>
                        
                        <p>
                            Your indentation length usually defaults to 4, and that's fine.
                            You can also set it to 2. I would avoid choosing anything
                            besides 2 or 4.
                        </p>
                        
                    <br><br>
                    
                    <h3>Comments</h3>
                    
                        <p>
                            I generally don't write comments in my code very much from my industry experience because
                            when working on a project over time, it is very easy for code to be updated but the comments
                            to be forgotten. When the comments become stale, they can also become misleading.
                            Instead, it is better to write clear code that is "self-documenting" as much as possible.
                            <br><br>
                            I mostly just write documentation-style comments for functions when writing something that
                            will be used in multiple projects, such as a data structure.
                            <br><br>
                            Generally with our projects in the classes, your intent is usually clear enough that I do not
                            need you to add comments to clarify what you're doing. Comments should be to clarify
                            something like a formula or something else that is non-obvious from just looking at it.
                        </p>
                    
                    <br><br>
                
                    <h3>Examples</h3>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <p><strong>Stroustrup style</strong></p>
                            
                            <p><a href="https://en.wikipedia.org/wiki/Indentation_style#Variant:_Stroustrup">(Wiki page)</a></p>
                               
                                <p>
                                    Per the <a href="https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#nl17-use-kr-derived-layout">C++ Core Guidelines</a>,
                                    the indentation and curly-brace style looks like this:
                                </p>
                    
<pre><code>struct Cable {
    int x;
    // ...
};

double Example(int x, int y)
{
    if (x == y) {
        // Multiple lines of code
    }

    if      (x &lt; 0)   
        negative_case();
    else if (x &gt; 0)   
        positive_case();
    else                
        zero_case();

    return some_value;
}
</code></pre>
                        </div>
                        
                        <div class="col-md-4">
                            <p><strong>Horstmann style</strong></p>
                            
                            <p><a href="https://en.wikipedia.org/wiki/Indentation_style#Horstmann_style">(Wiki page)</a></p>
                            
                            <p>
                                This style is also somewhat common in C++.
                            </p>
<pre><code>struct Cable 
{
    int x;
    // ...
};

double Example(int x, int y)
{
    if ( x == y ) 
    {
        // Multiple lines of code
    }

    if      (x &lt; 0)   
    {
        negativeCase();
    }
    else if (x &gt; 0)   
    {
        positiveCase();
    }
    else                
    {
        zeroCase();
    }

    return some_value;
}</code></pre>
                        </div>
                        
                        <div class="col-md-4">
                            <p><strong>Rachel's style</strong></p>
                            
                            <p>
                                This is my personal style of writing C++ code...
                            </p>
                            
                            
<pre><code>struct Cable 
{
    int x;
    // ...
};

double Example( int x, int y )
{
    if ( x == y )
    {
        // Multiple lines of code
    }

    if      ( x &lt; 0 )   { NegativeCase(); }
    else if ( x &gt; 0 )   { PositiveCase(); }
    else                { ZeroCase(); }

    return someValue;
}</code></pre>

                            <ul>
                                <li>I've found that highlighting conditions and parameters within ( ) with a mouse is easier if there is spacing between the item and the parentheses ( ), thus I put spacing there.</li>
                                <li>I prefer my curly braces on new lines, though if I am writing a one-line statement with in an if statement and I <em>know</em> that I'm not going to expand it later, I usually opt for the same line, and still keeping the curly braces.</li>
                                <li>I generally prefer explicitness to implicitness.</li>
                                <li>I am more used to camelCase variable names.</li>
                            </ul>

                        </div>
                    </div>
                    
                 
                    
                    
                    
                                
                </div> <!-- main page info -->
            </div> <!-- row -->
            
        </div> <!-- container -->

        <hr>

        <section class="">
        </section> <!-- footer -->
    </div> <!-- container-fluid -->
</body>
