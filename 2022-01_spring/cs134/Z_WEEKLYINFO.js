// CS 200
$( document ).ready( function() {
    
    firstMondayOfClass = new Date( 2022, 00, 17 );
    
    // JS really doesn't store this info?
    freakingMonths = [ "Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
    
    
    weekInfo = [
        //{   "week" : "1", 
            //"units" : [ "Unit 1" ],
            //"notes" : "First day of spring credit classes. Monday, Jan 17th, Martin Luther King Jr. Day - College closed."
        //},
        //{   "week" : "2", 
            //"units" : [ "Unit 2" ],
            //"notes" : "Jan 25 - Last day to drop a full-semester spring course and receive a 100-percent refund."
        //},
        //{   "week" : "3", 
            //"units" : [ "Unit 3" ],
            //"notes" : ""
        //},
        //{   "week" : "4", 
            //"units" : [ "Unit 4" ],
            //"notes" : ""
        //},
        //{   "week" : "5", 
            //"units" : [ "BREAK 1" ],
            //"notes" : "Feb 14 - Last day to drop a full-semester spring course without a withdrawal “W” on the student’s permanent record.<br>Feb 15 - Application deadline to apply for spring graduation."
        //},
        //{   "week" : "6", 
            //"units" : [ "Unit 5" ],
            //"notes" : ""
        //},
        //{   "week" : "7", 
            //"units" : [ "Unit 6" ],
            //"notes" : ""
        //},
        //{   "week" : "8", 
            //"units" : [ "Unit 7" ],
            //"notes" : "Mar 1 - Summer schedule of classes becomes available on the web."
        //},
        //{   "week" : "9", 
            //"units" : [ "BREAK 2" ],
            //"notes" : "Mar 14 to 20 - Spring Break; classes not in session. College offices open Monday through Friday.<br><br>Happy Holi!"
        //},
        //{   "week" : "10", 
            //"units" : [ "Unit 8" ],
            //"notes" : ""
        //},
        //{   "week" : "11", 
            //"units" : [ "Unit 9" ],
            //"notes" : "Ramadan Mubarak!"
        //},
        //{   "week" : "12", 
            //"units" : [ "Unit 10" ],
            //"notes" : "April 6 - Open enrollment for summer begins at 9 p.m. on the web."
        //},
        //{   "week" : "13", 
            //"units" : [ "BREAK 3" ],//"Unit 11" ],
            //"notes" : "Apr 15 - Last day to request a pass/fail grade option or to withdraw with a “W” from a full semester spring course.<br><br>Happy Easter!<br>Happy Passover!"
        //},
        //{   "week" : "14", 
            //"units" : [ "Unit 11" ],
            //"notes" : "Apr 18 - Open enrollment for fall begins at 9 p.m. on the web."
        //},
        //{   "week" : "15", 
            //"units" : [ "Unit 12" ],
            //"notes" : ""
        //},
        //{   "week" : "16", 
            //"units" : [ "Turn in all remaining items! No final exam." ],// "Unit 12" ],
            //"notes" : ""
        //},
        //{   "week" : "17", 
            //"units" : [ "FINALS WEEK (No final exam for this class)" ],
            //"notes" : "May 10 to May 16 - Scheduled spring final exams for students (<a href='https://www.jccc.edu/_resources/files/final-exam-calendars/spring-final-exam-schedule.pdf'>Final exam schedule</a>)"
        //},
        //{   "week" : "18", 
            //"units" : [ "SEMESTER OVER. YOU DID IT!!" ],
            //"notes" : "May 19 - Spring grades available to students by noon on the web."
        //},
    ];
} );
