// CS 200
$( document ).ready( function() {

  unitInfo = {
  "Unit 1" : {
      "link" : "unit01.html",
      "topic" : "Introduction and setup",
      "reminders" : [
          "If you have trouble setting anything up, send me an email via Canvas! We can work together via email or schedule a Zoom meeting to step through everything."
      ],
      "lectures" :        
      [   
        { 
          "name" : "SPRING 2022 INTRODUCTION / SYLLABUS", 
          "url" : "https://youtu.be/F-_Im-lzRj4" 
        },
        { 
          "name" : "Introduction", 
          "url" : "http://lectures.moosader.com/cs200/00-Introduction.mp4" 
        },
        { 
          "name" : "Integrated Development Environments", 
          "url" : "http://lectures.moosader.com/cs200/01-IDEs.mp4" 
        },
      ],
      "reading" :         
      [   
        { 
          "name" : "Chapter 1: Introduction", 
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter1-introduction.pdf" 
        },
        { 
          "name" : "Chapter 2: Writing programs", 
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter2-writing_programs.pdf" 
        },
      ],
      "exercises" :       
      [   
        { 
          "key" : "INTRO", "name" : "👋 The first assignment!",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2478419",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2478501",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },
        { 
          "key" : "SETUP1", "name" : "Installing an IDE",
          "docs" : "ASSIGNMENT_SETUP1_IDE.html",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2478424",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2478506",
          "due" : "Jan 21",
          "end" : "Jan 21",
        },
        { 
          "key" : "SETUP2", "name" : "Creating a GitLab account",
          "docs" : "ASSIGNMENT_SETUP2_GitLab.html",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2478425",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2478507",
          "due" : "Jan 21",
          "end" : "Jan 21",
        },
        { 
          "key" : "SETUP3", "name" : "Installing Git",
          "docs" : "ASSIGNMENT_SETUP3_Git.html",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2478426",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2478508",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },
      ],
      "tech-literacy" :   
      [   
        { 
          "key" : "TL1", "name" : "How computers and software work",
          "docs" : "ASSIGNMENT_TL1_HowComputersWork.html",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2478428",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2478510",
          "due" : "Jan 26",
          "end" : "Jan 26",
        },
      ],
    },

    "Unit 2" : {
      "link" : "unit02.html",
      "topic" : "C++ Basics",//"Input, output, storing data, and doing math",
      
      
      "lectures" :        [   
        { "name" : "C++ Basics",                                                
            "url" : "http://lectures.moosader.com/cs200/02-CPP-Basics.mp4" 
        },
        { "name" : "Coding examples (optional)",                                           
            "url" : "http://lectures.moosader.com/cs200/example-code/cs200-variable-practice.mp4" 
        },
      ],
      "reading" :         [   
        { "name" : "Chapter 3: Variables and data types",                       
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter3-variables.pdf" 
        },
        { "name" : "Chapter 4: Input and output",                               
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter4-input_output.pdf" 
        },
      ],
      
      "quizzes" :         
      [   
        { "key" : "U02Q", "name" : "Program basics, console input and output, and variables",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2505286",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2505287",
            "due" : "Feb 2",
            "end" : "Feb 4",
        },
      ],
      
      "archives" :        [   
        { "name" : "Program basics lecture (Spring 2021)",                              
          "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-01-26_cs200_lecture.mp4" 
        },
        { "name" : "Variables lecture (Spring 2021)",                              
          "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-01-28_cs200_lecture.mp4" 
        },
      ],
      
      "exercises" :       
      [   
        { "key" : "U02E", "name" : "C++ Basics - Recipe program",
            "docs" : "ASSIGNMENT_U02E_RecipeProgram.html",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2583374",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2583400",
            "due" : "Feb 2",
            "end" : "Feb 4",
        },
        {
          "key"   : "C1", "name" : "Check-in (Week 2)",
          "350"   : "https://canvas.jccc.edu/courses/52433/modules/items/2584885",
          "378"   : "https://canvas.jccc.edu/courses/52449/modules/items/2584886",
          "due"   : "Jan 28",
          "end"   : "Jan 29",
        },
      ],
    },

    "Unit 3" : {
      "link" : "unit03.html",
      "topic" : "Branching with if statements and switch statements",
      "lectures" :        
      [   
        { "name" : "Truth tables (from CS 134)",                                
            "url" : "http://lectures.moosader.com/cs134/cs134_04d_truthtables.mp4" 
        },
        { "name" : "Branching",                                                 
            "url" : "http://lectures.moosader.com/cs200/03-Branching.mp4" 
        },
        { "name" : "Example code - Branching",                                   
            "url" : "https://www.youtube.com/watch?v=hJss2U5GqBM" 
        },
      ],
      "reading" :         
      [   
        { "name" : "Chapter 5: Control flow (5.1, 5.2)",                        
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter5-control_flow.pdf" 
        },
      ],
      "archives" :        
      [  
        { "name" : "If statements lecture (Summer 2021)",                                   
            "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-02_cs200_lectureB_if_statements.mp4" 
        },
        { "name" : "Switch statements lecture (Summer 2021)",                                   
            "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-04_cs200_lecture.mp4" 
        },
      ],
      "exercises" :       
      [   
        { "key" : "U03E", "name" : "Branching",
            "docs" : "ASSIGNMENT_U03E_Branching.html",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2589263",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2589264",
            "due" : "Feb 9",
            "end" : "Feb 14",
        },
      ],      
      "quizzes" :         
      [   
        { "key" : "U03Q", "name" : "Branching",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2595379",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2595387",
            "due" : "Feb 9",
            "end" : "Feb 16",
        },
        { "key" : "DEBUG1", "name" : "Debugging - Basics",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2595377",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2595385",
            "due" : "Feb 28",
            "end" : "Feb 28",
        },
        { "key" : "DEBUG2", "name" : "Debugging - Branching",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2595378",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2595386",
            "due" : "Feb 28",
            "end" : "Feb 28",
        },
      ],
    },

    "Unit 4" : {
      "link" : "unit04.html",
      "topic" : "While loops and Project 1",
      
      "exams" :           
      [   
        { "key" : "T1",   "name" : "Test: Program basics",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2596721",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2596739",
          "due" : "Mar 2",
          "end" : "Mar 2",
        },
        
        { "key" : "T2",   "name" : "Test: Branching",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2596722",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2596740",
          "due" : "Mar 2",
          "end" : "Mar 2",
        },
        
        { "key" : "T3",   "name" : "Test: While loops",
          "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2596723",
          "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2596741",
          "due" : "Mar 2",
          "end" : "Mar 2",
        },
      ],
      
      "lectures" :        
      [   
        { "name" : "Looping",                                
            "url" : "http://lectures.moosader.com/cs200/04-Loops.mp4" 
        },
        { "name" : "Example code - While loops",                                 
            "url" : "https://www.youtube.com/watch?v=xLGqVI7vGvA" 
        },
        { "name" : "Example code - Pizza maker (variables, if statements, while loops) (optional)",                                   
            "url" : "https://www.youtube.com/watch?v=Y0LRamH_J3s" 
        },
      ],
      "reading" :         
      [   
        { "name" : "Chapter 5: Control flow (5.3, 5.4)",                        
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter5-control_flow.pdf" 
        },
      ],
      "archives" :        
      [  
        { "name" : "While loops (Summer 2021)",                                 
            "url" : "http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-22_cs200_lecture_whileloops.mp4" 
        },
      ],
      "exercises" :       
      [   
        { "key" : "U04E", "name" : "While loops",
            "docs" : "ASSIGNMENT_U04E_WhileLoops.html",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2596697",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2596707",
            "due" : "Feb 16",
            "end" : "Feb 21",
        },
      ],      
      "quizzes" :         
      [   
        { "key" : "U04Q", "name" : "While Loops",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2596711",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2596714",
            "due" : "Feb 16",
            "end" : "Feb 23",
        },
      ],
      
      "projects" :        
      [   
        { "key" : "P1",     "name" : "Kinejo Theaters Project (v1): Variables, Branching, Loops",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52433/assignments/1068998?module_item_id=2597359",
            "378" : "https://canvas.jccc.edu/courses/52449/assignments/1069066?module_item_id=2597360",
            "due" : "Feb 28",
            "end" : "Feb 28",
        },
      ],
    },

    "Unit 5" : {
      "link" : "unit05.html",
      "topic" : "Arrays and Pointers",
      "lectures" :        
      [   
        { "name" : "Arrays",
            "url" : "http://lectures.moosader.com/cs200/08-Arrays.mp4"
        },
        { "name" : "Loops",
            "url" : "http://lectures.moosader.com/cs200/04-Loops.mp4"
        },
        { "name" : "Example code - For loops",                                 
            "url" : "https://www.youtube.com/watch?v=4o9fWpwVWXU" 
        },
        // Add an example coding video (arrays)
      ],
      "archives" :        [   
          { "name" : "Arrays lecture (Spring 2021)",
              "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-23_cs200_lecture.mp4"
          },
          { "name" : "For loops lecture (Spring 2021)",
              "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-11_cs200_lecture.mp4"
          },
      ],
      "reading" : 
      [
        { "name" : "Chapter 5: Control flow (Just 5.3, 5.4)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter5-control_flow.pdf"
        },
        { "name" : "Chapter 6: Arrays",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter6-arrays.pdf"
        },
        { "name" : "Chapter 14: Pointers (Just 14.1, 14.2)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter14-pointers_and_memory.pdf"
        },
      ],
      "exercises" :       
      [   
        {
          "key"   : "C2", "name" : "Check-in (Week 5)",
          "350"   : "https://canvas.jccc.edu/courses/52433/modules/items/2607266",
          "378"   : "https://canvas.jccc.edu/courses/52449/modules/items/2607267",
          "due"   : "Feb 18",
          "end"   : "Feb 28",
        },
        {
          "key"   : "U05E", "name" : "Arrays, For Loops, and Pointers",
          "docs" : "ASSIGNMENT_U05E_ArraysPointers.html",
          "350"   : "https://canvas.jccc.edu/courses/52433/modules/items/2608171",
          "378"   : "https://canvas.jccc.edu/courses/52449/modules/items/2608170",
          "due"   : "Mar 2",
          "end"   : "Mar 8",
        },
      ],    
      "quizzes" :         
      [   
        { "key" : "U05Q1", "name" : "Arrays",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2608259",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2608264",
            "due" : "Mar 2",
            "end" : "Mar 8",
        },
        { "key" : "U05Q2", "name" : "For Loops",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2608258",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2608265",
            "due" : "Mar 2",
            "end" : "Mar 8",
        },
      ],
    },

    "Unit 6" : {
      "link" : "unit06.html",
      "topic" : "Strings and File I/O",
      
      "lectures" :        
      [   
        { 
          "name" : "Strings",
          "url" : "http://lectures.moosader.com/cs200/07-Strings.mp4"
        },
        { 
          "name" : "File I/O",
          "url" : "http://lectures.moosader.com/cs200/14-File-IO.mp4"
        },
        {
          "name" : "Arrays and File I/O example code",
          "url" : "https://www.youtube.com/watch?v=OVGSP7xkQHg"
        },
      ],
      "reading" :         
      [   
        { 
          "name" : "Chapter 7: Strings",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter7-strings.pdf"
        },
        { 
          "name" : "Chapter 8: File I/O",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter8-file_input_and_output.pdf"
        },
      ],
      "archives" :        
      [   
        { "name" : "Strings lecture (Spring 2021)",
          "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-25_cs200_lectureB_strings.mp4"
        },
        {
          "name" : "File I/O lecture (Spring 2021)",
          "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-02_cs200_lecture_fileio.mp4"
        },
      ],
      "exercises" :       
      [ 
        {
          "key"   : "U06E1", "name" : "String operations",
          "docs"  : "ASSIGNMENT_U06E1_Strings.html",
          "350"   : "https://canvas.jccc.edu/courses/52433/modules/items/2617111",
          "378"   : "https://canvas.jccc.edu/courses/52449/modules/items/2617112",
          "due"   : "Mar 9",
          "end"   : "Mar 18",
        },
        {
          "key"   : "U06E2", "name" : "File input and output",
          "docs"   : "ASSIGNMENT_U06E2_SavingAndLoadingFiles.html",
          "350"   : "https://canvas.jccc.edu/courses/52433/modules/items/2617114",
          "378"   : "https://canvas.jccc.edu/courses/52449/modules/items/2617115",
          "due"   : "Mar 16",
          "end"   : "Mar 23",
        },
      ],
      "quizzes" :         
      [   
      ],
      "tech-literacy" :   
      [   
      ],
    },

    "Unit 7" : {
      "link" : "unit07.html",
      "topic" : "Function basics, Project 2",//"Delegating tasks with functions, writing automated testing functions",
      
      "lectures" :        
      [   
        { 
          "name" : "Functions",
          "url" : "http://lectures.moosader.com/cs200/05-Functions.mp4"
        },
      ],
      "reading" :         
      [   
        { 
          "name" : "Chapter 9: Functions",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter9-functions.pdf"
        },
      ],
      "exercises" :       
      [ 
        {
          "key"   : "U07E", "name" : "Inputs and Outputs with functions",
          "docs"  : "ASSIGNMENT_U07E_FunctionBasics.html",
          "350"   : "https://canvas.jccc.edu/courses/52433/modules/items/2622089",
          "378"   : "https://canvas.jccc.edu/courses/52449/modules/items/2622090",
          "due"   : "Mar 16",
          "end"   : "Mar 19",
        },
      ],
      
      "projects" :        
      [   
        { "key" : "P2",     "name" : "Kinejo Theaters Project (v2): Arrays and Functions",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52433/modules/items/2622091",
            "378" : "https://canvas.jccc.edu/courses/52449/modules/items/2622092",
            "due" : "Mar 31",
            "end" : "Apr 1",
        },
      ],
    },

    "Unit 8" : {
      "link" : "unit08.html",
      //"topic" : "STL Structures (Vectors, List, Map)",//"Introduction to object oriented programming with classes and structs",
      "topic" : "More with Functions, Unit Tests",
      
      "lectures" :        
      [   
        // Add an example coding video
        { 
          "name" : "Functions",
          "url" : "http://lectures.moosader.com/cs200/05-Functions.mp4"
        },
        { 
          "name" : "Const, part 1",
          "url" : "http://lectures.moosader.com/cs200/09-Const.mp4"
        },
        { 
          "name" : "Functions and Unit Tests",
          "url" : "https://www.youtube.com/watch?v=PUwjMpLsXXE"
        },
      ],
      "reading" :         
      [   
        { 
          "name" : "Chapter 9: Functions",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter9-functions.pdf"
        },
        { 
          "name" : "Chapter 13: All about const (13.2, 13.3)",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter13-const.pdf"
        },
        {
          "name" : "Chapter 19: Testing",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter19-testing.pdf"
        },
      ],
      "archives" :        
      [   
        { "name" : "Functions lecture, part 1 (Spring 2021)",
          "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-16_cs200_lecture_functions.mp4"
        },
        { "name" : "Functions lecture, part 2 (Spring 2021)",
          "url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-18_cs200_lecture_functions_arrays_and_const.mp4"
        },
      ],
      "exercises" :       
      [ 
      ],
      "quizzes" :         
      [   
      ],
      "tech-literacy" :   
      [   
      ],
    },

    "Unit 9" : {
      "link" : "unit09.html",
      "topic" : "Classes and Object Oriented Programming",//"More object oriented programming",
      
      "lectures" :        
      [   
        { "name" : "Structs",         "url" : "http://lectures.moosader.com/cs200/10-Struct.mp4" },
        { "name" : "Classes, part 1", "url" : "http://lectures.moosader.com/cs200/11-Class-1.mp4" },
        // Add an example coding video
      ],
      "reading" :         
      [   
        { "name" : "Chapter 10: Basic Object Oriented Programming", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter10-oop1.pdf" },
      ],
      "archives" :        
      [   
        { "name" : "Structs and Classes lecture (Fall 2021)",
            "url" : "https://www.youtube.com/watch?v=EpbnaiGB8QY"
        },
      ],
      "exercises" :       
      [ 
      ],
      "quizzes" :         
      [   
      ],
      "tech-literacy" :   
      [   
      ],
    },

    "Unit 10" : {
      "link" : "unit10.html",
      "topic" : "Inheritance and Project 3",//"Memory management, pointers, and dynamic memory allocation",
      
      "lectures" :        
      [   
        { 
          "name" : "Classes, part 2",
          "url" : "http://lectures.moosader.com/cs200/12-Class-2.mp4"
        },
        { 
          "name" : "Inheritance",
          "url" : "http://lectures.moosader.com/cs200/15-Inheritance.mp4"
        },
        // Add an example coding video
      ],
      "reading" :         
      [   
        { 
          "name" : "Chapter 11: Intermediate Object Oriented Programming",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf"
        },
      ],
      "archives" :        
      [   
        { "name" : "Inheritance lecture (Summer 2021)",
          "url" : "http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-08_cs200_lecture_inheritance.mp4"
        },
      ],
      "exercises" :       
      [ 
      ],
      "quizzes" :         
      [   
      ],
      "tech-literacy" :   
      [   
      ],
    },

    "Unit 11" : {
      "link" : "unit11.html",
      "topic" : "Object Oriented Design, Memory Management",
      
      "lectures" :        
      [   
        { 
          "name" : "Pointers",
          "url" : "http://lectures.moosader.com/cs200/16-Pointer.mp4"
        },
        { 
          "name" : "Memory management",
          "url" : "http://lectures.moosader.com/cs200/17-Memory-Management.mp4"
        },
        { 
          "name" : "Dynamic arrays",
          "url" : "http://lectures.moosader.com/cs200/18-Dynamic-Arrays.mp4"
        },
        
        // Add an example coding video
      ],
      "reading" :         
      [ 
        { 
          "name" : "Chapter 14: Pointers, memory management, and dynamic variables and arrays",
          "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter14-pointers_and_memory.pdf"
        },
      ],
      "archives" :        
      [ 
      ],
      "exercises" :       
      [ 
      ],
      "quizzes" :         
      [   
      ],
      "tech-literacy" :   
      [   
      ],
    },

    "Unit 12" : {
      "link" : "unit12.html",
      "topic" : "Student projects",
      
    },

    "TEMPLATE" : {
      //"link" : "",
      //"topic" : "",
      //"lectures" :        
      //[   
        //{ "name" : "",
            //"url" : ""
        //},
        //{ "name" : "",
            //"url" : ""
        //},
      //],
      //"reading" :         
      //[   
        //{ "name" : "",
            //"url" : ""
        //},
        //{ "name" : "",
            //"url" : ""
        //},
      //],
      //"archives" :        
      //[   
        //{ "name" : "",
          //"url" : ""
        //},
      //],
      //"exercises" :       
      //[   
        //{ "key" : "", "name" : "",
            //"docs" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
                              
        //{ "key" : "", "name" : "",
            //"docs" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
      //],
      //"quizzes" :         
      //[   
        //{ "key" : "", "name" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
                              
        //{ "key" : "", "name" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
      //],
      //"tech-literacy" :   
      //[   
        //{ "key" : "", "name" : "",
            //"docs" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
      //],
      //"projects" :        
      //[   
        //{ "key" : "", "name" : "",
            //"docs" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
      //],
      //"exams" :           
      //[   
        //{ "key" : "", "name" : "",
            //"350" : "",
            //"378" : "",
            //"due" : "",
            //"end" : "",
        //},
      //]
    },
  };

} );

//"lectures" :        [   
        //{ "name" : "Arrays",
            //"url" : "http://lectures.moosader.com/cs200/08-Arrays.mp4"
        //},
        //{ "name" : "Loops",
            //"url" : "http://lectures.moosader.com/cs200/04-Loops.mp4"
        //},
      //],
      //"reading" :         [   
        //{ "name" : "Chapter 5: Control flow (5.3, 5.4)",
            //"url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter5-control_flow.pdf"
        //},
        //{ "name" : "Chapter 6: Arrays",
            //"url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter6-arrays.pdf"
        //},
      //],
      //"archives" :        [   
          //{ "name" : "Arrays, For loops, Strings, and File I/O (Fall 2021)",
              //"url" : "https://youtu.be/lPHn_8PKkYM"
          //}, 
          //{ "name" : "Arrays (Spring 2021)",
              //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-23_cs200_lecture.mp4"
          //},
          //{ "name" : "Strings (Spring 2021)",
              //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-25_cs200_lectureB_strings.mp4"
          //},
          //{ "name" : "File I/O (Spring 2021)",
              //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-02_cs200_lecture_fileio.mp4"
          //},
          //{ "name" : "Example code - For loops",                                 
              //"url" : "https://www.youtube.com/watch?v=4o9fWpwVWXU" 
          //},
      //],
      //"exercises" :       [   
        //{ "key" : "u06eA", "name" : "Arrays",
            //"docs" : "u06eA_Arrays.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946560",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946599",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "u06eB", "name" : "For loops",
            //"docs" : "u06eB_ForLoops.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946561",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946600",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "u06eC", "name" : "Strings",
            //"docs" : "u06eC_Strings.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946562",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946601",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "u06eD", "name" : "Saving and loading data in text files",
            //"docs" : "u06eD_SavingAndLoadingFiles.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946564",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946602",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
      //],
      //"quizzes" :         [  
        //{ "key" : "u06qA", "name" : "Arrays",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946520",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946490",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "u06qB", "name" : "For loops",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946475",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946531",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "u06qC", "name" : "Strings",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946504",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946499",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "u06qD", "name" : "Saving and loading data in text files",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946466",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946488",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
        //{ "key" : "c2",   "name" : "🧑‍🏫 Check-in (October)",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946536",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946580",
            //"due" : "Oct 1",
            //"end" : "Oct 31",
        //},
      //],
      //"tech-literacy" :   [   
        //{ "key" : "u06tl", "name" : "Jobs in software",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946513",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946486",
            //"due" : "Oct 13",
            //"end" : "Oct 20",
        //},
      //],
      //"exams" :           [   
        //{ "key" : "u06tA", "name" : "Arrays and File I/O",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946512",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946511",
            //"due" : "Dec 11",
            //"end" : "Dec 12",
        //},
        //{ "key" : "u06tB", "name" : "For loops",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946484",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946496",
            //"due" : "Dec 11",
            //"end" : "Dec 12",
        //},
      //]
      

      //"archives" :        [   
        //{ "name" : "Functions and Unit Tests(Fall 2021)",
            //"url" : "https://www.youtube.com/watch?v=PUwjMpLsXXE"
        //},
        //{ "name" : "Functions (Spring 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-16_cs200_lecture_functions.mp4"
        //},
        //{ "name" : "Const, pass by reference (Spring 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-18_cs200_lecture_functions_arrays_and_const.mp4"
        //},
      //],
      //"exercises" :       [   
        //{ "key" : "u07eA", "name" : "Functions",
            //"docs" : "u07eA_Functions.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946565",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946603",
            //"due" : "Oct 20",
            //"end" : "Oct 27",
        //},
        //{ "key" : "u07eB", "name" : "Unit tests",
            //"docs" : "u07eB_UnitTests.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946668",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946667",
            //"due" : "Oct 20",
            //"end" : "Oct 27",
        //},
        //{ "key" : "u07eC", "name" : "Const parameters",
            //"docs" : "u07eC_Const.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946568",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946605",
            //"due" : "Oct 20",
            //"end" : "Oct 27",
        //},
      //],
      //"quizzes" :         [   
        //{ "key" : "u07qA", "name" : "Functions",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946465",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946521",
            //"due" : "Oct 20",
            //"end" : "Oct 27",
        //},
        //{ "key" : "d3", "name" : "Debugging - Functions",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946518",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946509",
            //"due" : "Oct 20",
            //"end" : "Oct 27",
        //},
      //],
      //"projects" :        [   
        //{ "key" : "p2", "name" : "Project 2: Functions, Arrays",
            //"docs" : "projects.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946541",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946584",
            //"due" : "Oct 30",
            //"end" : "Nov 7",
        //},
      //],
      //"exams" :           [   
        //{ "key" : "u07t", "name" : "Functions",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946525",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946528",
            //"due" : "Dec 11",
            //"end" : "Dec 12",
        //},
      //]
      
      //"lectures" :        [   
        //{ "name" : "Structs",
            //"url" : "http://lectures.moosader.com/cs200/10-Struct.mp4"
        //},
        //{ "name" : "Classes, part 1",
            //"url" : "http://lectures.moosader.com/cs200/11-Class-1.mp4"
        //},
      //],
      //"reading" :         [   
        //{ "name" : "Chapter 10: Basic Object Oriented Programming",
            //"url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter10-oop1.pdf"
        //},
      //],
      //"archives" :        [   
        //{ "name" : "Structs and Classes (Fall 2021)",
            //"url" : "https://www.youtube.com/watch?v=EpbnaiGB8QY"
        //},
        //{ "name" : "Classes (Summer 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-06_cs200_lecture_classes.mp4"
        //},
      //],
      //"exercises" :       [   
        //{ "key" : "u08eA", "name" : "Intro to structs",
            //"docs" : "u08eA_Structs.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946570",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946606",
            //"due" : "Oct 3",
            //"end" : "Nov 10",
        //},
        //{ "key" : "u08eB", "name" : "Intro to classes",
            //"docs" : "u08eB_Classes.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946571",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946607",
            //"due" : "Oct 3",
            //"end" : "Nov 10",
        //},
      //],
      //"quizzes" :         [   
        //{ "key" : "u08qA", "name" : "Intro to classes",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946470",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946474",
            //"due" : "Oct 3",
            //"end" : "Nov 10",
        //},
        ////{ "key" : "u08qB", "name" : "Debugging - Classes (WIP)",
            ////"350" : "https://canvas.jccc.edu/courses/49304/assignments/946463",
            ////"378" : "https://canvas.jccc.edu/courses/49310/assignments/946501",
            ////"due" : "Oct 27",
            ////"end" : "Nov 3",
        ////},
      //],
      //"projects" :        [   
        //{ "key" : "p2", "name" : "Project 2: Functions, Arrays",
            //"docs" : "projects.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946541",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946584",
            //"due" : "Oct 3",
            //"end" : "Nov 10",
        //},
      //],
      //"tech-literacy" :   [   
        //{ "key" : "u08tl", "name" : "Bias and ethics in tech",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946495",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946530",
            //"due" : "Oct 3",
            //"end" : "Nov 10",
        //},
      //],
      
      //"lectures" :        [   
        //{ "name" : "Classes, part 2",
            //"url" : "http://lectures.moosader.com/cs200/12-Class-2.mp4"
        //},
        //{ "name" : "Inheritance",
            //"url" : "http://lectures.moosader.com/cs200/15-Inheritance.mp4"
        //},
      //],
      //"reading" :         [   
        //{ "name" : "Chapter 11: Intermediate Object Oriented Programming",
            //"url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf"
        //},
      //],
      //"archives" :        [   
        //{ "name" : "Class recording, Nov 2nd, 2021",
            //"url" : "https://youtu.be/zsPZZPpeLNw"
        //},
        //{ "name" : "Inheritance (Summer 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-08_cs200_lecture_inheritance.mp4"
        //},
      //],
      //"exercises" :       [   
        //{ "key" : "u09eA", "name" : "More with Classes",
            //"docs" : "u09eA_MoreClasses.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946572",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946619",
            //"due" : "Nov 10",
            //"end" : "Nov 17",
        //},
        //{ "key" : "u09eB", "name" : "Inheritance",
            //"docs" : "u09eB_Inheritance.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946573",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946609",
            //"due" : "Nov 10",
            //"end" : "Nov 17",
        //},
      //],
      //"quizzes" :         [   
        //{ "key" : "c3", "name" : "🧑‍🏫 Check-in (November)",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946537",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946581",
            //"due" : "Nov 1",
            //"end" : "Nov 30",
        //},
      //],
      //"exams" :           [   
        //{ "key" : "u09t", "name" : "Object-oriented programming",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946464",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946477",
            //"due" : "Dec 11",
            //"end" : "Dec 12",
        //},
      //]

//"lectures" :        [   
        //{ "name" : "Pointers",
            //"url" : "http://lectures.moosader.com/cs200/16-Pointer.mp4"
        //},
        //{ "name" : "Memory management",
            //"url" : "http://lectures.moosader.com/cs200/17-Memory-Management.mp4"
        //},
        //{ "name" : "Dynamic arrays",
            //"url" : "http://lectures.moosader.com/cs200/18-Dynamic-Arrays.mp4"
        //},
      //],
      //"reading" :         [   
        //{ "name" : "Chapter 14: Pointers, memory management, and dynamic variables and arrays",
            //"url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter14-pointers_and_memory.pdf"
        //},
      //],
      //"archives" :        [   
        //{ "name" : "Pointers and dynamic arrays (Fall 2021)",
            //"url" : "https://www.youtube.com/watch?v=mxnfMLtZogg"
        //},
        //{ "name" : "Pointers (Spring 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-04-20_cs200_lecture_pointers.mp4"
        //},
        //{ "name" : "More with pointers (Spring 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-04-22_cs200_lecture_pointers.mp4"
        //},
        //{ "name" : "Dynamic arrays (Spring 2021)",
            //"url" : "http://lectures.moosader.com/cs200/2021-01_Spring/2021-05-04_cs200_lecture_dynamic_arrays.mp4"
        //},
      //],
      //"exercises" :       [   
        //{ "key" : "u10eA", "name" : "Memory management",
            //"docs" : "https://gitlab.com/rachels-courses/cs200/-/blob/main/Unit10/u10eA_MemoryManagement/08Ae%20Memory%20Management.pdf",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946574",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946610",
            //"due" : "Nov 17",
            //"end" : "Nov 24",
        //},
        //{ "key" : "u10eB", "name" : "Pointers",
            //"docs" : "u10eB_Pointers.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946575",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946611",
            //"due" : "Nov 17",
            //"end" : "Nov 24",
        //},
        //{ "key" : "u10eC", "name" : "Dynamic memory allocation",
            //"docs" : "u10eC_DynamicMemoryAllocation.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946576",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946612",
            //"due" : "Nov 17",
            //"end" : "Nov 24",
        //},
      //],
      //"quizzes" :         [   
        //{ "key" : "u10qA", "name" : "Pointers",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946522",
            //"378" : "https://canvas.jccc.edu/courses/42979/assignments/917140",
            //"due" : "Nov 17",
            //"end" : "Nov 24",
        //},
      //],
      //"tech-literacy" :   [   
        //{ "key" : "u10tl", "name" : "Professional networking",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946510",
            //"378" : "https://canvas.jccc.edu/courses/42979/assignments/917164",
            //"due" : "Nov 17",
            //"end" : "Nov 24",
        //},
      //],
      //"exams" :           [   
        //{ "key" : "u10t", "name" : "Pointers and memory",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946498",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946551",
            //"due" : "Dec 11",
            //"end" : "Dec 12",
        //},
      //]

//"exercises" :       [   
        //{ "key" : "u11eA", "name" : "STL Vector (WIP)",
            //"docs" : "",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946762",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946764",
            //"due" : "Nov 24",
            //"end" : "Dec 1",
        //},
        //{ "key" : "u11eB", "name" : "STL Map (WIP)",
            //"docs" : "",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946763",
            //"378" : "https://canvas.jccc.edu/courses/49310/assignments/946765",
            //"due" : "Nov 24",
            //"end" : "Dec 1",
        //},
      //],
      
      //"quizzes" : [       
        //{ "key" : "c4", "name" : "🧑‍🏫 Check-in (December)",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946538",
            //"378" : "https://canvas.jccc.edu/courses/42979/assignments/917170",
            //"due" : "Dec 1",
            //"end" : "Dec 6",
        //},
      //],
      //"projects" :        [   
        //{ "key" : "p4", "name" : "Student project",
            //"docs" : "p_StudentProject.html",
            //"350" : "https://canvas.jccc.edu/courses/49304/assignments/946543",
            //"378" : "https://canvas.jccc.edu/courses/42979/assignments/917249",
            //"due" : "Dec 7",
            //"end" : "Dec 8",
        //},
      //],
