// CS 200
$( document ).ready( function() {

    headerInfo = {
        "courseCode"    : "CS 134",
        "course"        : "CS 134: Programming Fundamentals",
        "dates"         : "2022-06-06 to 2022-07-28",
        "semester"      : "Summer 2022",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs134/",
        "textbook1"     : "",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_134",
        "syllabus"      : "syllabus.html",
        "exampleCode"   : "#",
        "prerequisites" : "None",
        "description"   : "In this introductory course, students will create interactive computer applications that perform tasks and solve problems. Students will design, develop and test object-oriented programs that utilize fundamental logic, problem-solving techniques and key programming concepts. 3 hrs. lecture, 2 hrs. open lab /wk.",
        "credit_hours"  : "4",
    };
    
    sectionInfo = [
        //{
            //"semester"      : "Spring 2022",
            //"crn"           : "11468",
            //"section"       : "350",
            //"method"        : "Online only",
            //"times"         : "Online only",
            //"background"    : "bg1",
            //"room"          : "none"
        //},
    ];
    
} );
