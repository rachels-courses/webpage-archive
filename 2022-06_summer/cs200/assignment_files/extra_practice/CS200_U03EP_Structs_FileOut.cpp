#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

// Ignore these
void Test_Vehicle();
void Test_SaveFile();

// --------------------------------------------------------------------- PRACTICE PROBLEMS BEGIN

struct Vehicle
{
  int seats;
  int passengers;
  
  // Ignore these guys:
  friend ostream& operator<<( ostream& out, const Vehicle& item );
  friend bool operator==( const Vehicle& left, const Vehicle& right );
};

/**
 * Sum function
 * Inputs: Vehicle vehicle1, vehicle2, two vehicles with a set amount of seating.
 * Returns: A vehicle with more seats, containing both sets of passengers
 * 
 * Create a new Vehicle variable and set its seats to the size of
 * the sum of vehicle1 and vehicle2's seats.
 * Set the passengers variable to the sum of vehicle1 and vehicle2's passengers.
 * Return the new vehicle variable as the result.
 * */
Vehicle Sum( Vehicle vehicle1, Vehicle vehicle2 )
{
  Vehicle newCar;
  // Add code
  return newCar;
}

/**
 * SaveFile function
 * Inputs:  string filename - the filename to open up
 *          string line1, line2, line3 - lines of text to write to the text file,
 *          each on their own separate lines.
 * Returns: None
 * 
 * This function should create an output file stream, open the filename given,
 * and write the three lines to the text file, each on their separate lines.
 * */
void SaveFile( string filename, string line1, string line2, string line3 )
{
  // add code here
}
// --------------------------------------------------------------------- PRACTICE PROBLEMS END

// --------------------------------------------------------------------- main()
// You don't need to modify main.

int main() 
{
  cout << left;
  Test_Vehicle();
  Test_SaveFile();
  
  return 0;
} 


// --------------------------------------------------------------------- TESTS
// Ignore this code

void Test_Vehicle()
{
  string testname = "Vehicle";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  Vehicle input_a[TEST_COUNT];
  input_a[0].seats              = 4;
  input_a[0].passengers         = 3;
  input_a[1].seats              = 10;
  input_a[1].passengers         = 8;
  Vehicle input_b[TEST_COUNT];        
  input_b[0].seats              = 6;
  input_b[0].passengers         = 5;
  input_b[1].seats              = 12;
  input_b[1].passengers         = 12;
  Vehicle expected_output[TEST_COUNT];
  expected_output[0].seats      = 10;
  expected_output[0].passengers = 8;
  expected_output[1].seats      = 22;
  expected_output[1].passengers = 20;
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    Vehicle actual_output = Sum( input_a[i], input_b[i] );
    if ( actual_output == expected_output[i] )  { result = "[PASS]"; }
    else                                        { result = "[FAIL]"; }
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
    << ": Call " << testname << "(" << input_a[i] << ", " << input_b[i] 
    << ") and check that the result is " << expected_output[i] << endl << endl;
    if ( result == "[FAIL]" ) { cout << "HINT: Make sure you're using the 'return' command.\n If the numbers look like garbage,\n you're probably not assigning a value to those variables." << endl; }
    cout << "INPUTS:" << endl  << setw( 30 ) << "* input_a:" << setw( 20 ) << input_a[i] << endl << setw( 30 ) << "* input_b:" << setw( 20 ) << input_b[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output[i]<< endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
}

void Test_SaveFile()
{
  string testname = "SaveFile";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  string input_filename[TEST_COUNT] = { "test1.txt", "test2.txt" };
  string input_line1[TEST_COUNT] = { "ABC", "one" };
  string input_line2[TEST_COUNT] = { "DEF", "two" };
  string input_line3[TEST_COUNT] = { "GHI", "three" };
  string actual_output[TEST_COUNT][3];
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    SaveFile( input_filename[i], input_line1[i], input_line2[i], input_line3[i] );
    
    ifstream input( input_filename[i] );
    if ( input.fail() )
    {
      result = "[FAIL]";
      
      cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
        << ": Call " << testname << "(" << input_filename[i] << ", " << input_line1[i] << ", " << input_line2[i] << ", " << input_line3[i]
        << ") and check the file that was saved." << endl << endl;
      if ( result == "[FAIL]" ) { cout << "The file \"" << input_filename[i] << "\" was not found!" << endl; }
      
      continue;
    }
    
    getline( input, actual_output[i][0] );
    getline( input, actual_output[i][1] );
    getline( input, actual_output[i][2] );
    
    if      ( actual_output[i][0] != input_line1[i] ) { result = "[FAIL]"; }
    else if ( actual_output[i][1] != input_line2[i] ) { result = "[FAIL]"; }
    else if ( actual_output[i][2] != input_line3[i] ) { result = "[FAIL]"; }
    else                                              { result = "[PASS]"; }
  
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
        << ": Call " << testname << "(" << input_filename[i] << ", " << input_line1[i] << ", " << input_line2[i] << ", " << input_line3[i]
    << ") and check the file that was saved." << endl << endl;
    
    if ( result == "[FAIL]" ) { cout << "HINT: Make sure each line variable goes on a separate line. Use endl to end a line." << endl; }
    cout << "INPUTS:" << endl  
      << setw( 30 )  << "* input_filename:" << setw( 20 ) << input_filename[i] << endl 
      << setw( 30 ) << "* input_line1:" << setw( 20 ) << input_line1[i] << endl
      << setw( 30 ) << "* input_line2:" << setw( 20 ) << input_line2[i] << endl
      << setw( 30 ) << "* input_line3:" << setw( 20 ) << input_line3[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << endl  
      << input_line1[i] << endl << input_line2[i] << endl << input_line3[i] << endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << endl 
      << actual_output[i][0] << endl << actual_output[i][1] << endl << actual_output[i][2] << endl;
  }
}


ostream& operator<<( ostream& out, const Vehicle& item )
{
  out << setw( 5 ) << item.passengers << " passengers, " << setw( 5 ) << item.seats << " seats. ";
  return out;
}

bool operator==( const Vehicle& left, const Vehicle& right )
{
  return ( left.passengers == right.passengers && left.seats == right.seats );
}
