#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

// Ignore these
void Test_Countdown();
void Test_SumRange();
void Test_SumArray();
void DisplayArray( int arr[], int size );
void DisplayArrayLine( int arr[], int size );
bool Contains( string haystack, string needle, bool caseSensitive = true );

// --------------------------------------------------------------------- PRACTICE PROBLEMS BEGIN
/**
 * Countdown function
 * Inputs:  string filename - the file to output to
 *          int start - the number to begin with
 *          int end - the number to end with
 * Returns: no return statement
 * 
 * Creates a text file (with the filename) and outputs all the numbers
 * from `start` until `end` (inclusive).
 * Use a for loop to iterate over all numbers in the range.
 * */
void Countdown( string filename, int start, int end )
{
  // Add code
}

/**
 * SumRange function
 * Inputs:  int start - the number to begin with
 *          int end - the number to end with
 * Returns: The sum of all numbers from between `start` and `end`, inclusive.
 * 
 * Use a for loop to iterate over all numbers in the range.
 * */
int SumRange( int start, int end )
{
  int sum = 0;
  // Add code
  return sum;
}

/**
 * SumArray function
 * Inputs:  int numberArray[] - an array of integer values
 *          int size - the amount of numbers in the array
 * Returns: The sum of all numbers from within the array
 * 
 * Use a for loop to iterate over all numbers in the array.
 * */
int SumArray( int numberArray[], int size )
{
  int sum = 0;
  // Add code
  return sum;
}





// --------------------------------------------------------------------- PRACTICE PROBLEMS END

// --------------------------------------------------------------------- main()
// You don't need to modify main.

int main() 
{
  cout << left;
  Test_Countdown();
  Test_SumRange();
  Test_SumArray();
  
  return 0;
} 


// --------------------------------------------------------------------- TESTS
// Ignore this code

void Test_Countdown()
{
  // void Countdown( string filename, int start, int end )
  string testname = "Countdown";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  string input_filename[TEST_COUNT]   = { "u04ep_test1.txt", "u04ep_test2.txt" };
  int input_start[TEST_COUNT]         = { 1, 2 };
  int input_end[TEST_COUNT]           = { 10, 8 };
  string expected_output[TEST_COUNT]  = { "1 2 3 4 5 6 7 8 9 10", "2 3 4 5 6 7 8" };
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    Countdown( input_filename[i], input_start[i], input_end[i] );
    
    ifstream input( input_filename[i] );
    string actual_output = "";
    string buffer = "";
    while ( input >> buffer )
    {
      actual_output += buffer + " ";
    }
    
    if ( Contains( actual_output, expected_output[i] ) )  { result = "[PASS]"; }
    else                                                  { result = "[FAIL]"; }
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
    << ": Call " << testname << "( \"" << input_filename[i] << "\", " << input_start[i] << ", " << input_end[i] << ") and check the result" << endl << endl;
    cout << "INPUTS:" << endl  
      << setw( 30 ) << "* input_filename:" << setw( 20 ) << input_filename[i] << endl 
      << setw( 30 ) << "* input_start:" << setw( 20 ) << input_start[i] << endl
      << setw( 30 ) << "* input_end:" << setw( 20 ) << input_end[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output[i]<< endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
}

void Test_SumRange()
{
  string testname = "SumRange";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  int input_a[TEST_COUNT] = { 1, 5 };
  int input_b[TEST_COUNT] = { 5, 10 };
  int expected_output[TEST_COUNT] = { 15, 45 };
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    int actual_output = SumRange( input_a[i], input_b[i] );
    if ( actual_output == expected_output[i] )  { result = "[PASS]"; }
    else                                        { result = "[FAIL]"; }
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
    << ": Call " << testname << "(" << input_a[i] << ", " << input_b[i] << ") and check that the result is " << expected_output[i] << endl << endl;
    if ( result == "[FAIL]" ) { cout << "HINT: After doing the math make sure to use the return command." << endl; }
    cout << "INPUTS:" << endl  << setw( 30 ) << "* input_a:" << setw( 20 ) << input_a[i] << endl << setw( 30 ) << "* input_b:" << setw( 20 ) << input_b[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output[i]<< endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
}

void Test_SumArray()
{
  string testname = "SumArray";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  
  {
    int input_array[] = { 1, 3, 5, 8 };
    int input_size = 4;
    int expected_output = 17;
    int actual_output = SumArray( input_array, input_size );
    
    if ( actual_output == expected_output ) { result = "[PASS]"; }
    else                                    { result = "[FAIL]"; }
    
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " 1" 
    << ": Call " << testname << "( {";
    DisplayArrayLine( input_array, input_size );
    cout << "}, " << input_size << " ) and check that the result is " << expected_output << endl << endl;
    
    if ( result == "[FAIL]" )
    {
      cout << "HINT: Make sure to initialize your sum variable to 0 before the loop.\n Did you make sure to return the result?\n Make sure you're iterating over all the elements of the array...\n The `size` parameter specifies the size of the array." << endl;
    }
    
    cout << "INPUTS:" << endl
      << "* input_size: " << input_size << endl
      << "* input_array:" << endl;
    DisplayArray( input_array, input_size );
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output << endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
  
  {
    int input_array[] = { 1, 1, 3, 5, 8 };
    int input_size = 5;
    int expected_output = 18;
    int actual_output = SumArray( input_array, input_size );
    
    if ( actual_output == expected_output ) { result = "[PASS]"; }
    else                                    { result = "[FAIL]"; }
    
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " 1" 
    << ": Call " << testname << "( {";
    DisplayArrayLine( input_array, input_size );
    cout << "}, " << input_size << " ) and check that the result is " << expected_output << endl << endl;
    
    if ( result == "[FAIL]" )
    {
      cout << "HINT: Make sure to initialize your sum variable to 0 before the loop.\n Did you make sure to return the result?\n Make sure you're iterating over all the elements of the array...\n The `size` parameter specifies the size of the array." << endl;
    }
    
    cout << "INPUTS:" << endl
      << "* input_size: " << input_size << endl
      << "* input_array:" << endl;
    DisplayArray( input_array, input_size );
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output << endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }

}





void DisplayArray( int arr[], int size )
{
  for ( int i = 0; i < size; i++ )
  {
    cout << "arr[" << i << "] = " << arr[i] << endl;
  }
}

void DisplayArrayLine( int arr[], int size )
{
  for ( int i = 0; i < size; i++ )
  {
    if ( i != 0 ) { cout << ", "; }
    cout << arr[i];
  }
}

bool Contains( string haystack, string needle, bool caseSensitive /*= true*/ )
{
    string a = haystack;
    string b = needle;

    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ )
        {
            a[i] = tolower( a[i] );
        }

        for ( unsigned int i = 0; i < b.size(); i++ )
        {
            b[i] = tolower( b[i] );
        }
    }

    return ( a.find( b ) != string::npos );
}
