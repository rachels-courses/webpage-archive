#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

// Ignore these
void Test_Sum();
void Test_TotalPrice();
void Test_FormatName();

// --------------------------------------------------------------------- PRACTICE PROBLEMS BEGIN
/**
 * Sum function
 * Inputs: int a, int b, two numbers to add together
 * Returns: the sum of a and b
 * */
int Sum( int a, int b )
{
  return 100; // temporary
}

/**
 * TotalPrice function
 * Inputs:  float singlePrice, the price of one item
 *          int itemCount, how many items are being purchased
 * Returns: The total price, which is the single price times the item count
 * */
float TotalPrice( float singlePrice, int itemCount )
{
  return 100; // temporary
}

/**
 * FormatName function
 * Inputs:  string name1, name2 - two names
 * Returns: return the name in the format "name2, name1". Use the + operator to combine strings.
 * */
string FormatName( string name1, string name2 )
{
  return "IMPLEMENT MEEEE"; // temporary
}

// --------------------------------------------------------------------- PRACTICE PROBLEMS END

// --------------------------------------------------------------------- main()
// You don't need to modify main.

int main() 
{
  cout << left;
  Test_Sum();
  Test_TotalPrice();
  Test_FormatName();
  
  return 0;
} 


// --------------------------------------------------------------------- TESTS
// Ignore this code

void Test_Sum()
{
  string testname = "Sum";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  int input_a[TEST_COUNT] = { 2, 4 };
  int input_b[TEST_COUNT] = { 3, -8 };
  int expected_output[TEST_COUNT] = { 5, -4 };
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    int actual_output = Sum( input_a[i], input_b[i] );
    if ( actual_output == expected_output[i] )  { result = "[PASS]"; }
    else                                        { result = "[FAIL]"; }
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
    << ": Call " << testname << "(" << input_a[i] << ", " << input_b[i] << ") and check that the result is " << expected_output[i] << endl << endl;
    if ( result == "[FAIL]" ) { cout << "HINT: After doing the math make sure to use the return command." << endl; }
    cout << "INPUTS:" << endl  << setw( 30 ) << "* input_a:" << setw( 20 ) << input_a[i] << endl << setw( 30 ) << "* input_b:" << setw( 20 ) << input_b[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output[i]<< endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
}

void Test_TotalPrice()
{
  string testname = "TotalPrice";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  float input_a[TEST_COUNT] = { 2.53, 3.47 };
  int input_b[TEST_COUNT] = { 5, 7 };
  float expected_output[TEST_COUNT] = { 12.65, 24.29 };
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    int actual_output = Sum( input_a[i], input_b[i] );
    if ( actual_output == expected_output[i] )  { result = "[PASS]"; }
    else                                        { result = "[FAIL]"; }
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
    << ": Call " << testname << "(" << input_a[i] << ", " << input_b[i] << ") and check that the result is " << expected_output[i] << endl << endl;
    if ( result == "[FAIL]" ) { cout << "HINT: After doing the math make sure to use the return command." << endl; }
    cout << "INPUTS:" << endl  << setw( 30 ) << "* input_a:" << setw( 20 ) << input_a[i] << endl << setw( 30 ) << "* input_b:" << setw( 20 ) << input_b[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output[i]<< endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
}

void Test_FormatName()
{
  string testname = "FormatName";
  cout << endl << string( 80, '#' ) << endl <<  testname << " TESTS" << endl;
  string result = "";
  const int TEST_COUNT = 2;
  string input_a[TEST_COUNT] = { "Keanu",  "Ted" };
  string input_b[TEST_COUNT] = { "Reeves", "Lasso" };
  string expected_output[TEST_COUNT] = { "Reeves, Keanu", "Lasso, Ted" };
  
  for ( int i = 0; i < TEST_COUNT; i++ )
  {
    string actual_output = FormatName( input_a[i], input_b[i] );
    if ( actual_output == expected_output[i] )  { result = "[PASS]"; }
    else                                        { result = "[FAIL]"; }
    cout << endl << string( 80, '-' ) << endl << setw( 8 ) << result << "Test_" << testname << " " << (i+1) 
    << ": Call " << testname << "(" << input_a[i] << ", " << input_b[i] << ") and check that the result is " << expected_output[i] << endl << endl;
    if ( result == "[FAIL]" ) { cout << "HINT: Did you remember to add a \", \"?" << endl; }
    cout << "INPUTS:" << endl  << setw( 30 ) << "* input_a:" << setw( 20 ) << input_a[i] << endl << setw( 30 ) << "* input_b:" << setw( 20 ) << input_b[i] << endl;
    cout << setw( 30 ) << "EXPECTED OUTPUT:" << setw( 20 ) << expected_output[i]<< endl;
    cout << setw( 30 ) << "ACTUAL OUTPUT:   " << setw( 20 ) << actual_output << endl << endl;
  }
}
