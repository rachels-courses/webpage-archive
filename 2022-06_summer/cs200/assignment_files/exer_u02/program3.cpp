#include <iostream>
using namespace std;

int main( int argumentCount, char * argumentList[] ) 
{
  // Make sure we have enough arguments
  if ( argumentCount < 4 )
  {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " name course semester year" << endl;
    return 1; // Exit with error code 1
  }


  // Program exit
  return 0;
} 